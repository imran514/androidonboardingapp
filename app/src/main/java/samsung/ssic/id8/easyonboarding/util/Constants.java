package samsung.ssic.id8.easyonboarding.util;

public class Constants {


    public static final String LIGHTS_URL = "http://%s/api/%s/lights/%d";


    public static final String STRING_TOKEN = ";";

    public static final String ADD_DEVICES_DIALOG_CANCEL = "ssic.id8.easyonboarding.ADD_DEVICES_DIALOG_CANCEL";
    public static final String NEXT_FRAGMENT_INTENT = "ssic.id8.nighthawk.FRAGMENT_NEXT_INTENT";
    public static final String ADD_DEVICES_DIALOG_SELECTED = "ssic.id8.easyonboarding.ADD_DEVICES_DIALOG_SELECTED";
    public static final String UPDATE_DATA_INTENT = "ssic.id8.nighthawk.UPDATE_DATA_INTENT";
    public static final String MODULE_ONBOARDED = "ssic.id8.nighthawk.MODULE_ONBOARDED";
    public static final String LED_ON = "ssic.id8.nighthawk.LED_ON";
    public static final String LED_OFF = "ssic.id8.nighthawk.LED_OFF";
    public static final String BT_PAIRING_REQ_INTENT = "android.bluetooth.device.action.PAIRING_REQUEST";
    public static final String BT_TARGET_DISCOVERY_RESULT = "android.bluetooth.device.discovery.target";
    public static final String DISCOVERED_MODULE_ID = "DISCOVERED_MODULE_ID";


    public static final String NEW_MODULE = "new_module";
    public static final int DO_NOTHING = 3;
    public static final int QR_CODE = 0;
    public static final int NFC_TAG = 1;
    public static final int MANUAL_ENTRY = 2;

    public static final String MODULE_INFO = "MODULE_INFO";
    public static final String MODULE_INFO_BUNDLE = "MODULE_INFO_BUNDLE";

    public static final int ARTIK_0 = 0;
    public static final int ARTIK_1 = 1;
    public static final int ARTIK_5 = 5;
    public static final int ARTIK_7 = 7;
    public static final int ARTIK_10 = 1;
    public static final int DETECTED_DEVICE = 3;
    public static final int START_SCANNING = 1;
    public static final int STOP_SCANNING = 2;
    public static final int CONNECT_DEVICE = 4;
    public static final int GATT_CONNECTED = 5;
    public static final int GATT_DISCONNECTED = 6;
    public static final int GATT_FAILED = 7;
    public static final int GATT_SERVICES_DISCOVERED = 8;
    public static final int STATUS_NOTIFIED = 9;
    public static final int LONG_STATUS_NOTIFIED = 10;
    public static final int VENDOR_ID_READ = 11;
    public static final int DEVICE_ID_READ = 12;
    public static final int DISCONNECT_TARGET = 13;
    public static final int RESET_TARGET = 14;
    public static final int SEND_WIFI_CRED = 15;
    public static final int DETECTED_SDR_DEVICE = 16;
    public static final int ACTION_DATA_AVAILABLE = 17;
    public static final int EXTRA_DATA = 18;
    public static final int DEVICE_TYPE_ID_READ = 19;
    public static final int DEVICE_CHALLENGE_PIN_READ = 20;
    public static final int DEVICE_DID_READ = 21;
    public static final int IPADDRESS_READ = 22;
    public static final int ERROR_INVALID_MAC_ADDRESS = 23;
    public static final int REGIS_STATE_START = 0;
    public static final int REGIS_STATE_GET_DEVICE_TYPE_ID = 1;
    public static final int REGIS_STATE_GET_CHALLENGE_PIN = 2;
    /*    public static final int REGIS_STATE_GET_CHALLENGE_PIN = 3;
            public static final int REGIS_STATE_GET_CHALLENGE_PIN = 4;*/
    public static final int REGIS_STATE_RECEIVED_CHALLENGE_PIN = 5;
    public static final int REGIS_STATE_USER_CONFIRMED = 6;
    public static final int REGIS_STATE_GET_DEVICE_DID = 7;
    public static final int REGIS_STATE_COMPLETED = 8;
    public static final int PLUGIN_ARTIK = 0;
    public static final int ARTIK_FOUND = 1;
    public static final int WIFI_SCAN = 2;
    public static final int WIFI_CONNCTD = 3;
    public static final int LED_TEST_START = 4;
    public static final int LED_TEST_DONE = 5;



    public static String ONBOARDING_DEVICE_MAC = "AA:BB:CC:DD:EE:FF";

    public static String getOnboardingDeviceMac() {
        return ONBOARDING_DEVICE_MAC;
    }

    public static void setOnboardingDeviceMac(String onboardingDeviceMac) {
        ONBOARDING_DEVICE_MAC = onboardingDeviceMac;
    }


}
