/*
*
* DeviceListObject.java
*
* Created by Namrata Garach on 3/7/16
*
* */

package samsung.ssic.id8.easyonboarding.device_onboarding;

/* Getters and setters for already connected device list */

import samsung.ssic.id8.easyonboarding.control.Module;

public class DeviceListObject {

    public String deviceName;
    public String deviceId;
    public Boolean requiresProviderAuth;
    public Boolean hasCloudConnector;
    public Module mModule;

    public DeviceListObject(String deviceId, String deviceName, Boolean requiresProviderAuth,
                            Boolean hasCloudConnector) {
        this.deviceId = deviceId;
        this.deviceName = deviceName;
        this.requiresProviderAuth = requiresProviderAuth;
        this.hasCloudConnector = hasCloudConnector;
    }

    public DeviceListObject(String deviceId, String deviceName, Boolean requiresProviderAuth,
                            Boolean hasCloudConnector, Module module) {
        this.deviceId = deviceId;
        this.deviceName = deviceName;
        this.requiresProviderAuth = requiresProviderAuth;
        this.hasCloudConnector = hasCloudConnector;
        this.mModule = module;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public Module getModule() {
        return mModule;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public Boolean getRequiresProviderAuth() {
        return requiresProviderAuth;
    }

    public void setRequiresProviderAuth(Boolean requiresProviderAuth) {
        this.requiresProviderAuth = requiresProviderAuth;
    }

    public Boolean getHasCloudConnector() {
        return hasCloudConnector;
    }

    public void setHasCloudConnector(Boolean hasCloudConnector) {
        this.hasCloudConnector = hasCloudConnector;
    }
}
