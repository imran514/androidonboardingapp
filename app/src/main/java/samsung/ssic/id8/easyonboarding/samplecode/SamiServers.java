package samsung.ssic.id8.easyonboarding.samplecode;

import io.samsungsami.android.SamiStack;

public class SamiServers {

    public static final int PROD = 0;
    public static final int DEV = 1;
    public static final int QA = 2;
    public static final int LOCALHOST = 3;

    /**
     * Returns the default configuration
     *
     * @return
     */
    public static SamiStack getDefaultConfig() {
        return getProd();
    }

    /**
     * Returns the configuration for PROD
     *
     * @return
     */
    public static SamiStack getProd() {
        SamiStack server = new SamiStack(
                "04501afb1a4243949ff9c57547bd7342",
                "https://samsungsami.io/companion/authorize");
        return server;
    }

    /**
     * Returns the configuration for DEV
     *
     * @return
     */
    public static SamiStack getDev() {
        SamiStack server = new SamiStack(
                "04501afb1a4243949ff9c57547bd7342",
                "https://api-dev.samsungsami.io/companion/authorize");
        return server;
    }

    /**
     * Returns the configuration for QA
     *
     * @return
     */
    public static SamiStack getQA() {
        SamiStack server = new SamiStack(
                "04501afb1a4243949ff9c57547bd7342",
                "https://samsungsami.io/companion/authorize");
        return server;
    }

    /**
     * Returns the configuration for Samsung Accounts
     *
     * @return
     */
    public static SamiStack getSDS() {
        SamiStack server = new SamiStack(
                "04501afb1a4243949ff9c57547bd7342",
                "https://api-sds.samsungsami.io/companion/authorize");
        return server;
    }

    /**
     * Returns the configuration for localhost according to
     * default SAMI ports configuration on the sample localhost file
     *
     * @param ipAddress the IP address of the SAMI machine in the network
     * @return
     */
    public static SamiStack getLocalhost(String ipAddress) {
        SamiStack server = new SamiStack(
                "04501afb1a4243949ff9c57547bd7342",
                "http://localhost:9000/authorize",
                "http://" + ipAddress + ":6671",
                "http://" + ipAddress + ":6621",
                "http://" + ipAddress + ":6681/api",
                "http://" + ipAddress + ":6621",
                "ws://" + ipAddress + ":6671",
                "ws://" + ipAddress + ":6672");
        return server;
    }

}
