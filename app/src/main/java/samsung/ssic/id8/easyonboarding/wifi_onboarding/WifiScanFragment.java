package samsung.ssic.id8.easyonboarding.wifi_onboarding;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import samsung.ssic.id8.easyonboarding.R;
import samsung.ssic.id8.easyonboarding.util.Avenir;
import samsung.ssic.id8.easyonboarding.util.Constants;


public class WifiScanFragment extends Fragment {

    private final static String TAG = "WifiScanFragment";
    List<Map<String, String>> mWifiNetworksList;
    private TextView mTextView;
    private ListView mListView;
    private ProgressDialog mProgressDialog;

    private ImageButton mRefreshButton;
    private SimpleAdapter mSimpleAdapter;
    private Handler handler = new Handler();
    private INetworkProvisioner mProvisioner;
    private AlertDialog.Builder mBuilder;
    private LayoutInflater mLayoutInflater;
    private Handler mHandler;
    private int connectedVal = 0;

    private static String getEncryption(ScanResult result) {
        if (result.capabilities.contains("WEP")) {
            return "WEP";
        } else if (result.capabilities.contains("PSK")) {
            return getPskType(result);
        } else if (result.capabilities.contains("EAP")) {
            return "EAP";
        }
        return "OPEN";
    }

    private static String getPskType(ScanResult result) {
        boolean wpa = result.capabilities.contains("WPA-PSK");
        boolean wpa2 = result.capabilities.contains("WPA2-PSK");
        if (wpa2 && wpa) {
            return "WPA/WPA2";
        } else if (wpa2) {
            return "WPA2";
        } else if (wpa) {
            return "WPA";
        } else {
            return "UNKNOWN";
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.wifi_scan_fragment, container, false);
        connectedVal = 0;
        //getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
/*
        mTextView = (TextView) v.findViewById(R.id.welcome_swipe_text);
        mTextView.setAnimation(AnimationUtils.loadAnimation(getActivity().getApplicationContext(),
				R.anim.fade_in_out));
*/
        mBuilder = new AlertDialog.Builder(getActivity());
        Avenir avenir = Avenir.getInstance(getActivity());
        TextView textView = (TextView)
                v.findViewById(R.id.setup_user_text);

        mLayoutInflater = inflater;


        mRefreshButton = (ImageButton) v.findViewById(R.id.refreshButton);
        mRefreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mProvisioner.requestScan();
                showProgressDialog("Loading", "Loading WiFi AP...");
            }
        });

        mWifiNetworksList = new ArrayList<Map<String, String>>();
        mListView = (ListView) v.findViewById(R.id.list);

        String[] from = {"ssid", "encryption"};
        int[] to = {R.id.ssidfield, R.id.encryption};

        mSimpleAdapter = new SimpleAdapter(getActivity(), mWifiNetworksList, R.layout.list_item_2, from, to);
        Log.d(TAG, "simple adapter SSID: " + from.toString() + " Encryption : " + to.toString());
        mListView.setAdapter(mSimpleAdapter);


        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final TextView textView1 = (TextView) view.findViewById(R.id.ssidfield);
                final TextView textView2 = (TextView) view.findViewById(R.id.encryption);
                final String selectedSSID = textView1.getText().toString();


                Log.d(TAG, "Clicked SSID: " + selectedSSID);
                if (textView2.getText().toString().equals("OPEN")) {
                    if ((storeWiFiNetwork(selectedSSID, textView2.getText().toString(), null))) {
                        Log.d(TAG, "Selected SSID: " + selectedSSID + " Encryption : " + textView2.getText().toString() + " Password : Open");
                    }
                    mHandler.sendEmptyMessage(Constants.SEND_WIFI_CRED);
                    showProgressDialog("Connecting", "Connecting to Wifi ...");
                    //getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


                } else if (textView2.getText().toString().equals("UNKNOWN")) {

                } else {
                    mBuilder.setMessage(null);
                    View dialogView = mLayoutInflater.inflate(R.layout.dialog_wifi, null);
                    final EditText editText = (EditText) dialogView.findViewById(R.id.password);
                    final CheckBox checkBox = (CheckBox) dialogView.findViewById(R.id.checkBox);
                    checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if (!isChecked) {
                                editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

                            } else {
                                editText.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);

                            }
                            editText.setSelection(editText.getText().length());
                        }
                    });
                    mBuilder.setView(dialogView);

                    mBuilder.setTitle(selectedSSID);
                    mBuilder.setIcon(R.drawable.wifi_icon);
                    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(editText, 0);
                    mBuilder.setPositiveButton("Join", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // ((WifiOnboarding_Activity) getActivity()).setWifiNetwork(mWifiNetwork);
 /*                           final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);*/
                            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                            if ((storeWiFiNetwork(selectedSSID, textView2.getText().toString(), editText.getText().toString()))) {
                                Log.d(TAG, "Selected SSID: " + selectedSSID + " Encryption : " + textView2.getText().toString() + " Password : " + editText.getText().toString());
                            }

                            mHandler.sendEmptyMessage(Constants.SEND_WIFI_CRED);

                            showProgressDialog("Connecting", "Connecting to Wifi ...");
                        }
                    });
                    mBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                        }
                    });
                    AlertDialog securityDialog = mBuilder.create();
                    securityDialog.show();
                    securityDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                        }
                    });
                }


            }
        });


        mProvisioner = new WiFiNetworkProvisioner(getActivity(), this.handler, new INetworkProvisioner.Listener() {

            @Override
            public void onScanResult(List<ScanResult> wifiList) {

                mWifiNetworksList.clear();
                hideProgressBar();

                outerLoop:
                for (int i = 0; i < wifiList.size(); i++) {
                    ScanResult result = wifiList.get(i);

                    if (result == null || result.SSID.isEmpty() ||
                            result.capabilities.contains("[IBSS]") ||
                            result.level < -75) {
                        continue;
                    }


                    String resultEncryption = getEncryption(result);

                    for (Map<String, String> entry : mWifiNetworksList) {
                        if (entry.get("ssid").toString().equals(result.SSID)) {
                            // Wifi is known
                            String encryption = entry.get("encryption");
                            // To avoid collisions of multiple SSIDs
                            if (!encryption.equals(getEncryption(result))) {
                                resultEncryption = resultEncryption + "," + encryption;
                                entry.put("encryption", resultEncryption);
                            }
                            continue outerLoop;
                        }
                    }
                    //Log.d(TAG, "Found SSID: "+result.SSID + " Encryption : "+resultEncryption);
                    mWifiNetworksList.add(putData(result.SSID, resultEncryption));
                }
                mSimpleAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed() {
                // TODO Auto-generated method stub
                Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDisconnected() {
                Toast.makeText(getActivity(), "Disconnected", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onConnected(WiFiNetwork wifiNetwork) {
                Toast.makeText(getActivity(), "Connected", Toast.LENGTH_SHORT).show();
                //storeWiFiNetwork(wifiNetwork);
                //finish();
            }

            @Override
            public void onConnecting() {
                Toast.makeText(getActivity(), "Connecting", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAuthenticating() {
                Toast.makeText(getActivity(), "Authenticating", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onObtainingIP() {
                Toast.makeText(getActivity(), "Obtaining IP", Toast.LENGTH_SHORT).show();
            }
        });

        mProvisioner.initWifiNetworkProvisioner();
        return v;
    }

    public void setHandler(Handler handler) {
        mHandler = handler;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onDestroy() {
        //mProvisioner.releaseWifiConfiguration();
        mProvisioner.unregisterBroadcastReceiver();
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
        mProvisioner.unregisterBroadcastReceiver();

    }


    @Override
    public void onResume() {
        super.onResume();
        showProgressDialog("Loading", "Loading WiFi AP...");
        mProvisioner.registerBroadcastReceiver();
        mProvisioner.requestScan();
    }

    private Map<String, String> putData(String ssid, String encryption) {
        Map<String, String> item = new HashMap<String, String>();
        item.put("ssid", ssid);
        item.put("encryption", encryption);
        return item;
    }

    private boolean storeWiFiNetwork(String ssid, String auth, String password) {
        WiFiNetwork wiFiNetwork = ((WifiOnboarding_Activity) getActivity()).getWifiNetwork();
        wiFiNetwork.SSID = ssid;
        if (!auth.equals("OPEN"))
            wiFiNetwork.authentication = "SECURE";
        else
            wiFiNetwork.authentication = auth;
        wiFiNetwork.password = password;
        ((WifiOnboarding_Activity) getActivity()).setWifiNetwork(wiFiNetwork);
        return true;
    }

    public void broadcastIntent(String action) {
        Intent intent = new Intent();
        intent.setAction(action);
        getActivity().sendBroadcast(intent);
    }

    public void updateStatus(String status) {
        hideProgressBar();
        if (connectedVal == 0) {
            connectedVal = 1;
            broadcastIntent(Constants.NEXT_FRAGMENT_INTENT);
        }
    }

    public void hideProgressBar() {
        if (mProgressDialog != null & mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void showProgressDialog(String title, String message) {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setTitle(title);
        mProgressDialog.setMessage(message);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

    }
}
