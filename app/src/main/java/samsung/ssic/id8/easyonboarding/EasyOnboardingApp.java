package samsung.ssic.id8.easyonboarding;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import io.samsungsami.android.Sami;
import io.samsungsami.android.api.Call;
import io.samsungsami.android.api.Callback;
import io.samsungsami.android.api.Code;
import io.samsungsami.client.ApiException;
import io.samsungsami.model.Device;
import io.samsungsami.model.DeviceArray;
import io.samsungsami.model.DeviceType;
import io.samsungsami.model.DeviceTypeEnvelope;
import io.samsungsami.model.DevicesEnvelope;
import samsung.ssic.id8.easyonboarding.control.Module;
import samsung.ssic.id8.easyonboarding.device_onboarding.DeviceListObject;


/**
 * Created by i.mohammed on 8/8/16.
 */
public class EasyOnboardingApp extends Application{

    private static final String TAG = "EasyOnboardingApp";

    public static final String APP_PREFS = "app_prefs";
    public static final String MODULES_PREF_KEY = "app_prefs";

    private Sami mSamiInstance ;
    private ArrayList<Module> mModules = new ArrayList<Module>();
    private Module mCurrentModule ;
    private SharedPreferences mSharedPreferences;
    private Type mListType;
    private  ArrayList<DeviceListObject> mUserDevices = new ArrayList<>();

    private boolean mRequiresCloudConnector = false;

    @Override
    public void onCreate() {
        super.onCreate();
        mSharedPreferences = getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
        mListType = new TypeToken<ArrayList<Module>>() {}.getType();

        Gson gson = new Gson();
        String json = mSharedPreferences.getString(MODULES_PREF_KEY, "");
        Log.e(TAG ,  "############### JSON ################");
        Log.e(TAG ,  "JSON = " + json);

        mModules = gson.fromJson(json, mListType);
        if(mModules == null){
            mModules = new ArrayList<Module>();
        }
    }

    public void addModule( Module module){

        mModules.add(module);
        Gson gson  = new Gson();
        String moduelsJson = gson.toJson(mModules, mListType);
        mSharedPreferences.edit().putString(MODULES_PREF_KEY, moduelsJson).commit();

    }

    public ArrayList<Module> getModules(){
        return mModules;
    }

    public ArrayList<DeviceListObject> getUserDevices() {
        return mUserDevices;
    }

    public void setUserDevices(ArrayList<DeviceListObject> mUserDevices) {
        this.mUserDevices = mUserDevices;
    }

    public void setCurrentModule(Module module){
        this.mCurrentModule = module;
    }

    public Sami getSamiInstance() {
        return mSamiInstance;
    }

    public void setSamiInstance(Sami mSamiInstance) {
        this.mSamiInstance = mSamiInstance;
    }

    // Make a call to get the devices of the current user
    public ArrayList<DeviceListObject> getUserDeviceFromCloud(final UserDevicesFetchedListener listener) {

        final ArrayList<DeviceListObject> deviceListObjArrayList = new ArrayList<DeviceListObject>();
        try {
            final String uid = mSamiInstance.getCredentials().getId();

            new Call(new Code() {

                @Override
                public Object run() {
                    try {
                        // Get the first 25 user devices added to SAMI for current user
                        DevicesEnvelope devicesEnvelope = mSamiInstance.getUsersApi()
                                .getUserDevices(0, 25, false, uid);
                        DeviceArray deviceArray = devicesEnvelope.getData();
                        for (Device device : deviceArray.getDevices()) {
                            DeviceTypeEnvelope deviceTypeEnvelope = mSamiInstance.getDeviceTypesApi().
                                    getDeviceType(device.getDtid());
                            DeviceType deviceType = deviceTypeEnvelope.getData();
                            if (deviceType.getHasCloudConnector()) { // check if cloud auth required
                                mRequiresCloudConnector = true;
                            }
                            DeviceListObject deviceListObject = new DeviceListObject(device.getId(),
                                    device.getName(),
                                    device.getNeedProviderAuth(), mRequiresCloudConnector);
                            deviceListObjArrayList.add(deviceListObject);
                        }
                    } catch (ApiException e) {
                        Log.e(TAG, e.getMessage());
                    }
                    Log.e(TAG, "DeviceListObject arrayList12 :" + deviceListObjArrayList.size());
                    listener.onUserDevicesFetched();
                    return deviceListObjArrayList;
                }
            }, new Callback() {

                @Override
                public void onApiResult(Object result) {
                }
            }).execute();
        } catch (Exception e) {
            Log.e(TAG, e.getStackTrace().toString());
        }
        mUserDevices = deviceListObjArrayList;
        return deviceListObjArrayList;
    }

    public interface UserDevicesFetchedListener{
        void onUserDevicesFetched();
    }
}
