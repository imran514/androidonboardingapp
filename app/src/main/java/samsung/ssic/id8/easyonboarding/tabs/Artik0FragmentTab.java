package samsung.ssic.id8.easyonboarding.tabs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import samsung.ssic.id8.easyonboarding.EasyOnboardingApp;
import samsung.ssic.id8.easyonboarding.R;
import samsung.ssic.id8.easyonboarding.control.Module;
import samsung.ssic.id8.easyonboarding.device_onboarding.DeviceListAdapter;
import samsung.ssic.id8.easyonboarding.device_onboarding.DeviceListObject;
import samsung.ssic.id8.easyonboarding.util.Constants;

public class Artik0FragmentTab extends Fragment {
    private final String TAG = "Artik0FragmentTab";

    private ArrayList<DeviceListObject> mDevicesOnCloud = new ArrayList<>();
    WebView mWebView;
    DeviceListAdapter mListAdapter;
    private String mAccessToken;


    public static Artik0FragmentTab newInstance() {
        Artik0FragmentTab fragment = new Artik0FragmentTab();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.artik0_layout, container, false);
        mAccessToken = "HelloAccessToken";

        ListView artik0ListView = (ListView) rootView.findViewById(R.id.list);
        mWebView = (WebView) rootView.findViewById(R.id.webview);
        mWebView.setVisibility(View.INVISIBLE);
        mDevicesOnCloud  = ((EasyOnboardingApp)getActivity().getApplication()).getUserDevices();
        mListAdapter = new DeviceListAdapter(getActivity(),
                R.layout.list_item_card, new ArrayList<DeviceListObject>(), mWebView, mAccessToken);
        artik0ListView.setAdapter(mListAdapter);

        EasyOnboardingApp easyOnboardingApp = (EasyOnboardingApp)getActivity().getApplication();
        List<Module> modules = easyOnboardingApp.getModules();
        for (Object object : modules) {
            Module module = (Module)object;
            if (module.getType() == Constants.ARTIK_0) {
                DeviceListObject deviceListObject1 = new DeviceListObject(module.getPlace(), module.getName(),
                        false, false, module);
                if(isRegisteredOnCloud(module)) {
                    mListAdapter.add(deviceListObject1);
                }
            }
        }
        mListAdapter.notifyDataSetChanged();

        return rootView;
    }

    private boolean isRegisteredOnCloud(Module currentModule){
        for (DeviceListObject device : mDevicesOnCloud){
            if(device.getDeviceId().equals(currentModule.getDID())){
                return true;
            }
        }
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


}
