package samsung.ssic.id8.easyonboarding.wifi_onboarding;

import java.util.HashMap;
import java.util.UUID;

public class OnboardingGattService {

    /* Service UUID */
    public static final UUID SERVICE_UUID = UUID.fromString("FFFFFFFF-C0C1-FFFF-C0C1-201401000000");

    /* Characteristics UUIDs */
    public static final UUID CHARACTERISTIC_STATUS = UUID.fromString("FFFFFFFF-C0C1-FFFF-C0C1-201401000001");
    public static final UUID CHARACTERISTIC_LONG_STATUS = UUID.fromString("FFFFFFFF-C0C1-FFFF-C0C1-201401000002");
    public static final UUID CHARACTERISTIC_SSID = UUID.fromString("FFFFFFFF-C0C1-FFFF-C0C1-201401000003");
    public static final UUID CHARACTERISTIC_AUTH = UUID.fromString("FFFFFFFF-C0C1-FFFF-C0C1-201401000004");
    public static final UUID CHARACTERISTIC_PASS = UUID.fromString("FFFFFFFF-C0C1-FFFF-C0C1-201401000005");
    public static final UUID CHARACTERISTIC_CHANNEL = UUID.fromString("FFFFFFFF-C0C1-FFFF-C0C1-201401000006");
    public static final UUID CHARACTERISTIC_COMMAND = UUID.fromString("FFFFFFFF-C0C1-FFFF-C0C1-201401000007");
    public static final UUID CHARACTERISTIC_VENDOR_ID = UUID.fromString("FFFFFFFF-C0C1-FFFF-C0C1-201401000008");
    public static final UUID CHARACTERISTIC_DEVICE_ID = UUID.fromString("FFFFFFFF-C0C1-FFFF-C0C1-201401000009");
    public static final UUID CHARACTERISTIC_WIFI_STATE = UUID.fromString("FFFFFFFF-C0C1-FFFF-C0C1-201401000010");
    public static final UUID CHARACTERISTIC_IPADDRESS = UUID.fromString("FFFFFFFF-C0C1-FFFF-C0C1-201401000011");
    public static final UUID CHALLENGE_PIN_UUID = UUID.fromString("0000FFF1-0000-1000-8000-00805F9B34FB");
    public static final UUID DEVICE_TOKEN_UUID = UUID.fromString("0000FFF2-0000-1000-8000-00805F9B34FB");
    public static final UUID DEVICE_TYPE_ID_UUID = UUID.fromString("0000FFF3-0000-1000-8000-00805F9B34FB");
    public static final UUID START_REGISTRATION_UUID = UUID.fromString("0000FFF5-0000-1000-8000-00805F9B34FB");
    public static final UUID COMPLETE_REGISTRATION_UUID = UUID.fromString("0000FFF6-0000-1000-8000-00805F9B34FB");
    public static final UUID DEVICE_ID_CHARACTERISTIC_UUID = UUID.fromString("0000FFF7-0000-1000-8000-00805F9B34FB");
    public static final UUID UID_CHARACTERISTIC_UUID = UUID.fromString("0000FFF8-0000-1000-8000-00805F9B34FB");
    public static final UUID SDR_REGISTRATION_UUID = UUID.fromString("0000FFF0-0000-1000-8000-00805F9B34FB");
    public static final String SDR_SERVICE = "0000FFF0-0000-1000-8000-00805F9B34FB";

    private static HashMap<String, String> attributes = new HashMap();

    public static final String STATUS_DISCONNECTED = "DISCONNECTED";
    public static final String STATUS_INITIALIZING = "INITIALIZING";
    public static final String STATUS_INITIALIZED = "INITIALIZED";
    public static final String STATUS_CONNECTING = "CONNECTING";
    public static final String STATUS_CONNECTED = "CONNECTED";
    public static final String STATUS_FAILED = "FAILED";
    public static final String STATUS_RECVD_PIN = "RECVD_PIN";
    public static final String STATUS_RECVD_TOKEN = "RECVD_TOKEN";

    public static final String LONG_STATUS_NONE = "NONE";
    public static final String LONG_STATUS_SET_SSID = "Set SSID";
    public static final String LONG_STATUS_SET_AUTH = "Set auth type";
    public static final String LONG_STATUS_WRONG_AUTH = "Wrong auth. type";
    public static final String LONG_STATUS_SET_PSK = "Set PSK";
    public static final String LONG_STATUS_SET_CHAN = "Conn. or set channel";
    public static final String LONG_STATUS_AUTHING = "Authenticating";
    public static final String LONG_STATUS_GETTING_IP = "Getting IP address";
    public static final String LONG_STATUS_CONN_ESTAB = "Connection completed";
    public static final String LONG_STATUS_PSK_NOT_REQ = "No PSK required";
    public static final String LONG_STATUS_RECVD_PIN = "Received Challenge";
    public static final String LONG_STATUS_RECVD_TOKEN = "Received Token";
    public static final String LONG_STATUS_INVALID_SSID = "Invalid SSID";
    public static final String LONG_STATUS_START_REGISTRATION_ERROR = "Registeration Error";
    public static final String LONG_STATUS_COMPLETE_REGISTRATION_ERROR = "Invalid Token";
    public static final String LONG_STATUS_WRONG_PASSWORD = "Invalid Password";
    public static final String LONG_STATUS_NO_IPADDRESS = "Unable to get Ip";
    public static final String LONG_STATUS_INTERNET_UNAVAILABLE = "No Internet";
    //public static final String LONG_STATUS_INVALID_WIFI_STATE = "Invalid wifi module state.";
    public static final String LONG_STATUS_INVALID_WIFI_STATE = "Invalid wifi module ";

    public static final String INVALID_MAC_ADDRESS = "Invalid Mac Address";

    static {
        // Sample SDR Services.
        attributes.put("0000FFF0-0000-1000-8000-00805F9B34FB", "SDR Service");
        // Sample Characteristics.
        attributes.put("0000FFF1-0000-1000-8000-00805F9B34FB", "Challenge PIN");
        attributes.put("0000FFF2-0000-1000-8000-00805F9B34FB", "Device Token");
        attributes.put("0000FFF3-0000-1000-8000-00805F9B34FB", "Device Type ID");
        attributes.put("0000FFF5-0000-1000-8000-00805F9B34FB", "Start Registration");
        attributes.put("0000FFF6-0000-1000-8000-00805F9B34FB", "Complete Registration");
    }

    public static String lookup(String uuid, String defaultName) {
        String name = attributes.get(uuid);
        return name == null ? defaultName : name;
    }


    //UUIDS
//SERVICE UUID : <16 byte UUID> | <3 byte DEVICE TYPE> | <2 byte RESERVE>
//Eg: 928bc7858360436d969ad15cf3b1cef0|A504F4|0000
//UUIDS
//SERVICE UUID : <16 byte UUID> | <3 byte DEVICE TYPE> | <2 byte RESERVE>
//Eg: 928bc7858360436d969ad15cf3b1cef0|A504F4|0000


	/* Service UUID */
/*	public static final UUID SERVICE_UUID = UUID.fromString("0000FFe0-0000-1000-8000-00805F9B34FB");

	*//* Characteristics UUIDs *//*
    public static final UUID CHARACTERISTIC_STATUS = UUID.fromString("0000FFE1-0000-1000-8000-00805F9B34FB");
	public static final UUID CHARACTERISTIC_LONG_STATUS = UUID.fromString("0000FFE2-0000-1000-8000-00805F9B34FB");
	public static final UUID CHARACTERISTIC_SSID = UUID.fromString("0000FFE3-0000-1000-8000-00805F9B34FB");
	public static final UUID CHARACTERISTIC_AUTH = UUID.fromString("0000FFE5-0000-1000-8000-00805F9B34FB");
	public static final UUID CHARACTERISTIC_PASS = UUID.fromString("0000FFE6-0000-1000-8000-00805F9B34FB");
	public static final UUID CHARACTERISTIC_CHANNEL = UUID.fromString("0000FFE7-0000-1000-8000-00805F9B34FB");
	public static final UUID CHARACTERISTIC_COMMAND = UUID.fromString("0000FFE9-0000-1000-8000-00805F9B34FB");
	public static final UUID CHARACTERISTIC_VENDOR_ID = UUID.fromString("0000FFEA-0000-1000-8000-00805F9B34FB");
	public static final UUID CHARACTERISTIC_DEVICE_ID = UUID.fromString("0000FFEB-0000-1000-8000-00805F9B34FB");*/



/*	//CHARACTERISTIC UUIDS WILL be same for all artik boards.
    var SERVICE_UUID  = '0000FFe0-0000-1000-8000-00805F9B34FB';
	var STATUS_CHARACTERISTIC_UUID= '0000FFE1-0000-1000-8000-00805F9B34FB';
	var DETAILED_STATUS_CHARACTERISTIC_UUID = '0000FFE2-0000-1000-8000-00805F9B34FB';
	var SSID_CHARACTERISTIC_UUID= '0000FFE3-0000-1000-8000-00805F9B34FB';
	var AUTH_CHARACTERISTIC_UUID= '0000FFE5-0000-1000-8000-00805F9B34FB';
	var PASSPHRASE_CHARACTERISTIC_UUID= '0000FFE6-0000-1000-8000-00805F9B34FB';
	var CHANNEL_CHARACTERISTIC_UUID= '0000FFE7-0000-1000-8000-00805F9B34FB';
	var COMMAND_CHARACTERISTIC_UUID= '0000FFE9-0000-1000-8000-00805F9B34FB';
	var VENDORID_CHARACTERISTIC_UUID= '0000FFEA-0000-1000-8000-00805F9B34FB';
	var DEVICEID_CHARACTERISTIC_UUID= '0000FFEB-0000-1000-8000-00805F9B34FB';*/


}
