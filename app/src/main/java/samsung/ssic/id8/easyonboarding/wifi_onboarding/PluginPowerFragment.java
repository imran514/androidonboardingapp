package samsung.ssic.id8.easyonboarding.wifi_onboarding;


import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import samsung.ssic.id8.easyonboarding.R;
import samsung.ssic.id8.easyonboarding.control.Module;
import samsung.ssic.id8.easyonboarding.util.Constants;


public class PluginPowerFragment extends Fragment {


    private final static String TAG = "PluginPowerFragment";
    private Module mModule;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.plugin_power_fragment, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        ImageView img = (ImageView) v.findViewById(R.id.plugin);
        ImageView moduleImage = (ImageView) v.findViewById(R.id.module_img);
        img.setBackgroundResource(R.drawable.plug01);

        setArtikImage(moduleImage);
        ObjectAnimator cloudAnim = ObjectAnimator.ofFloat(v.findViewById(R.id.plugin), "y", 1300, 1100);
        cloudAnim.setDuration(1000);
        cloudAnim.setRepeatCount(ValueAnimator.INFINITE);
        cloudAnim.setRepeatMode(ValueAnimator.RESTART);

        cloudAnim.start();

/*        // Get the background, which has been compiled to an AnimationDrawable object.
        AnimationDrawable frameAnimation = (AnimationDrawable) img.getBackground();

        // Start the animation (looped playback by default).
        frameAnimation.start();*/

/*        ValueAnimator anim = ValueAnimator.ofFloat(0, 1);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float slideOffset = (Float) valueAnimator.getAnimatedValue();
                img.onDrawerSlide(drawerLayout, slideOffset);
            }
        });
        anim.setInterpolator(new DecelerateInterpolator());
// You can change this duration to more closely match that of the default animation.
        anim.setDuration(500);
        anim.start();*/

/*        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.UPDATE_DATA_INTENT);
        FragmentIntentReceiver fragmentIntentReceiver = new FragmentIntentReceiver();
        getActivity().registerReceiver(fragmentIntentReceiver, intentFilter);*/

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    public void setModule(Module module) {
        mModule = module;
    }


    public class FragmentIntentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(Constants.UPDATE_DATA_INTENT)) {
                Log.d(TAG, "Received UPDATE_DATA_INTENT ");
            }
        }
    }

    public void setArtikImage(ImageView artikImage) {
        if (mModule != null) {
            switch (mModule.getType()) {
                case Constants.ARTIK_0:
                    artikImage.setImageResource(R.drawable.a0_identified);
                    break;
                case Constants.ARTIK_5:
                    artikImage.setImageResource(R.drawable.a5_identified);
                    break;
                case Constants.ARTIK_7:
                    artikImage.setImageResource(R.drawable.a7_identified);
                    break;
                case Constants.ARTIK_10:
                    artikImage.setImageResource(R.drawable.a10_identified);
                    break;
                default:
                    artikImage.setImageResource(R.drawable.a5_identified);
                    break;

            }
        }
    }

}
