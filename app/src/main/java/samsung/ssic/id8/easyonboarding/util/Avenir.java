package samsung.ssic.id8.easyonboarding.util;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by v.nandanavan on 3/9/16.
 */
public class Avenir {

    private static Avenir instance;
    private static Typeface ultraLight;
    private static Typeface bold;
    private static Typeface boldItalic;
    private static Typeface demiBoldItalic;
    private static Typeface heavy;
    private static Typeface heavyItalic;
    private static Typeface italic;
    private static Typeface medium;
    private static Typeface mediumItalic;
    private static Typeface regular;
    private static Typeface ultraLightItalic;

    public static Avenir getInstance(Context context) {
        synchronized (Avenir.class) {
            if (instance == null) {
                instance = new Avenir();
                ultraLight = Typeface.createFromAsset(context.getResources().getAssets(), "AvenirNext-UltraLight.ttf");
                bold = Typeface.createFromAsset(context.getResources().getAssets(), "AvenirNext-Bold.ttf");
                boldItalic = Typeface.createFromAsset(context.getResources().getAssets(), "AvenirNext-BoldItalic.ttf");
                demiBoldItalic = Typeface.createFromAsset(context.getResources().getAssets(), "AvenirNext-DemiBoldItalic.ttf");
                heavy = Typeface.createFromAsset(context.getResources().getAssets(), "AvenirNext-Heavy.ttf");
                heavyItalic = Typeface.createFromAsset(context.getResources().getAssets(), "AvenirNext-HeavyItalic.ttf");
                italic = Typeface.createFromAsset(context.getResources().getAssets(), "AvenirNext-Italic.ttf");
                medium = Typeface.createFromAsset(context.getResources().getAssets(), "AvenirNext-Medium.ttf");
                mediumItalic = Typeface.createFromAsset(context.getResources().getAssets(), "AvenirNext-MediumItalic.ttf");
                regular = Typeface.createFromAsset(context.getResources().getAssets(), "AvenirNext-Regular.ttf");
                ultraLightItalic = Typeface.createFromAsset(context.getResources().getAssets(), "AvenirNext-UltraLightItalic.ttf");


            }
            return instance;
        }
    }

    public static Typeface getHeavy() {
        return heavy;
    }

    public static Typeface getMedium() {
        return medium;
    }

    public Typeface getUltraLight() {
        return ultraLight;
    }

    public Typeface getBold() {
        return bold;
    }

    public Typeface getBoldItalic() {
        return boldItalic;
    }

    public Typeface getDemiBoldItalic() {
        return demiBoldItalic;
    }

    public Typeface getHeavyItalic() {
        return heavyItalic;
    }

    public Typeface getItalic() {
        return italic;
    }

    public Typeface getMediumItalic() {
        return mediumItalic;
    }

    public Typeface getRegular() {
        return regular;
    }

    public Typeface getUltraLightItalic() {
        return ultraLightItalic;
    }
}