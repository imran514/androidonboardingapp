package samsung.ssic.id8.easyonboarding.edgenode;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;

import samsung.ssic.id8.easyonboarding.R;
import samsung.ssic.id8.easyonboarding.control.Module;
import samsung.ssic.id8.easyonboarding.util.Constants;

/**
 * Created by v.nandanavan on 4/6/16.
 */
public class ModuleListAdapter extends ArrayAdapter {
    private Activity mActivity;
    private int selectedIndex = -1;

    private List<Module> mModules;

    public ModuleListAdapter(Context context, List<Module> modules) {
        super(context, 0, modules);
        mActivity = (Activity) context;
        mModules = modules;
    }

    public int getCount() {
        return mModules.size();
    }

    public Module getItem(int position) {
       return mModules.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView moduleImage;
        TextView moduleName;
        RadioButton radioButton;
        Module module = mModules.get(position);
        if (convertView == null) {
            convertView = mActivity.getLayoutInflater().inflate(R.layout.module_grid_item, parent, false);
        }
        moduleImage = (ImageView) convertView.findViewById(R.id.moduleImageView);
        moduleName = (TextView) convertView.findViewById(R.id.moduleName);
        radioButton = (RadioButton) convertView.findViewById(R.id.moduelRadioBtn);


        moduleName.setText(module.getName());
        setArtikImage(moduleImage, module);
        if (selectedIndex == position) {
            radioButton.setChecked(true);
        } else {
            radioButton.setChecked(false);
        }
        return convertView;
    }


    public void setArtikImage(ImageView artikImage, Module module) {
        switch (module.getType()) {
            case Constants.ARTIK_0:
                artikImage.setImageResource(R.drawable.a0_identified);
                break;
            case Constants.ARTIK_5:
                artikImage.setImageResource(R.drawable.a5_identified);
                break;
            case Constants.ARTIK_7:
                artikImage.setImageResource(R.drawable.a7_identified);
                break;
            case Constants.ARTIK_10:
                artikImage.setImageResource(R.drawable.a10_identified);
                break;
            default:
                artikImage.setImageResource(R.drawable.a5_identified);
                break;

        }
    }

    public void setSelectedIndex(int index) {
        selectedIndex = index;
    }

}