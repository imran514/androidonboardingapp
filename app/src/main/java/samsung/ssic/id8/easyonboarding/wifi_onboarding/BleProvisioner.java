package samsung.ssic.id8.easyonboarding.wifi_onboarding;

import android.annotation.TargetApi;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.ParcelUuid;
import android.util.Log;
import android.widget.Toast;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import samsung.ssic.id8.easyonboarding.R;
import samsung.ssic.id8.easyonboarding.util.Constants;


@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class BleProvisioner extends Service {

    private final IBinder mBinder = new LocalBinder();
    private Context mContext;
    private Handler mHandler;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothDevice mDevice;
    private BluetoothGatt mBluetoothGatt;
    private BleBlockingQueue mBleBlockingQueue;
    private Boolean mRetvalue = false;
    private State mState = State.DISABLED;
    private State mSavedState;
    private String TAG = "BleProvisioner";
    //For Kitkat.
    private BluetoothAdapter.LeScanCallback leScanCallback;

    //For Lollipop.
    private ScanCallback mLeScanBack;
    private static String mDeviceAddress;

    private UUID mServiceUUID;
    private int mRssiThreshold;
    private int mRegistrationState = 0;
    private ScanSettings settings;
    private List<ScanFilter> filters;
    private String mModuleMacAddress;

    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if ((status==133)||(status==257)) {
                    Log.d(TAG, "Unrecoverable error 133 or 257. DEVICE_DISCONNECTED intent broadcast with full reset");
            }
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                mState = State.CONNECTED;
                mBluetoothGatt = gatt;
                Log.i(TAG, "Connected to GATT server : "+mRegistrationState);
                if(mRegistrationState == Constants.REGIS_STATE_GET_CHALLENGE_PIN){
                    Log.i(TAG, "mRegistrationState : REGIS_STATE_GET_CHALLENGE_PIN. Get ChallengePin");
                    writeCharacteristic(OnboardingGattService.START_REGISTRATION_UUID, "1");
                    Runnable task = new Runnable() {
                        public void run() {
                           readCharacteristic(OnboardingGattService.CHALLENGE_PIN_UUID);
                        }
                    };
                    //worker.schedule(task, 5, TimeUnit.SECONDS);
                    //We will receive Notitification when challenge pin is available

                } else if (mRegistrationState == Constants.REGIS_STATE_GET_DEVICE_DID) {
                    //GATT reconnected. So request DeviceToken
                    Runnable task = new Runnable() {
                        public void run() {
                            readCharacteristic(OnboardingGattService.DEVICE_ID_CHARACTERISTIC_UUID);
                        }
                    };
                    worker.schedule(task, 5, TimeUnit.SECONDS);

                } else if (mRegistrationState == Constants.REGIS_STATE_COMPLETED) {

                }  else if (mRegistrationState == Constants.REGIS_STATE_GET_DEVICE_TYPE_ID){
                    readCharacteristic(OnboardingGattService.DEVICE_TYPE_ID_UUID);
                }  else {
                    mHandler.sendEmptyMessage(Constants.GATT_CONNECTED);
                }

                //TODO: BluetoothLeService does mBluetoothGatt.discoverServices()) here

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                if (mState == State.DISCONNECTING) {
                    mState = State.DISCONNECTED;

                    Log.i(TAG, "Correctly disconnected from GATT server.");
                    mHandler.sendEmptyMessage(Constants.GATT_DISCONNECTED);
                } else {
                    if(mRegistrationState >= Constants.REGIS_STATE_GET_DEVICE_DID) {
                        mHandler.sendMessage(Message.obtain(mHandler, Constants.DEVICE_DID_READ, "1232"));
                        mRegistrationState = Constants.REGIS_STATE_COMPLETED;
                    } else {
                        Log.i(TAG, "Something wrong happened and we're disconnected from GATT server.");
                        mHandler.sendEmptyMessage(Constants.GATT_FAILED);
                        mBluetoothGatt.disconnect();
                    }
                }
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.w(TAG, "onServicesDiscovered!!!: " + status);
                mHandler.sendEmptyMessage(Constants.GATT_SERVICES_DISCOVERED);
            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }



        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.d(TAG, "onCharacteristicRead received: " + status);
                mBleBlockingQueue.newResponse(characteristic);

                final byte[] data = characteristic.getValue();
                if (data != null && data.length > 0) {

                    String value = new String(data);

                    if (characteristic.getUuid().equals(OnboardingGattService.CHARACTERISTIC_VENDOR_ID)) {
                        mHandler.sendMessage(Message.obtain(mHandler, Constants.VENDOR_ID_READ, value));
                    } else if (characteristic.getUuid().equals(OnboardingGattService.CHARACTERISTIC_DEVICE_ID)) {
                        mHandler.sendMessage(Message.obtain(mHandler, Constants.DEVICE_ID_READ, value));
                    } else if (characteristic.getUuid().equals(OnboardingGattService.CHARACTERISTIC_IPADDRESS)) {
                        if ((value != null) || (!value.isEmpty()) || (value.contains("INVALID"))) {
                            Log.d(TAG, "Read Characeteristic CHARACTERISTIC_IPADDRESS :" + value);
                            mHandler.sendMessage(Message.obtain(mHandler, Constants.IPADDRESS_READ, value));
                        }
                    } else if (characteristic.getUuid().equals(OnboardingGattService.DEVICE_ID_CHARACTERISTIC_UUID)) {
                        if ((value != null) || (!value.isEmpty())) {
                            Log.d(TAG, "Read Characeteristic DEVICE_DID_UUID :" + value);
                            mHandler.sendMessage(Message.obtain(mHandler, Constants.DEVICE_DID_READ, value));
                            mRegistrationState = Constants.REGIS_STATE_COMPLETED;
                        }

                    } else {
/*						final StringBuilder stringBuilder = new StringBuilder(data.length);
                        for(byte byteChar : data)
							stringBuilder.append(String.format("%02X ", byteChar));
						String dataValue = new String(data) + "\n" + stringBuilder.toString();*/

                        Log.d(TAG, "onCharacteristicRead received: " + value + " mRegistrationState : " + mRegistrationState);
                        if (mRegistrationState == Constants.REGIS_STATE_GET_DEVICE_TYPE_ID) {
                            mHandler.sendMessage(Message.obtain(mHandler, Constants.DEVICE_TYPE_ID_READ, value));

                        } //else if (mRegistrationState == Constants.REGIS_STATE_GET_CHALLENGE_PIN) {
                        else if  (characteristic.getUuid().equals(OnboardingGattService.CHALLENGE_PIN_UUID)) {
                            Log.e(TAG , "onCharacteristicRead CHALLENGE_PIN_UUID");
                            //Received Challenge Pin
                            if ((value != null) || (!value.isEmpty()) || (value.contains("INVALID"))) {
                                mHandler.sendMessage(Message.obtain(mHandler, Constants.DEVICE_CHALLENGE_PIN_READ, value));
                                Log.d("TAG", "Read Characeteristic CHALLENGE_PIN :" + value);
                                mRegistrationState = Constants.REGIS_STATE_RECEIVED_CHALLENGE_PIN;
                            }

                        } else if (mRegistrationState == Constants.REGIS_STATE_GET_DEVICE_DID) {
                            if ((value != null) || (!value.isEmpty())) {
                                mHandler.sendMessage(Message.obtain(mHandler, Constants.DEVICE_DID_READ, value));
                                mRegistrationState = Constants.REGIS_STATE_COMPLETED;

                            }
                        }
                    }
                }
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {
            Log.d(TAG, "onCharacteristicChanged received: " + characteristic.getUuid().toString());
            // TODO notifications are not passed to the blocking queue
            // mBleBlockingQueue.newResponse(characteristic);

            final byte[] data = characteristic.getValue();
            if (data != null && data.length > 0) {

                String value = new String(data);

                if (characteristic.getUuid().equals(OnboardingGattService.CHARACTERISTIC_STATUS)) {
                    mHandler.sendMessage(Message.obtain(mHandler, Constants.STATUS_NOTIFIED, value));
                    Log.e(TAG, "status 1=" + value);
                } else if (characteristic.getUuid().equals(OnboardingGattService.CHARACTERISTIC_LONG_STATUS)) {
                    mHandler.sendMessage(Message.obtain(mHandler, Constants.LONG_STATUS_NOTIFIED, value));
                    Log.e(TAG, "status= 2" + value);
                } else {
                    if (mRegistrationState == Constants.REGIS_STATE_GET_DEVICE_TYPE_ID) {
                        Log.d(TAG, "Received REGIS_STATE_GET_DEVICE_TYPE_ID Pin mRegistrationState : " + mRegistrationState);
                        mHandler.sendMessage(Message.obtain(mHandler, Constants.DEVICE_TYPE_ID_READ, value));

                    } //else if (mRegistrationState == Constants.REGIS_STATE_GET_CHALLENGE_PIN) {
                        else if  (characteristic.getUuid().equals(OnboardingGattService.CHALLENGE_PIN_UUID)) {
                        Log.d(TAG, "Received Challenge Pin mRegistrationState : " + mRegistrationState);
                        //Received Challenge Pin
                        if ((value != null) || (!value.isEmpty()) || (value.contains("INVALID"))) {
                            mHandler.sendMessage(Message.obtain(mHandler, Constants.DEVICE_CHALLENGE_PIN_READ, value));
                            mRegistrationState = Constants.REGIS_STATE_RECEIVED_CHALLENGE_PIN;
                            Log.d("TAG", "changed mRegistrationState : " + mRegistrationState + " CHALLENGE_PIN :"+value);
                        } else {
                            Log.d(TAG, "Received Changed Invalid Challenge Pin");
                        }

                    } //else if (mRegistrationState == Constants.REGIS_STATE_GET_DEVICE_TOKEN) {
                        else if  (characteristic.getUuid().equals(OnboardingGattService.DEVICE_ID_CHARACTERISTIC_UUID)) {
                        if ((value != null) || (!value.isEmpty())) {
                            mHandler.sendMessage(Message.obtain(mHandler, Constants.DEVICE_DID_READ, value));
                            mRegistrationState = Constants.REGIS_STATE_COMPLETED;
                            ;
                            Log.d(TAG, "mRegistrationState : " + mRegistrationState+ " DEVICE_TOKEN :"+value);
                        }
                    }
                }
            }
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Log.d(TAG, "onCharacteristicWrite received: " + characteristic.getUuid().toString() + "and STATUS: " + status);
            mBleBlockingQueue.newResponse(characteristic);
        }
    };

    private  final ScheduledExecutorService worker =
            Executors.newSingleThreadScheduledExecutor();



    public BleProvisioner() {

    }

    // Constructor
    public BleProvisioner(Context context, Handler handler) {
        mContext = context;
        mHandler = handler;
    }

    private static void reverse(byte[] array) {
        if (array == null) {
            return;
        }
        int i = 0;
        int j = array.length - 1;
        byte tmp;
        while (j > i) {
            tmp = array[j];
            array[j] = array[i];
            array[i] = tmp;
            j--;
            i++;
        }
    }

    public void setRegistrationState(int state) {

        mRegistrationState = state;
        Log.d("WifiOnboarding_Activity", "setRegistrationState : " + mRegistrationState);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // After using a given device, you should make sure that
        // BluetoothGatt.close() is called
        // such that resources are cleaned up properly. In this particular
        // example, close() is
        // invoked when the UI is disconnected from the Service.
        close();
        return super.onUnbind(intent);
    }

    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    public boolean init() {

        Log.d(TAG, "Ble Provisioner init");
        if (mContext == null)
            return false;

        if (!mContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(mContext, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            return false;
        }

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        mDeviceAddress= null;

        mBluetoothGatt = null;

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Toast.makeText(mContext, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!isEnabled())
            mBluetoothAdapter.enable();

        // Create the Blocking Queue with it's own thread and start it.
        mBleBlockingQueue = new BleBlockingQueue();
        mBleBlockingQueue.start();

        mState = State.INITIALIZED;
        Log.d(TAG, "Bluetooth initialized.");
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            //For Jelly Bean MR2 and Kitkat
            leScanCallback = new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
                    if(mState == State.SCANNING && scanRecord != null &&
                            hasServiceUUID(scanRecord)) {
                        //if (hasServiceUUID(result.getScanRecord().getBytes())) { // If device advertise searched UUID
                        Log.d(TAG, "Found an onboarding device!");
                        stopScanLeDevices();
                        mDevice = device;
                        mHandler.sendEmptyMessage(Constants.DETECTED_DEVICE);
                    } else if (device.getName() != null && device.getName().equals("Artik Cloud Secure Regi")) {
                        Log.d(TAG, "Found SDR Device");
                        stopScanLeDevices();
                        mDevice = device;
                        mHandler.sendMessage(Message.obtain(mHandler, Constants.DETECTED_SDR_DEVICE, device.getName()));
                    }
                }
            };
        } else {
            //For Lollipop.
            settings = new ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                    .build();
            filters = new ArrayList<ScanFilter>();
            //ScanFilter filter = new ScanFilter.Builder().setServiceUuid(ParcelUuid.fromString(OnboardingGattService.SERVICE_UUID.toString())).build();

            mLeScanBack = new ScanCallback() {
                @Override
                public void onScanResult(int callbackType, ScanResult result) {
                    super.onScanResult(callbackType, result);

                    if(mState == State.SCANNING && result.getScanRecord().getServiceUuids() != null &&
                            result.getScanRecord().getServiceUuids().toString().equals("[ffffffff-c0c1-ffff-c0c1-201401000000]")) {
                        //if (hasServiceUUID(result.getScanRecord().getBytes())) { // If device advertise searched UUID
                        Log.d(TAG, "Found an onboarding device!");
                        stopScanLeDevices();
                        result.getDevice().getAddress();
                        mDevice = result.getDevice();
                        mHandler.sendEmptyMessage(Constants.DETECTED_DEVICE);
                    } else if (result.getDevice().getName() != null && result.getDevice().getName().equals("Artik Cloud Secure Regi")) {
                        Log.d(TAG, "Found SDR Device");
                        stopScanLeDevices();
                        mDevice = result.getDevice();
                        mHandler.sendMessage(Message.obtain(mHandler, Constants.DETECTED_SDR_DEVICE, result.getDevice().getName()));
                    }
                }

                @Override
                public void onBatchScanResults(List<ScanResult> results) {
                    super.onBatchScanResults(results);
                }

                @Override
                public void onScanFailed(int errorCode) {
                    super.onScanFailed(errorCode);
                }
            };
        }

        return true;
    }

    public boolean isEnabled() {

        if (mBluetoothAdapter == null)
            return false;

        return mBluetoothAdapter.isEnabled();
    }
	
/*	private boolean deviceIsNexus4() {
		if (Build.MODEL.equals("Nexus 4"))
			return true;
		return false;
	}
*/

    public void setModuleMacAddress(String moduleMacAddress) {
        this.mModuleMacAddress = moduleMacAddress;
    }

    public void startScanLeDevices(UUID uuid, int rssiThreshold) {
        if(uuid == null) {
            Log.d(TAG, "startScanLeDevices is null" );
        } else {
            Log.d(TAG, "startScanLeDevices UUID :" +uuid.toString());
        }
        mServiceUUID = uuid;
        mRssiThreshold = rssiThreshold;

        startScanLeDevices();
    }

    public void startScanLeDevices() {
		
/*		if (deviceIsNexus4()) {
			mHandler.postDelayed(new Runnable() {
	            @Override
	            public void run() {
	        		startScanLeDevices();
	            }
	        }, SCAN_PERIOD);
		}
*/
        try {
            ScanFilter filter = new ScanFilter.Builder().setDeviceAddress(mModuleMacAddress).build();
            filters.add(filter);
            Log.d(TAG, "LE Device Scan started");
            mState = State.SCANNING;
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                mBluetoothAdapter.startLeScan(leScanCallback);
            } else {
                mBluetoothAdapter.getBluetoothLeScanner().startScan(filters, settings, mLeScanBack);
            }
        }catch (IllegalArgumentException exception){
            mHandler.sendEmptyMessage(Constants.ERROR_INVALID_MAC_ADDRESS);
        }

        //mBluetoothAdapter.getBluetoothLeScanner().startScan((ScanCallback) mLeScanBack);
        //mBluetoothAdapter.startLeScan(mLeScanCallback);
    }


    public void stopScanLeDevices() {
        if (mState == State.SCANNING) {
            mState = State.INITIALIZED;
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                mBluetoothAdapter.stopLeScan(leScanCallback);
            } else {
                mBluetoothAdapter.getBluetoothLeScanner().stopScan((ScanCallback) mLeScanBack);
            }

           // mBluetoothAdapter.stopLeScan(mLeScanCallback);
        } else if (mState == State.CONNECTING) {

        } else if (mState == State.CONNECTED) {

        }
    }

    private boolean hasServiceUUID(byte[] scanRecord) {

        int current_entry_idx = 0;
        int data_type_idx = 0;
        int data_start_idx = 0;

        while (current_entry_idx < scanRecord.length) {

            data_type_idx = current_entry_idx + 1;
            data_start_idx = current_entry_idx + 2;

            if (scanRecord[current_entry_idx] > 16 && scanRecord[data_type_idx] == 6) {
                // We found a 128 bits service UUID
                byte[] possibleService = Arrays.copyOfRange(scanRecord, data_start_idx, data_start_idx + 16);
                // We need to reverse the UUID
                BleProvisioner.reverse(possibleService);
                // Build the UUID to compare
                ByteBuffer bb = ByteBuffer.wrap(possibleService);
                UUID posible = new UUID(bb.getLong(), bb.getLong());

                if (posible.equals(mServiceUUID))
                    return true;

            }
            current_entry_idx = current_entry_idx + scanRecord[current_entry_idx] + 1;
        }
        return false;
    }

    public boolean connectGatt() {
        if (mBluetoothAdapter == null || mDevice == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or undetected device.");
            return false;
        }

        if (mBluetoothGatt != null) {
            Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");
            if (mBluetoothGatt.connect()) {
                mState = State.CONNECTING;
                return true;
            } else
                return false;
        } else {

            mBluetoothGatt = mDevice.connectGatt(mContext, true, mGattCallback);
        }
        Log.d(TAG, "Trying to create a new connection.");
        mState = State.CONNECTING;
        return true;
    }

    public void disconnectGatt() {
        Log.e(TAG, "Disconnecting from gatt server.");
        if (mBluetoothAdapter == null || mDevice == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or undetected device.");
        }
        if (mBluetoothGatt != null) {
            try {
                mBluetoothGatt.disconnect();
                mBluetoothGatt.close();
                Log.d(TAG, "Disconnecting from gatt server.");
                mState = State.DISCONNECTING;
            } catch (Exception ex) {
                Log.e(TAG, "Exception in disconnecting from gatt server.");
            }
            ;
        }
        mBluetoothGatt = null;
        mState = State.DISCONNECTING;
    }

    protected BluetoothGattCharacteristic getCharacteristicFromUUID(
            final UUID characteristicUUID) {

        if (mBluetoothAdapter == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return null;
        } else if (mBluetoothGatt == null) {
            Log.w(TAG, "mBluetoothGatt not initialized");
            return null;
        } else if (mServiceUUID == null) {
            Log.w(TAG, "mServiceUUID not initialized");
            return null;
        } else if (characteristicUUID == null) {
            Log.w(TAG, "characteristicUUID not initialized");
            return null;
        }

        BluetoothGattService gattService = mBluetoothGatt.getService(mServiceUUID);

        if (gattService == null) {
            Log.w(TAG, "GATT Service not found for UUID " + mServiceUUID.toString());
            return null;
        }
        // Get Characteristic
        return gattService.getCharacteristic(characteristicUUID);
    }

    public void readCharacteristic(final UUID characteristicUUID) {
        Log.w(TAG, "GATT Service Read Characteristic " + characteristicUUID.toString());
        final BluetoothGattCharacteristic characteristic = getCharacteristicFromUUID(characteristicUUID);

        if (characteristic == null) {
            Log.w(TAG, "Problem getting characteristic from UUID " + characteristicUUID.toString());
            return;
        }
        if (characteristicUUID.equals(OnboardingGattService.CHALLENGE_PIN_UUID)) {
            Log.e(TAG , "readCharacteristic CHALLENGE_PIN_UUID");
            mRegistrationState = Constants.REGIS_STATE_GET_CHALLENGE_PIN;
            Log.d("WifiOnboarding_Activity", "mRegistrationState : " + mRegistrationState);
        } else if (characteristicUUID.equals(OnboardingGattService.DEVICE_TYPE_ID_UUID)) {
            mRegistrationState = Constants.REGIS_STATE_GET_DEVICE_TYPE_ID;
            Log.d("WifiOnboarding_Activity", "mRegistrationState : " + mRegistrationState);
        }
        else if (characteristicUUID.equals(OnboardingGattService.CHARACTERISTIC_IPADDRESS)) {
            Log.e(TAG , "readCharacteristic CHARACTERISTIC_IPADDRESS");
        }
        mBleBlockingQueue.newRequest(characteristic, new Runnable() {
            @Override
            public void run() {
                if (!mBluetoothGatt.readCharacteristic(characteristic))
                    Log.w(TAG, "readCharacteristic not possible for UUID " + characteristicUUID.toString());
            }
        }, true);


    }

    public void setCharacteristicNotification(final UUID characteristicUUID,
                                              final boolean enabled) {

        final BluetoothGattCharacteristic characteristic = getCharacteristicFromUUID(characteristicUUID);

        mBleBlockingQueue.newRequest(characteristic, new Runnable() {
            @Override
            public void run() {
                // Get Service
                try {
                if (!mBluetoothGatt.setCharacteristicNotification(characteristic, enabled))
                    Log.w(TAG, "setCharacteristicNotification not possible for UUID " + characteristicUUID.toString());

                List<BluetoothGattDescriptor> descriptors = characteristic.getDescriptors();
                for (BluetoothGattDescriptor bluetoothGattDescriptor : descriptors) {
                    if (enabled) {
                        if (!bluetoothGattDescriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE))
                            Log.w(TAG, "setValue not possible for UUID " + characteristicUUID.toString());
                    } else {
                        if (!bluetoothGattDescriptor.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE))
                            Log.w(TAG, "setValue not possible for UUID " + characteristicUUID.toString());
                    }
                    if (!mBluetoothGatt.writeDescriptor(bluetoothGattDescriptor))
                        Log.w(TAG, "writeDescriptor not possible for UUID " + characteristicUUID.toString());
                }
            } catch(Exception e) {
                Log.e(TAG,e.getMessage());
            }}
        }, false);
    }

    public boolean writeCharacteristic(final UUID characteristicUUID, final String value) {
        mRetvalue = false;
        final BluetoothGattCharacteristic characteristic = getCharacteristicFromUUID(characteristicUUID);

        if (characteristic == null) {
            Log.w(TAG, "Problem getting characteristic from UUID " + characteristicUUID.toString());
            return false;
        }


        mBleBlockingQueue.newRequest(characteristic, new Runnable() {
            @Override
            public void run() {

                characteristic.setValue(value);

                if (!mBluetoothGatt.writeCharacteristic(characteristic)) {
                    Log.w(TAG, "writeCharacteristic not possible for UUID " + characteristicUUID.toString());

                } else {
                    mRetvalue = true;
                    if (characteristicUUID.equals(OnboardingGattService.COMPLETE_REGISTRATION_UUID)) {
                        Log.d("WifiOnboarding_Activity", "mRegistrationState : " + Constants.REGIS_STATE_GET_DEVICE_DID);
                        mRegistrationState = Constants.REGIS_STATE_GET_DEVICE_DID;
/*                        Runnable task = new Runnable() {
                            public void run() {
                                readCharacteristic(OnboardingGattService.DEVICE_TOKEN_UUID);
                            }
                        };
                       worker.schedule(task, 5, TimeUnit.SECONDS);*/
                        //Status is notified when Device Token is REady

                    }
                }
            }
        }, true);
        return mRetvalue;
    }

    public void writeCharacteristic(final UUID characteristicUUID, final byte[] value) {

        final BluetoothGattCharacteristic characteristic = getCharacteristicFromUUID(characteristicUUID);

        Log.d(TAG, "Writing Chactersitc UUID " + characteristicUUID.toString());
        if (characteristic == null) {
            Log.w(TAG, "Problem getting characteristic from UUID " + characteristicUUID.toString());
            return;
        }

        mBleBlockingQueue.newRequest(characteristic, new Runnable() {
            @Override
            public void run() {

                characteristic.setValue(value);

                if (!mBluetoothGatt.writeCharacteristic(characteristic))
                    Log.w(TAG, "writeCharacteristic not possible for UUID " + characteristicUUID.toString());
            }
        }, true);

    }

    public void pause() {
        mSavedState = mState;
        mState = State.PAUSED;
        // Here depending on the state where we're we should do different things...
        // Stop Scanning
        // Disconnect from Gatt Server
        if (mSavedState == State.SCANNING) {
            stopScanLeDevices();
        } else if (mSavedState == State.CONNECTED ||
                mSavedState == State.CONNECTING) {
            disconnectGatt();
        }

    }

    public void stop() {
        mSavedState = null;

        if (mSavedState == State.SCANNING) {
            stopScanLeDevices();
        } else if (mSavedState == State.CONNECTED ||
                mSavedState == State.CONNECTING) {
            disconnectGatt();
        }
        mRegistrationState = Constants.REGIS_STATE_START;
        mState = State.DISABLED;
    }

    public void resume() {
        if (mState == State.PAUSED) {

            // Then here we need to recover and get to the state were we were before
            // Start Scan again if scanning
            // Connect to GATT Server if connected

            if (mSavedState == State.SCANNING) {
                startScanLeDevices();
            } else if (mSavedState == State.CONNECTED ||
                    mSavedState == State.CONNECTING) {
                connectGatt();
            }
            mSavedState = null;
        } else if (mState == State.CONNECTED || mState == State.FAILED) {
            connectGatt();
        }
    }

    public boolean discoverServices() {
        if (mBluetoothAdapter == null || mDevice == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or undetected device.");
            return false;
        }

        if (mBluetoothGatt != null) {
            mBluetoothGatt.discoverServices();
            return true;
        }
        return false;
    }

    public void executeWrite(UUID characteristicUUID) {
        int retry = 0;
        wait(500);

        boolean valueWritten = writeCharacteristic(characteristicUUID, "1");

        do {
            // try wait for total of 9 seconds max for value to be written
            if (retry == 3) {
                Log.i("WriteCommand", "Error writing value to BLE device");
                break;
            }
            wait(3000);
            retry++;
        } while (!valueWritten);
    }

    public void wait(int milliseconds) {
        try {
            synchronized (Thread.currentThread()) {
                Thread.currentThread().wait(milliseconds);
            }
        } catch (InterruptedException e) {
            //ignore
        }
    }

/*	private void readCharacteristicValue(String data) {
		if (data != null) {
			if (mRegistrationState == WifiOnboarding_Activity.REGIS_STATE_GET_DEVICE_TYPE_ID) {
				mDeviceTypeId = data;
			} else if (mRegistrationState ==  WifiOnboarding_Activity.REGIS_STATE_GET_CHALLENGE_PIN) {
				mChallengePin = data;
			} else if (mRegistrationState == WifiOnboarding_Activity.REGIS_STATE_GET_DEVICE_TOKEN) {
				mDeviceToken = data;
			}
		}
		// dequeueCommand();
	}*/

    private enum State {
        DISABLED,        // Provisioner not started or released
        INITIALIZED,    // Provisioner started
        SCANNING,        // BLE interface is ON and scanning
        CONNECTING,    // Device found trying to connect
        CONNECTED,        // Connected as central device
        DISCONNECTING,  // Disconnecting
        DISCONNECTED,   // Disconnected
        FAILED,            // Provisioner has failed
        PAUSED;            // Provisioner is paused
    }

    public class LocalBinder extends Binder {
        BleProvisioner getService(Context context, Handler handler) {
            mContext = context;
            mHandler = handler;
            return BleProvisioner.this;
        }
    }

    public void setmServiceUUID(UUID mServiceUUID) {
        this.mServiceUUID = mServiceUUID;
    }

}
