package samsung.ssic.id8.easyonboarding.websocket;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import io.samsungsami.android.Sami;
import io.samsungsami.android.api.Call;
import io.samsungsami.android.api.Callback;
import io.samsungsami.android.api.Cast;
import io.samsungsami.android.api.Code;
import io.samsungsami.client.ApiException;
import io.samsungsami.model.Device;
import io.samsungsami.model.DeviceEnvelope;
import io.samsungsami.model.DeviceType;
import io.samsungsami.model.DeviceTypeArray;
import io.samsungsami.model.DeviceTypesEnvelope;
import samsung.ssic.id8.easyonboarding.R;
import samsung.ssic.id8.easyonboarding.samplecode.SamiServers;

public class OnBoardOtherDevices extends Activity implements OnItemSelectedListener {

    ArrayAdapter<String> adapter;
    Hashtable<String, String> deviceTypeIdMap = new Hashtable<String, String>();
    private Spinner spinner;
    private Button mConnectButton;
    private Sami sami;
    private EditText deviceName;
    private String deviceType, myDeviceName, deviceTypeId;
    private List<String> listOfdeviceType = new ArrayList<>();
    private WebsocketPipe live;
    private WebsocketPipe websocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_board_other_devices);
        spinner = (Spinner) findViewById(R.id.spinner);

        getDevicesTypes();

        adapter = new ArrayAdapter<String>(OnBoardOtherDevices.this,
                android.R.layout.simple_spinner_item, listOfdeviceType);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        deviceName = (EditText) findViewById(R.id.deviceName);

        mConnectButton = (Button) findViewById(R.id.button4);
        mConnectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Device device = createDevice();
                if (device != null) {
                    Log.i("OnBoardOtherDevices", "Device created");
                }
                String sdid = device.getId();
                if (sdid != null) {
                    connectWebsockets(sdid);
                }
            }
        });
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        return;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
        switch (position) {
            case 0:
            case 1:
            case 2:
                break;
            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SAMIdisconnect();
    }

    /**
     * Closes websocket connections
     */
    void SAMIdisconnect() {
        if (live != null) {
            live.disconnect();
        }
        if (websocket != null) {
            websocket.disconnect();
        }
    }

    /**
     * Creates websocket connections for /websocket and /live pipes
     * Shows how the listener could be inline or in an external file
     */
    void connectWebsockets(String sdid) {
        Log.i("SDID", sdid);

        if (live != null) {
            live.disconnect();
        }
        live = new WebsocketPipe(this, "live", null);
        live.connect(sami.getLiveConnectionString());

        if (websocket != null) {
            websocket.disconnect();
        }
        websocket = new WebsocketPipe(this, "websocket", sami.getWebsocketRegisterMessage(sdid));
        websocket.connect(sami.getWebsocketConnectionString(true));
    }

    /**
     * Registers a new device in SAMI
     *
     * @return
     */
    Device createDevice() {
        final String uid = sami.getCredentials().getId();
        myDeviceName = deviceName.getText().toString();
        deviceType = spinner.getSelectedItem().toString();
        deviceTypeId = deviceTypeIdMap.get(deviceType);
        if (myDeviceName.isEmpty() || myDeviceName.equals("")) {
            myDeviceName = "My " + deviceType;
        }
        Object result = new Call(new Code() {

            @Override
            public Object run() {
                Device device = new Device();
                device.setDtid(deviceTypeId);
                device.setName(myDeviceName);
                device.setUid(uid);
                try {
                    DeviceEnvelope envelope = sami.getDevicesApi()
                            .addDevice(device);
                    device = envelope.getData();
                } catch (ApiException e) {
                    e.printStackTrace();
                }
                return device;
            }
        }).executeInSync();
        return Cast.as(Device.class, result);
    }


    /**
     * Makes a non blocking REST call to get the device type of the current user
     */
    public List<String> getDevicesTypes() {
        // final ArrayList<String> deviceNameList = new ArrayList<>();
        sami = Sami.setInstance(this, SamiServers.getDefaultConfig());
        new Call(new Code() {

            @Override
            public Object run() {
                try {
                    // Get all device type from SAMI that can use cloud auth
                    DeviceTypesEnvelope deviceTypeEnvelope = sami.getDeviceTypesApi()
                            .getDeviceTypes("", 0, 100);

                    // If you want to get all device type for particular user that can use cloud
                    // auth - use getUsersApi().getUserDeviceTypes(0, 50, false,
                    // sami.getCredentials().getId());

                    DeviceTypeArray deviceTypeArray = deviceTypeEnvelope.getData();
                    List<DeviceType> devicetypeList = deviceTypeArray.getDeviceTypes();

                    for (int deviceTypeIndex = 0; deviceTypeIndex < devicetypeList.size(); deviceTypeIndex++) {
                        DeviceType deviceType = devicetypeList.get(deviceTypeIndex);
                        if (!(deviceType.getHasCloudConnector()) && !(deviceType.getRsp())) {
                            listOfdeviceType.add(deviceType.getName());
                            deviceTypeIdMap.put(deviceType.getName(), deviceType.getId());
                        }
                    }
                    return listOfdeviceType;
                } catch (ApiException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }, new Callback() {

            @Override
            public void onApiResult(Object result) {
                if (listOfdeviceType.size() != 0) {
                    adapter.notifyDataSetChanged();
                }
            }
        }).execute();
        return listOfdeviceType;
    }

}