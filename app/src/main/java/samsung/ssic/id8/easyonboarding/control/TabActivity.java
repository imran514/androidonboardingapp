package samsung.ssic.id8.easyonboarding.control;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;

import java.util.ArrayList;

import io.samsungsami.android.Credentials;
import io.samsungsami.android.Sami;
import io.samsungsami.api.MessagesApi;
import samsung.ssic.id8.easyonboarding.EasyOnboardingApp;
import samsung.ssic.id8.easyonboarding.LED.SAMISession;
import samsung.ssic.id8.easyonboarding.LoginActivity;
import samsung.ssic.id8.easyonboarding.R;
import samsung.ssic.id8.easyonboarding.device_onboarding.DeviceListObject;
import samsung.ssic.id8.easyonboarding.samplecode.SAMISessionConstants;
import samsung.ssic.id8.easyonboarding.tabs.Artik0FragmentTab;
import samsung.ssic.id8.easyonboarding.tabs.Artik10FragmentTab;
import samsung.ssic.id8.easyonboarding.tabs.Artik1FragmentTab;
import samsung.ssic.id8.easyonboarding.tabs.Artik5FragmentTab;
import samsung.ssic.id8.easyonboarding.tabs.Artik7FragmentTab;
import samsung.ssic.id8.easyonboarding.tabs.GroupsFragmentTab;
import samsung.ssic.id8.easyonboarding.util.AlertDialogList;
import samsung.ssic.id8.easyonboarding.util.Constants;

public class TabActivity extends AppCompatActivity {

    /*public static final int GROUPS = 0;
    public static final int A10 = 1;
    public static final int A5 = 2;
    public static final int A0 = 3;
    public static final int A1 = 4;
    public static final int A7 = 5;
*/
    public static final int A7 = 0;
    public static final int A5 = 1;
    public static final int A0 = 2;
    //public static final String CLIENT_ID = "9ad2304b998d4da0bf1b6b145eaa9f64";  // Night Hawk
    private static final int RC_BARCODE_CAPTURE = 9001;
    private static final String SAMIHUB_BASE_PATH = "https://api.samsungsami.io/v1.1";
    private static final String DEVICE_ID = "1915399dd24c4d1d867386e248e90ff6"; // Motion Sensor
    //private static final String SRC_DEVICE_ID = "209925947478483fbeb4f7aac3f39ea3"; // Night Hawk Android Phone
    private static final String SAMI_AUTH_BASE_URL = "https://accounts.samsungsami.io";
    private static final String CLIENT_ID = "724614bd57d24d7ba18323301570e0f0";  // Artik Control App
    private static final String REDIRECT_URL = "android-app://redirect";


    private static final String WS_HEADER = "WebSocket /websocket: ";
    private static final String LIVE_HEADER = "WebSocket /live: ";
    private static final String DEVICE_REGISTERED = "device registered ";
    private static final String CONNECTED = "connected ";

    //private FragmentTabHost mTabHost;
    public static AlertDialogList mADL;
    public static String TAG = "TabActivity";

    private static MessagesApi mMessagesApi = null;

    Receiver mReceiver;
    FloatingActionButton mFab;
    private int TAB_ITEMS = 3;
    private TextView statusMessage;
    private TextView barcodeValue;
    private Artik0FragmentTab mArtik0FragmentTab = null;
    private Artik1FragmentTab mArtik1FragmentTab = null;
    private Artik5FragmentTab mArtik5FragmentTab = null;
    private Artik10FragmentTab mArtik10FragmentTab = null;
    private Artik7FragmentTab mArtik7FragmentTab = null;
    private GroupsFragmentTab mArtikGroupsFragmentTab = null;
    private LinearLayout mSlidingTabsLayout;
    private TextView welcomeText;
    private ProgressDialog mProgressDialog;
    private boolean mRequiresCloudConnector = false;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;

    //  private UsersApi mUsersApi = null;
    //private String mAccessToken = "6457e8a0a15742c49a9e74bf95f3f0de"; This access token is for LED turning on
    private String mAccessToken;

    public static void addModules() {
        String[] values = new String[]{"Use QR Code", "Use NFC", "Manual Input"};
        mADL.show("Add new Artik Module", values, values.length, Constants.MANUAL_ENTRY);
    }

/*    public static void cmdLightON() {
        try {
            Log.v(TAG, ": send ON button is clicked.");

            //new PostLightCommand(true).execute(mMessagesApi);
        } catch (Exception e) {
            Log.v(TAG, "Run into Exception");
            e.printStackTrace();
        }
    }

    public static void cmdLightOFF() {
        try {
            Log.v(TAG, ": send OFF button is clicked.");

            //new PostLightCommand(false).execute(mMessagesApi);
        } catch (Exception e) {
            Log.v(TAG, "Run into Exception");
            e.printStackTrace();
        }
    }*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);

        Intent intent = getIntent();

        mAccessToken = intent.getStringExtra(SAMISessionConstants.ACCESS_TOKEN);
        Log.d("Venkat", " TabActivity Access token: " + mAccessToken);
        mSlidingTabsLayout = (LinearLayout) findViewById(R.id.sliding_tabs_layout);
        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayShowCustomEnabled(true);
        mActionBar.setCustomView(R.layout.custom_actionbar);


        // SAMISession.getInstance().setContext(this);
        //LayoutInflater mInflater = LayoutInflater.from(this);

        // View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
//        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);
//        mTitleTextView.setText("My Own Title");

//        ImageButton imageButton = (ImageButton) mCustomView
//                .findViewById(R.id.imageButton);
//        imageButton.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(getApplicationContext(), "Refresh Clicked!",
//                        Toast.LENGTH_LONG).show();
//            }
//        });

        //mActionBar.setCustomView(mCustomView);


        mADL = new AlertDialogList(this);
        mReceiver = new Receiver();
        welcomeText = (TextView) findViewById(R.id.welcome_user_text);

        mArtik5FragmentTab = new Artik5FragmentTab();
        mArtik1FragmentTab = new Artik1FragmentTab();
        mArtik0FragmentTab = new Artik0FragmentTab();
        mArtik10FragmentTab = new Artik10FragmentTab();
        mArtik7FragmentTab = new Artik7FragmentTab();
        mArtikGroupsFragmentTab = new GroupsFragmentTab();

        ViewPager viewPager = (ViewPager) findViewById(R.id.sliding_viewpager);
        //viewPager.setPageTransformer(true, new DepthPageTransformer());
        viewPager.setAdapter(new SlidingPageAdapter(getSupportFragmentManager(), this));

        // Give the TabLayout the ViewPager
        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
        //requestPermissions(new String[]{ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);

        //mTabHost.setVisibility(View.INVISIBLE);
        mFab = (FloatingActionButton) findViewById(R.id.addNewDevices);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addModules();
/*                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });


        registerReceiver(mReceiver, makeInteractionIntent());

    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("Venkat", "coarse location permission granted");
                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Functionality limited");
                    builder.setMessage("Since location access has not been granted, this app will not be able to discover beacons when in the background.");
                    builder.setPositiveButton(android.R.string.ok, null);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

                            @Override
                            public void onDismiss(DialogInterface dialog) {
                            }

                        });
                    }
                    builder.show();
                }
                return;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            try {

                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                Bundle bundle = new Bundle();
                bundle.putBoolean("logout", true);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
            }
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    public void startCapture(int selection) {
        switch (selection) {
            case Constants.MANUAL_ENTRY:
                Intent activtyIntent = new Intent(TabActivity.this, Manual_CaptureActivity.class);
                activtyIntent.putExtra("SELECTION", selection);
                activtyIntent.putExtra(SAMISessionConstants.ACCESS_TOKEN, mAccessToken);
                startActivity(activtyIntent);
                break;
            case Constants.NFC_TAG:
                Intent activtyIntent1 = new Intent(TabActivity.this, NFC_CaptureActivity.class);
                activtyIntent1.putExtra("SELECTION", selection);
                activtyIntent1.putExtra(SAMISessionConstants.ACCESS_TOKEN, mAccessToken);
                startActivity(activtyIntent1);
                break;
            case Constants.QR_CODE:
                //Intent intent = new Intent(this, WifiOnboarding_Activity.class);
                Intent intent = new Intent(this, QRC_CaptureActivity.class);
                intent.putExtra(QRC_CaptureActivity.AutoFocus, true);
                intent.putExtra(QRC_CaptureActivity.UseFlash, false);

                intent.putExtra(SAMISessionConstants.ACCESS_TOKEN, mAccessToken);
                startActivity(intent);
                break;
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mReceiver, makeInteractionIntent());
        showProgressDialog();
        ((EasyOnboardingApp) getApplication()).getUserDeviceFromCloud(new EasyOnboardingApp.UserDevicesFetchedListener() {
            @Override
            public void onUserDevicesFetched() {
                hideProgressDialog();
            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (mReceiver != null) {
                unregisterReceiver(mReceiver);
            }

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_BARCODE_CAPTURE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(QRC_CaptureActivity.BarcodeObject);
                    statusMessage.setText(R.string.barcode_success);
                    barcodeValue.setText(barcode.displayValue);
                    Log.d(TAG, "Barcode read: " + barcode.displayValue);
                } else {
                    statusMessage.setText(R.string.barcode_failure);
                    Log.d(TAG, "No barcode captured, intent data is null");
                }
            } else {
                statusMessage.setText(String.format(getString(R.string.barcode_error),
                        CommonStatusCodes.getStatusCodeString(resultCode)));
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void setupSamihubApi() {
        // Invoke the appropriate API
/*        mUsersApi = new UsersApi();
        mUsersApi.setBasePath(SAMIHUB_BASE_PATH);
        mUsersApi.addHeader("Authorization", "bearer " + mAccessToken);*/

        mMessagesApi = new MessagesApi();
        mMessagesApi.setBasePath(SAMIHUB_BASE_PATH);
        mMessagesApi.addHeader("Authorization", "bearer " + mAccessToken);
    }


    /*
        REMOTE CALLS TO SAMI
     */

    public void getA5DeviceList() {

    }


    public class Receiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(Constants.ADD_DEVICES_DIALOG_CANCEL)) {

                //mFab.setChecked(false);

            } else if (intent.getAction().equals(Constants.MODULE_ONBOARDED)) {
                Log.d(TAG, "MODULE_ONBOARDED intent ");

                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    Module module = (Module) bundle.get(Constants.NEW_MODULE);
                    if (module != null) {
                        EasyOnboardingApp easyOnboardingApp = (EasyOnboardingApp) getApplication();
                        easyOnboardingApp.addModule(module);
                        Log.d(TAG, "Received module :  " + module.getName());
                    } else {
                        Log.d(TAG, "Received null object  intent ");
                    }
                } else {
                    Log.d(TAG, "Received bundle object  null ");
                }
            } else if (intent.getAction().equals(Constants.ADD_DEVICES_DIALOG_SELECTED)) {
                int selection = intent.getIntExtra("SELECTION", 1);
                Log.d(TAG, "selection " + selection);


                //mFab.setChecked(false);
                startCapture(selection);

            } else if (intent.getAction().equals(Constants.LED_ON)) {
                Log.d(TAG, "LED On ");
                try {
                    Log.v(TAG, ": on button is clicked.");
                    SAMISession.getInstance().sendOnActionInDeviceChannelWS();
                } catch (Exception e) {
                    Log.v(TAG, "Run into Exception :" + e.getMessage());
                    e.printStackTrace();
                }


            } else if (intent.getAction().equals(Constants.LED_OFF)) {
                Log.d(TAG, "LED Off ");
                try {
                    Log.v(TAG, ": off button is clicked.");
                    SAMISession.getInstance().sendOffActionInDeviceChannelWS();
                } catch (Exception e) {
                    Log.v(TAG, "Run into Exception :" + e.getMessage());
                    e.printStackTrace();
                }

            }
        }
    }

    public class SlidingPageAdapter extends FragmentPagerAdapter {
        private Context context;
        private String tabTitles[] = new String[]{ "A7", "A5", "A0"};

        public SlidingPageAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context = context;
        }

        @Override
        public int getCount() {
            return TAB_ITEMS;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            return tabTitles[position];
        }


        @Override
        public Fragment getItem(int position) {

            Log.d(TAG, "SlidingPageAdapter " + position);
            switch (position) {

               /* case GROUPS:
                    if (mArtikGroupsFragmentTab == null)
                        mArtikGroupsFragmentTab = new GroupsFragmentTab();
                    return mArtikGroupsFragmentTab;

                case A10:
                    if (mArtik10FragmentTab == null)
                        mArtik10FragmentTab = new Artik10FragmentTab();
                    return mArtik10FragmentTab;*/

                case A5:
                    if (mArtik5FragmentTab == null)
                        mArtik5FragmentTab = new Artik5FragmentTab();
                    return mArtik5FragmentTab;
                /*case A1:
                    if (mArtik1FragmentTab == null)
                        mArtik1FragmentTab = new Artik1FragmentTab();
                    return mArtik1FragmentTab;*/
                case A0:
                    if (mArtik0FragmentTab == null)
                        mArtik0FragmentTab = new Artik0FragmentTab();
                    return mArtik0FragmentTab;
                case A7:
                    if (mArtik7FragmentTab == null)
                        mArtik7FragmentTab = new Artik7FragmentTab();
                    return mArtik7FragmentTab;


                default:
                    if (mArtikGroupsFragmentTab == null)
                        mArtikGroupsFragmentTab = new GroupsFragmentTab();
                    return mArtikGroupsFragmentTab;

            }
        }
    }

    private static IntentFilter makeInteractionIntent() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ADD_DEVICES_DIALOG_CANCEL);
        intentFilter.addAction(Constants.ADD_DEVICES_DIALOG_SELECTED);
        intentFilter.addAction(Constants.MODULE_ONBOARDED);
        return intentFilter;
    }


    public void hideProgressDialog() {
        if (mProgressDialog != null & mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void showProgressDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle("Loading");
        mProgressDialog.setMessage("Getting user devices..");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

    }

}
