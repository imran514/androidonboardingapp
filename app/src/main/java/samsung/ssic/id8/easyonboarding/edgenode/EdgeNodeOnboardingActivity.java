package samsung.ssic.id8.easyonboarding.edgenode;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.samsungsami.android.Credentials;
import io.samsungsami.android.Sami;
import samsung.ssic.id8.easyonboarding.EasyOnboardingApp;
import samsung.ssic.id8.easyonboarding.LED.MainActivity;
import samsung.ssic.id8.easyonboarding.R;
import samsung.ssic.id8.easyonboarding.control.Module;
import samsung.ssic.id8.easyonboarding.device_onboarding.DeviceListObject;
import samsung.ssic.id8.easyonboarding.util.Constants;

/**
 * Created by i.mohammed on 8/18/16.
 */
public class EdgeNodeOnboardingActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "EdgeNodeOnboarding";

    private static final int ERROR_HUB_NOT_SELECTED = 111;
    private static final int ERROR_RIGISTERATION = 112;

    private static final String DEVICE_TYPE = "dtffe018d82ab24f2981dea0307a630f71";
    private static final String IP_ADDRESS = "IP_ADDRESS";

    private String RIGISTER_URL = "http://IP_ADDRESS:80/v1.0/service/AKCProvision";
    private String MODE_URL = "http://IP_ADDRESS:80/v1.0/service";
    private String DEVICES_URL = "http://IP_ADDRESS:80/v1.0/devices";
    private String USER_DETAILS_URL = "http://IP_ADDRESS:80/v1.0/service/conf";

    private Module mEdgeNode;
    private Module mEdgeNodeHub;
    private ModuleListAdapter mModuleListAdapter;
    private Dialog mErrorDialog;
    private RequestQueue queue;

    private EasyOnboardingApp mEasyOnboardingApp;
    private Handler mHandler;
    private ProgressDialog mProgressDialog;
    private ArrayList<DeviceListObject> mDevicesOnCloud = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_module_list);
        queue = Volley.newRequestQueue(this);
        mHandler = new Handler();

        mEasyOnboardingApp = (EasyOnboardingApp) getApplication();
        mEdgeNode = getmModuleInfo();
        Log.e(TAG, "ID = " + mEdgeNode.getDeviceUUID());
        ListView gridview = (ListView) findViewById(R.id.moduleListview);
        Button linkModule = (Button) findViewById(R.id.linkModuleBtn);
        linkModule.setOnClickListener(this);
        mDevicesOnCloud  = mEasyOnboardingApp.getUserDevices();
        mModuleListAdapter = new ModuleListAdapter(this, new ArrayList<Module>());
        List<Module> modules = mEasyOnboardingApp.getModules();
        for (Object object : modules) {
            Module module = (Module)object;
            if (module.getType() == Constants.ARTIK_5 || module.getType() == Constants.ARTIK_7) {
                if(isRegisteredOnCloud(module)) {
                    mModuleListAdapter.add(module);
                }
            }
        }
        gridview.setAdapter(mModuleListAdapter);

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                mModuleListAdapter.setSelectedIndex(position);
                mModuleListAdapter.notifyDataSetChanged();
                mEdgeNodeHub = mModuleListAdapter.getItem(position);
                Log.e(TAG, "ip = " + mEdgeNodeHub.getIPAddress());

            }
        });

        initProgressDialog();
    }

    private boolean isRegisteredOnCloud(Module currentModule){
        for (DeviceListObject device : mDevicesOnCloud){
            if(device.getDeviceId().equals(currentModule.getDID())){
                return true;
            }
        }
        return false;
    }

    public Module getmModuleInfo() {
        Bundle bundle = getIntent().getBundleExtra(Constants.MODULE_INFO_BUNDLE);
        Module module = bundle.getParcelable(Constants.MODULE_INFO);
        return module;

    }

    @Override
    public void onClick(View view) {

        if (mEdgeNodeHub != null) {
            sendUserDetials();
            //checkHubDiscoveryMode();
            mProgressDialog.show();
        } else {
            showErrorDialog(ERROR_HUB_NOT_SELECTED, "");
        }
    }

    private void showErrorDialog(int errorCode, String errorMessage) {

        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }

        if (mErrorDialog != null && mErrorDialog.isShowing()) {
            mErrorDialog.dismiss();
        }

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(getResources().getString(R.string.error));
        switch (errorCode) {
            case ERROR_HUB_NOT_SELECTED:
                dialogBuilder.setMessage(getResources().getString(R.string.error_hub_not_selected));
                break;
            case ERROR_RIGISTERATION:
                dialogBuilder.setMessage(errorMessage);
                break;
        }

        dialogBuilder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mErrorDialog.dismiss();
            }
        });

        mErrorDialog = dialogBuilder.create();
        mErrorDialog.show();
    }

    private void searchDevice(){

        JsonArrayRequest putRequest = new JsonArrayRequest(Request.Method.GET, getURL(DEVICES_URL), null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            Log.d(TAG, "response  = " + response);

                            Log.d(TAG, "response size = " + response.length());
                            for(int i=0; i < response.length();i++){
                                JSONObject deviceObject = response.getJSONObject(i);
                                String uuid = deviceObject.getString("uuid");
                                Log.d(TAG, "uuid =  " + uuid);
                                String deviceMacAddress = mEdgeNode.getMacAddress().toLowerCase();
                                Log.d(TAG, "deviceMacAddress =  " + deviceMacAddress);
                                if(uuid.contains(deviceMacAddress)){
                                    mEdgeNode.setDeviceUUID(uuid);
                                    registerModuleToCloud();
                                    Log.d(TAG, "device Found ");
                                    return;
                                }
                            }
                            //if it comes here, it was unable to find the device
                            showErrorDialog(ERROR_RIGISTERATION, "Device not found");
                        } catch (JSONException exception) {
                            Log.e(TAG, "Error while setting discovery mdoe" + exception.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e(TAG, "Error = " + error.getMessage());
                    }
                }
        ) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Connection", "close");
                return headers;
            }

        };

        putRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(putRequest);
    }

    private void sendUserDetials() {

        Sami sami = Sami.getInstance();
        Credentials credential = sami.getCredentials();

        Log.e(TAG, credential.getId());
        Log.e(TAG, credential.getToken());

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", credential.getId());
            jsonObject.put("token",credential.getToken());

        } catch (JSONException e) {
            Log.e(TAG, "Error while sending user details");
        }
        Log.e(TAG, "jsonObject = " + jsonObject.toString());
        JsonObjectRequest putRequest = new JsonObjectRequest(Request.Method.PUT, getURL(USER_DETAILS_URL), jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d(TAG, "response = " + response.toString());
                            if (response.getBoolean("success")) {
                                searchDevice();
                            } else {
                                showErrorDialog(ERROR_RIGISTERATION, response.getString("error"));
                            }
                        } catch (JSONException exception) {
                            Log.e(TAG, exception.getStackTrace().toString());
                            showErrorDialog(ERROR_RIGISTERATION, "Error while sending user details to ENM");
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d(TAG, "Error77 = " + error.toString());
                    }
                }
        ) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }

        };

        putRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(putRequest);

    }

    private void registerModuleToCloud() {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("uuid", mEdgeNode.getDeviceUUID());
            jsonObject.put("dtid", DEVICE_TYPE);
            jsonObject.put("name", mEdgeNode.getName());

        } catch (JSONException e) {
            Log.e(TAG, "Error while setting discovery mdoe");
        }
        Log.e(TAG, "jsonObject = " + jsonObject.toString());
        JsonObjectRequest putRequest = new JsonObjectRequest(Request.Method.POST, getURL(RIGISTER_URL), jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d(TAG, "response = " + response.toString());
                            if (response.getBoolean("success")) {
                                getDID();
                            } else {
                                showErrorDialog(ERROR_RIGISTERATION, response.getString("error"));
                            }
                        } catch (JSONException exception) {
                            Log.e(TAG, exception.getStackTrace().toString());
                            showErrorDialog(ERROR_RIGISTERATION, "Error while registering the device to cloud");
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d(TAG, "Error77 = " + error.toString());
                    }
                }
        ) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }

        };

        putRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(putRequest);

    }

    private void getDID() {

        StringRequest putRequest = new StringRequest(Request.Method.GET, getURL(DEVICES_URL)+"/"+mEdgeNode.getDeviceUUID(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            String did = jsonResponse.getString("akcuuid");
                            Log.e(TAG, "Edge Node DID = " + did);
                            mEdgeNode.setDID(did);
                            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                                mProgressDialog.dismiss();
                            }
                            goToNext();
                        } catch (JSONException exception) {
                            Log.e(TAG, "Error while setting discovery mdoe");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e(TAG, "Error = " + error.getMessage());
                        if (mProgressDialog != null && mProgressDialog.isShowing()) {
                            mProgressDialog.dismiss();
                        }
                        goToNext();
                    }
                }
        ) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Connection", "close");
                return headers;
            }

        };

        putRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(putRequest);

    }


    private Runnable delayedRunnable = new Runnable() {
        @Override
        public void run() {
            registerModuleToCloud();
        }
    };

    private void initProgressDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle("Registering");
        mProgressDialog.setMessage("Registering " + mEdgeNode.getName() + "  to cloud");
        mProgressDialog.setIndeterminate(true);
    }

    private String getURL(String url) {
        String newUrl = url.replace(IP_ADDRESS, mEdgeNodeHub.getIPAddress());
        Log.e(TAG, "newUrl = " + newUrl);
        return newUrl;
    }

    public void goToNext() {
        mEasyOnboardingApp.addModule(mEdgeNode);
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Constants.NEW_MODULE, (Parcelable) mEdgeNode);
        startActivity(intent);
        finish();
    }

}
