package samsung.ssic.id8.easyonboarding.samplecode;

/*
 * Copyright (C) 2015 Samsung Electronics Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.os.Looper;
import android.util.Log;

public class SAMISessionConstants {

    public static final String SAMI_AUTH_BASE_URL = "https://accounts.samsungsami.io";
    //public static final String CLIENT_ID = "97c5a4f583f544b9bdb20a6658a89d86";// AKA application id
    public static final String REDIRECT_URL = "http://localhost:8000/samidemo/index.php";
    public static final String ONBOARDING_ACCESS_TOKEN_PREF = "OnboardingAccessToken";
    public static final String ONBOARDING_PREFS = "OnboardingPreferences" ;
    public static final String ONBOARDING_USERID_PREF = "OnboardingUserId";

    public static final String ACCESS_TOKEN = "AccessToken";
    public static final String DEVICE_NAME = "DeviceName";
    //public static final String ARTIK5_DEVICE_TYPE_ID="dt1af0a4a97ba84e23a231bc53b9dcbc6e";
    public static final String ARTIK5_DEVICE_TYPE_ID="dt6594d3d6959446f292ab19b26609251b";
    public static final String DEVICE_ONBOARDING_PACKAGE = "samsung.ssic.id8.easyonboarding.device_onboarding";
    public static final String DEVICE_SCAN_ACTIVITY_PATH = "samsung.ssic.id8.easyonboarding.bluetoothgattclient." +
            "DeviceScanActivity";
    public static final String ONBOARD_CLOUD_AUTH_DEVICE_ACTIVITY_PATH = "samsung.ssic.id8.easyonboarding.cloudauth." +
            "OnBoardCloudAuthDeviceActivity";
    public static final String ONBOARD_OTHER_DEVICES = "samsung.ssic.id8.easyonboarding.websocket.OnBoardOtherDevices";
    private static final String TAG = SAMISessionConstants.class.getSimpleName();
    private static SAMISessionConstants instance;

    private SAMISessionConstants() {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            Log.e(TAG, "Constructor is not called in UI thread ");
        }
    }

    public static SAMISessionConstants getInstance() {
        if (instance == null) {
            instance = new SAMISessionConstants();
        }
        return instance;
    }
}
