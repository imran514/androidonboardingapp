/*
*
* DeviceListActivity.java
*
* Created by Namrata Garach on 2/25/16.
*
* */

package samsung.ssic.id8.easyonboarding.device_onboarding;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

import io.samsungsami.android.Sami;
import io.samsungsami.android.api.Call;
import io.samsungsami.android.api.Callback;
import io.samsungsami.android.api.Code;
import io.samsungsami.client.ApiException;
import io.samsungsami.model.Device;
import io.samsungsami.model.DeviceArray;
import io.samsungsami.model.DeviceType;
import io.samsungsami.model.DeviceTypeEnvelope;
import io.samsungsami.model.DevicesEnvelope;
import samsung.ssic.id8.easyonboarding.R;
import samsung.ssic.id8.easyonboarding.samplecode.SAMISessionConstants;
import samsung.ssic.id8.easyonboarding.samplecode.SamiServers;

// Shows list of devices already connected with SAMI. Allows to authorize cloud auth devices.
public class DeviceListActivity extends Activity {

    public WebView mWebView;
    private String TAG = "DeviceListActivity";
    private ListView mListView;
    private ArrayList<DeviceListObject> mDeviceList = new ArrayList<DeviceListObject>();
    private boolean mRequiresCloudConnector = false;
    private String mAccessToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        mAccessToken = intent.getStringExtra(SAMISessionConstants.ACCESS_TOKEN);

        setContentView(R.layout.activity_device_list);

        mListView = (ListView) findViewById(R.id.list);
        mWebView = (WebView) findViewById(R.id.webview);
        mWebView.setVisibility(View.INVISIBLE);

        // Get the list of device names that are connected to SAMI for current user
        mDeviceList = getUserDeviceNames();

        DeviceListAdapter listAdapter = new DeviceListAdapter(DeviceListActivity.this,
                R.layout.list_item_layout, mDeviceList, mWebView, mAccessToken);
        mListView.setAdapter(listAdapter);

        Button connectDeviceButton = (Button) findViewById(R.id.btAdd);
        connectDeviceButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Open the acitivity which give options to select the type of new device
                // you wish to connect to SAMI
                openConnectDeviceActivity();
            }
        });
    }

    public void openConnectDeviceActivity() {
        Intent intent = new Intent(DeviceListActivity.this, ConnectDeviceActivity.class);
        intent.putExtra(SAMISessionConstants.ACCESS_TOKEN, mAccessToken);
        startActivity(intent);
    }

    // Make a call to get the devices of the current user
    public ArrayList<DeviceListObject> getUserDeviceNames() {

        final ArrayList<DeviceListObject> deviceListObjArrayList = new ArrayList<DeviceListObject>();
        final Sami sami = Sami.setInstance(this, SamiServers.getDefaultConfig());
        final String uid = sami.getCredentials().getId();

        new Call(new Code() {

            @Override
            public Object run() {
                try {
                    // Get the first 25 user devices added to SAMI for current user
                    DevicesEnvelope devicesEnvelope = sami.getUsersApi()
                            .getUserDevices(0, 25, false, uid);
                    DeviceArray deviceArray = devicesEnvelope.getData();
                    for (Device device : deviceArray.getDevices()) {
                        DeviceTypeEnvelope deviceTypeEnvelope = sami.getDeviceTypesApi().
                                getDeviceType(device.getDtid());
                        DeviceType deviceType = deviceTypeEnvelope.getData();
                        if (deviceType.getHasCloudConnector()) { // check if cloud auth required
                            mRequiresCloudConnector = true;
                        }
                        DeviceListObject deviceListObject = new DeviceListObject(device.getId(),
                                device.getName(),
                                device.getNeedProviderAuth(), mRequiresCloudConnector);
                        deviceListObjArrayList.add(deviceListObject);
                    }
                } catch (ApiException e) {
                    Log.e(TAG, e.getMessage());
                }
                return deviceListObjArrayList;
            }
        }, new Callback() {

            @Override
            public void onApiResult(Object result) {
                if (deviceListObjArrayList.size() != 0) {
                    mListView.invalidateViews(); // refresh list view
                }
            }
        }).execute();

        return deviceListObjArrayList;
    }
}