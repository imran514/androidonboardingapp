package samsung.ssic.id8.easyonboarding.websocket;

import android.util.Log;

import org.java_websocket.handshake.ServerHandshake;

import io.samsungsami.android.websockets.SamiWebsocket;
import io.samsungsami.android.websockets.WebsocketEvents;


public class WebsocketPipe extends SamiWebsocket {

    public WebsocketPipe(final OnBoardOtherDevices activity, final String logPrefix, final String registerMessage) {
        setWebsocketEvents(new WebsocketEvents() {
            @Override
            public void onOpen(ServerHandshake handshakedata) {
                Log.i("WebsocketPipe", logPrefix + " onOpen");
                if (logPrefix.equalsIgnoreCase("websocket")) {
                    send(registerMessage);
                    //  activity.setSendMessageButton(true);
                    //   activity.runWebsocketPipeTest();
                }
            }

            @Override
            public void onMessage(String message) {
                Log.i("WebsocketPipe", logPrefix + " onMessage: " + message);
            }

            @Override
            public void onClose(int code, String reason, boolean remote) {
                // activity.setSendMessageButton(false);
                Log.i("WebsocketPipe", logPrefix + " onClose: " + reason);
            }

            @Override
            public void onError(Exception ex) {
                Log.i("WebsocketPipe", logPrefix + " onError: " + ex.getMessage());
            }

            @Override
            public void onPing() {
                Log.i("WebsocketPipe", logPrefix + " onPing()");
            }

            @Override
            public void onPingTimeout() {
                Log.i("WebsocketPipe", logPrefix + " onPingTimeout()");
            }
        });
    }
}
