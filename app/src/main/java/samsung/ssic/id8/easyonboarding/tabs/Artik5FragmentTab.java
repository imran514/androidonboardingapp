package samsung.ssic.id8.easyonboarding.tabs;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import samsung.ssic.id8.easyonboarding.EasyOnboardingApp;
import samsung.ssic.id8.easyonboarding.LoginActivity;
import samsung.ssic.id8.easyonboarding.R;
import samsung.ssic.id8.easyonboarding.control.Module;
import samsung.ssic.id8.easyonboarding.control.TabActivity;
import samsung.ssic.id8.easyonboarding.device_onboarding.DeviceListAdapter;
import samsung.ssic.id8.easyonboarding.device_onboarding.DeviceListObject;
import samsung.ssic.id8.easyonboarding.samplecode.SAMISessionConstants;
import samsung.ssic.id8.easyonboarding.util.Constants;

public class Artik5FragmentTab extends Fragment {

    private final String TAG = "Artik5FragmentTab";
    private ArrayList<DeviceListObject> mDevicesOnCloud = new ArrayList<>();
    WebView mWebView;
    DeviceListAdapter mListAdapter;
    private String mAccessToken;
    private ListView artik5ListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.artik5_layout, container, false);
        artik5ListView = (ListView) rootView.findViewById(R.id.list);

        // mListAdapter.notifyDataSetChanged();
        return rootView;
    }

    public Artik5FragmentTab newInstance(String accessToken) {
        Artik5FragmentTab fragment = new Artik5FragmentTab();
        mAccessToken = accessToken;
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.MODULE_ONBOARDED);
        FragmentIntentReceiver fragmentIntentReceiver = new FragmentIntentReceiver();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(fragmentIntentReceiver, intentFilter);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, "Artik 5 onCreate  ");


        mWebView = (WebView) getView().findViewById(R.id.webview);
        mWebView.setVisibility(View.INVISIBLE);
        mDevicesOnCloud  = ((EasyOnboardingApp)getActivity().getApplication()).getUserDevices();
        mListAdapter = new DeviceListAdapter(getActivity(),
                R.layout.list_item_card, new ArrayList<DeviceListObject>(), mWebView, mAccessToken);
        artik5ListView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return false;
            }
        });
        EasyOnboardingApp easyOnboardingApp = (EasyOnboardingApp)getActivity().getApplication();
        List<Module> modules = easyOnboardingApp.getModules();
        for (Object object : modules) {
            Module module = (Module)object;
            if (module.getType() == Constants.ARTIK_5) {
                DeviceListObject deviceListObject1 = new DeviceListObject(module.getPlace(), module.getName(),
                        false, false, module);
                if(isRegisteredOnCloud(module)) {
                    mListAdapter.add(deviceListObject1);
                }
            }
        }
        artik5ListView.setAdapter(mListAdapter);

    }

    private boolean isRegisteredOnCloud(Module currentModule){
        for (DeviceListObject device : mDevicesOnCloud){
            if(device.getDeviceId().equals(currentModule.getDID())){
                return true;
            }
        }
        return false;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    public class FragmentIntentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(Constants.MODULE_ONBOARDED)) {
                Log.d(TAG, "MODULE_ONBOARDED intent ");
                Bundle bundle = intent.getExtras();
                Log.d(TAG, "Received MODULE_ONBOARDED intent ");
                Module addedModule = (Module) bundle.get(Constants.MODULE_INFO);
                if (addedModule.getType() == Constants.ARTIK_5) {
                    DeviceListObject deviceListObject1 = new DeviceListObject(addedModule.getPlace(), addedModule.getName(),
                            false, false);
                    mListAdapter.add(deviceListObject1);
                    mListAdapter.notifyDataSetChanged();
                }


            }
        }
    }

}
