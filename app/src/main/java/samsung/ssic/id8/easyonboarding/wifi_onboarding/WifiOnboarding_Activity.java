package samsung.ssic.id8.easyonboarding.wifi_onboarding;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import io.samsungsami.android.Sami;
import io.samsungsami.client.ApiException;
import io.samsungsami.model.DeviceEnvelope;
import samsung.ssic.id8.easyonboarding.EasyOnboardingApp;
import samsung.ssic.id8.easyonboarding.LED.MainActivity;
import samsung.ssic.id8.easyonboarding.R;
import samsung.ssic.id8.easyonboarding.control.Module;
import samsung.ssic.id8.easyonboarding.customUI.CustomViewPager;
import samsung.ssic.id8.easyonboarding.samplecode.SAMISessionConstants;
import samsung.ssic.id8.easyonboarding.samplecode.SamiServers;
import samsung.ssic.id8.easyonboarding.util.Constants;

public class WifiOnboarding_Activity extends AppCompatActivity {


    public static String TAG = "WifiOnboarding_Activity";

    private static final int NO_INTERNET_ERROR_THRESHOLD = 10;
    private static final int INVALID_WIFI_ERROR_THRESHOLD = 5;

    private static int RSSI = -35;
    private static ProgressDialog mProgressDialog;
    private static Boolean mWifiOnboarding = true;
    private static WiFiNetwork mWifiNetwork;
    private static SharedPreferences mSharedPrefs;
    private static SharedPreferences.Editor mSharedEditor;
    final String url = "https://api.samsungsami.io/v1.1/devices/registrations/pin";
    protected int mRetryCount;
    Receiver mReceiver;
    Module mOnboardingModule;
    CountDownTimer mCountDownTimer;
    RequestQueue queue;
    private int NUM_ITEMS = 4;
    private int TAB_ITEMS = 5;
    private boolean DEBUG = false;
    private MyAdapter mAdapter;
    private CustomViewPager mPager;
    private WifiScanFragment mWifiScanFragment;
    private PluginPowerFragment mPluginPowerFragment;
    private WifiConnectedFragment mWifiConnectedFragment;
    private DeviceDetectedFragment mDeviceDetectedFragment;
    private Handler mHandler;
    private BleProvisioner mBleProvisioner;
    private static Boolean WIFI_ONBOARDING_ONLY = false;
    private Dialog mErrorDialog;
    private int mNoInternetErrorCount = 0;
    private int mInvalidWifiStateErrorCount = 0;
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBleProvisioner = ((BleProvisioner.LocalBinder) service).getService(WifiOnboarding_Activity.this, mHandler);
            if (!mBleProvisioner.init()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }

            mBleProvisioner.setRegistrationState(Constants.REGIS_STATE_START);
            Log.d("Venkat", " WifiOnboardingAcitity Access token: " + mAccessToken);
            Bundle bundle = getIntent().getBundleExtra(Constants.MODULE_INFO_BUNDLE);

            if (bundle != null) {
                mOnboardingModule = bundle.getParcelable(Constants.MODULE_INFO);
                mBleProvisioner.setModuleMacAddress(mOnboardingModule.getMacAddress());
            }
            if (mWifiOnboarding) {
                mBleProvisioner.startScanLeDevices(OnboardingGattService.SERVICE_UUID, RSSI);
                startTimer();
            } else {
                mBleProvisioner.startScanLeDevices(OnboardingGattService.SERVICE_UUID, RSSI);
            }
            mBleProvisioner.resume();
            // Automatically connects to the device upon successful start-up
            // initialization.

            //mBleProvisioner.connectGatt();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBleProvisioner = null;
        }
    };
    private WifiManager mWifiManager;
    // private  WiFiNetwork mNetwork;
    private String mAccessToken;
    private String mDeviceTypeId, mChallengePin, mDeviceDID, mDeviceName;
    private TextView mSendResponse;

    public static WiFiNetwork getWifiNetwork() {
        Log.d(TAG, "getWifiNetwork SSID: " + mWifiNetwork.SSID + " Encryption : " + mWifiNetwork.authentication + " Password :" + mWifiNetwork.password);
        return mWifiNetwork;
    }

    public void setWifiNetwork(WiFiNetwork wifiNetwork) {
        WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = wifiManager.getConnectionInfo();
        /*Safety Measure for DEMO*/
        if (info.getSSID().toString().equals("\"Artik\"")) {
            Log.d(TAG, "Connected to Artik network !");
            wifiNetwork.SSID = "Artik";
            wifiNetwork.authentication = "OPEN";
            wifiNetwork.password = "";
        }
        Log.d(TAG, "setWifiNetwork SSID: " + wifiNetwork.SSID + " Encryption : " + wifiNetwork.authentication + " Password :" + wifiNetwork.password);
        WifiOnboarding_Activity.mWifiNetwork = wifiNetwork;
    }

    public SharedPreferences.Editor getmSharedEditor() {
        return mSharedEditor;
    }

    public SharedPreferences getmSharedPrefs() {
        return mSharedPrefs;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*mSlidingTabsLayout = (LinearLayout) findViewById(R.id.sliding_tabs_layout);*/

        Bundle extras = getIntent().getExtras();


        queue = Volley.newRequestQueue(this);
        mWifiOnboarding = true;
        mNoInternetErrorCount = 0;
        mInvalidWifiStateErrorCount = 0;
        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayShowCustomEnabled(true);
        mActionBar.setCustomView(R.layout.custom_actionbar);
        //LayoutInflater mInflater = LayoutInflater.from(this);

        // View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
//        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);
//        mTitleTextView.setText("My Own Title");

//        ImageButton imageButton = (ImageButton) mCustomView
//                .findViewById(R.id.imageButton);
//        imageButton.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(getApplicationContext(), "Refresh Clicked!",
//                        Toast.LENGTH_LONG).show();
//            }
//        });

        //mActionBar.setCustomView(mCustomView);


        mSharedPrefs = getSharedPreferences("wifi_network", MODE_MULTI_PROCESS);
        mSharedEditor = mSharedPrefs.edit();

        mReceiver = new Receiver();


        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.NEXT_FRAGMENT_INTENT);
        intentFilter.addAction(Constants.BT_PAIRING_REQ_INTENT);
        intentFilter.addAction(Constants.BT_TARGET_DISCOVERY_RESULT);

        registerReceiver(mReceiver, intentFilter);

        if (mDeviceDetectedFragment == null)
            mDeviceDetectedFragment = new DeviceDetectedFragment();
        mDeviceDetectedFragment.setModule(mOnboardingModule);

        mHandler = new Handler(Looper.getMainLooper()) {

            @Override
            public void handleMessage(Message inputMessage) {
                switch (inputMessage.what) {

                    case Constants.START_SCANNING:
                        mRetryCount = 0;
                        Log.d(TAG, "START_SCANNING");
                        if (mWifiOnboarding) {
                            mBleProvisioner.startScanLeDevices(OnboardingGattService.SERVICE_UUID, RSSI);
                            startTimer();
                        } else {
                            mBleProvisioner.startScanLeDevices(OnboardingGattService.SDR_REGISTRATION_UUID, RSSI);
                        }

                        break;
                    case Constants.STOP_SCANNING:
                        Log.d(TAG, "STOP_SCANNING");
                        mBleProvisioner.stopScanLeDevices();
                        break;
                    case Constants.DETECTED_DEVICE:
                        Log.d(TAG, "DETECTED_DEVICE");
                        if (mWifiOnboarding) {
                            //mFab.setVisibility(View.INVISIBLE);
                            stopTimer();
/*                            jumpToPage(Constants.ARTIK_FOUND);
                            mPager.setVisibility(View.VISIBLE);*/

                            Log.d(TAG, "CONNECT_DEVICE");
                            try {
                                mBleProvisioner.connectGatt();
                            } catch (Exception e) {

                            }
                        } else {
                            Log.d(TAG, "DETECTED_SDR_DEVICE");
                            mDeviceName = (String) inputMessage.obj;
                            //mHandler.sendEmptyMessage(WifiOnboarding_Activity.STOP_SCANNING);
                            Log.d(TAG, "CONNECT_DEVICE");
/*                            mProgressDialog = ProgressDialog.show(WifiOnboarding_Activity.this, mOnboardingModule.getModuleName(),
                                    "Registering device on Cloud ...");*/
                            mBleProvisioner.connectGatt();
                        }
                        break;
                    case Constants.DETECTED_SDR_DEVICE:
                        if (!mWifiOnboarding) {
                            Log.d(TAG, "DETECTED_SDR_DEVICE");
                            mDeviceName = (String) inputMessage.obj;
                            //mHandler.sendEmptyMessage(WifiOnboarding_Activity.STOP_SCANNING);
                            Log.d(TAG, "CONNECT_DEVICE");
/*                            mProgressDialog = ProgressDialog.show(WifiOnboarding_Activity.this, mOnboardingModule.getModuleName(),
                                    "Registering device on Cloud ...");*/
                            mBleProvisioner.connectGatt();
                        }
                        break;
                    case Constants.GATT_CONNECTED:
                        Log.d(TAG, "GATT_CONNECTED");
                        // TODO Maybe discoverServices should be called from here instead of doing it
                        if (mPager.getCurrentItem() == Constants.PLUGIN_ARTIK) {
                            jumpToPage(Constants.ARTIK_FOUND);
                            mPager.setVisibility(View.VISIBLE);
                        }
                        //directly on the Ble Provisioner
                        // if(DEBUG) Toast.makeText(getApplicationContext(), "Connected to Artik", Toast.LENGTH_SHORT).show();
                        mBleProvisioner.discoverServices();
                        break;
                    case Constants.GATT_DISCONNECTED:
                        Log.d(TAG, "GATT_DISCONNECTED");
                        if (DEBUG)
                            Toast.makeText(getApplicationContext(), "GATT DISCONNECTED", Toast.LENGTH_SHORT).show();
                        break;
                    case Constants.GATT_FAILED:
                        Log.d(TAG, "GATT_FAILED");
                        if (mRetryCount < 5) {
                            if (DEBUG)
                                Toast.makeText(getApplicationContext(), "RETRYING GATT connection", Toast.LENGTH_SHORT).show();
                            Log.d(TAG, "Reconnecting to GATT");
                            mBleProvisioner.connectGatt();
                            mRetryCount++;
                        } else {
                            Log.d(TAG, "Reconnecting to GATT failed");
                            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                                mProgressDialog.dismiss();

                            }
                            Toast.makeText(getApplicationContext(), "Connection to Artik failed", Toast.LENGTH_SHORT).show();
                            if (DEBUG)
                                Toast.makeText(getApplicationContext(), "Giving up retrying GATT connection", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case Constants.GATT_SERVICES_DISCOVERED:
                        Log.d(TAG, "GATT_SERVICES_DISCOVERED");
                        if (mWifiOnboarding) {
                            if (DEBUG)
                                Toast.makeText(getApplicationContext(), "SERVICES DISCOVERED", Toast.LENGTH_SHORT).show();
                            Log.d(TAG, "GATT_SERVICES_DISCOVERED for Wifi Onboarding");
                            mBleProvisioner.setCharacteristicNotification(OnboardingGattService.CHARACTERISTIC_STATUS, true);
                            mBleProvisioner.setCharacteristicNotification(OnboardingGattService.CHARACTERISTIC_LONG_STATUS, true);

                            // Once services are discovered read VendorID and DeviceID
                            mBleProvisioner.readCharacteristic(OnboardingGattService.CHARACTERISTIC_VENDOR_ID);
                            mBleProvisioner.readCharacteristic(OnboardingGattService.CHARACTERISTIC_DEVICE_ID);
                        } else {
                            Log.d(TAG, "GATT_SERVICES_DISCOVERED for Device Onboarding");
      /*                      mBleProvisioner.setCharacteristicNotification(OnboardingGattService.CHALLENGE_PIN_UUID, true);
                            mBleProvisioner.setCharacteristicNotification(OnboardingGattService.DEVICE_TOKEN_UUID, true);
*/
                            mBleProvisioner.readCharacteristic(OnboardingGattService.DEVICE_TYPE_ID_UUID);

                        }

                        break;
                    case Constants.DEVICE_TYPE_ID_READ:

                        if (inputMessage.obj != null) {
                            mDeviceTypeId = (String) inputMessage.obj;
                            Log.d(TAG, "DEVICE_TYPE_ID_READ : " + mDeviceTypeId);
                        }

                        // Read the challenge pin information
                        // mBleProvisioner.writeCharacteristic(OnboardingGattService.START_REGISTRATION_UUID, "1");
                        mBleProvisioner.writeCharacteristic(OnboardingGattService.START_REGISTRATION_UUID, "1");
                        Runnable task = new Runnable() {
                            public void run() {
                                // mBleProvisioner.readCharacteristic(OnboardingGattService.CHALLENGE_PIN_UUID);
                            }
                        };
                        worker.schedule(task, 8, TimeUnit.SECONDS);

                        break;
                    case Constants.DEVICE_CHALLENGE_PIN_READ:
                        mChallengePin = (String) inputMessage.obj;
                        Log.d(TAG, "DEVICE_CHALLENGE_PIN_READ : " + mChallengePin);

                        // Go to next mState - Confirm user with SAMI - mState 5
                        // confirm user with SAMI using devicetype id, device name
                        // and challenge pin
                        //Error Validation done in BleProvisioner and State updated properly


                        confirmUserWithSAMI();

                        break;
                    case Constants.DEVICE_DID_READ:
                        if (!mWifiOnboarding) {
                            mDeviceDID = (String) inputMessage.obj;
                            Log.d(TAG, "DEVICE_DID_READ : " + mDeviceDID);

                            if (mDeviceDID != null && !mDeviceDID.equals("1232")) {
                                mOnboardingModule.setDID(mDeviceDID);
                                EasyOnboardingApp easyOnboardingApp = (EasyOnboardingApp) getApplication();
                                easyOnboardingApp.addModule(mOnboardingModule);
                            }

/*                                mBluetoothLeService.close();
                                mBluetoothLeService.disconnect();*/

                            if (mProgressDialog != null) {

                                try {
                                    mProgressDialog.setMessage("Device registered successfully");
                                    if (mProgressDialog.isShowing()) {
                                        mProgressDialog.dismiss();
                                        // go to device list activity
/*                                        Intent deviceListIntent = new Intent(WifiOnboarding_Activity.this, DeviceListActivity.class);
                                        // passing the accesstoken obtained during login to
                                        // use it in further api calls to SAMI
                                        deviceListIntent.putExtra(SAMISessionConstants.ACCESS_TOKEN, mAccessToken);
                                        startActivity(deviceListIntent);*/
                                        testLED();
                                    }
                                } catch (Exception e) {
                                    Log.d(TAG, e.getMessage());
                                }
                            }

                        }
                        break;
                    case Constants.VENDOR_ID_READ:
                        if (mWifiOnboarding) {
                            Log.d(TAG, "VENDOR_ID_READ");
                        }
                        break;
                    case Constants.DEVICE_ID_READ:
                        if (mWifiOnboarding) {
                            Log.d(TAG, "DEVICE_ID_READ");
                            Log.e(TAG, "DEVICE_ID = " + (String) inputMessage.obj);

                            if (mRetryCount > 0) {
                                mHandler.sendEmptyMessage(Constants.SEND_WIFI_CRED);
                            }
                        }
                        break;
                    //TODO:Hide Progress bar
                    case Constants.SEND_WIFI_CRED:
                        mInvalidWifiStateErrorCount = 0;
                        Log.d(TAG, "Sending WIFI Credentials :");
                        // At this point we can execute all necessary writes for onboarding
                        mBleProvisioner.writeCharacteristic(OnboardingGattService.CHARACTERISTIC_SSID, mWifiNetwork.SSID);
                        mBleProvisioner.writeCharacteristic(OnboardingGattService.CHARACTERISTIC_AUTH, mWifiNetwork.authentication);
                        if (!mWifiNetwork.authentication.equals("OPEN"))
                            mBleProvisioner.writeCharacteristic(OnboardingGattService.CHARACTERISTIC_PASS, mWifiNetwork.password);
                        else
                            mBleProvisioner.writeCharacteristic(OnboardingGattService.CHARACTERISTIC_PASS, "");
                        byte[] ba = new byte[1];
                        ba[0] = 0x01;
                        //mBleProvisioner.writeCharacteristic(OnboardingGattService.CHARACTERISTIC_COMMAND, ba);

                        break;
                    case Constants.STATUS_NOTIFIED:

                        String status = (String) inputMessage.obj;
                        Log.d(TAG, "STATUS_NOTIFIED : " + status);
                        if (status.equals(OnboardingGattService.STATUS_RECVD_PIN)) {
                            Log.e(TAG, "11CHALLENGE_PIN_UUID");
                            mBleProvisioner.readCharacteristic(OnboardingGattService.CHALLENGE_PIN_UUID);
                        } else if (status.equals(OnboardingGattService.STATUS_RECVD_TOKEN)) {
                            mBleProvisioner.readCharacteristic(OnboardingGattService.DEVICE_ID_CHARACTERISTIC_UUID);
                        }
                        break;
                    case Constants.LONG_STATUS_NOTIFIED:

                        String longStatus = (String) inputMessage.obj;
                        Log.d(TAG, "LONG_STATUS_NOTIFIED : " + longStatus);

                        if (longStatus.equals(OnboardingGattService.LONG_STATUS_CONN_ESTAB) && mWifiOnboarding) {
                            mBleProvisioner.readCharacteristic(OnboardingGattService.CHARACTERISTIC_IPADDRESS);
                            //mWifiScanFragment.updateStatus(longStatus);
                        } else if (isError(longStatus)) {
                            showErrorDialog(longStatus);
                        }
                        break;
                    case Constants.IPADDRESS_READ:
                        String ipAddress = (String) inputMessage.obj;
                        Log.d(TAG, "ipAddress : " + ipAddress);
                        mOnboardingModule.setIPAddress(ipAddress);
                        mNoInternetErrorCount = 0;
                        mInvalidWifiStateErrorCount = 0;
                        if (mWifiOnboarding) {
                            mWifiScanFragment.updateStatus(null);
                        }
                        break;
                    case Constants.DISCONNECT_TARGET:
                        Log.d(TAG, "DISCONNECT_TARGET");
                        mBleProvisioner.writeCharacteristic(OnboardingGattService.CHARACTERISTIC_COMMAND, new byte[]{0x02});
                        break;
                    case Constants.RESET_TARGET:
                        Log.d(TAG, "RESET_TARGET");
                        mBleProvisioner.writeCharacteristic(OnboardingGattService.CHARACTERISTIC_COMMAND, new byte[]{0x03});
                        break;
                    case Constants.ERROR_INVALID_MAC_ADDRESS:
                        Log.d(TAG, "ERROR_INVALID_MAC_ADDRESS");
                        showErrorDialog(OnboardingGattService.INVALID_MAC_ADDRESS);
                        break;
                    default:
                        super.handleMessage(inputMessage);

                }
            }
        };


        mPager = (CustomViewPager) findViewById(R.id.pager);
        mPager.setPagingEnabled(false); //TODO :Set to true for debugging. False for demos
        mPager.setPageTransformer(true, new DepthPageTransformer());
        mAdapter = new MyAdapter(getSupportFragmentManager());

        mPager.setAdapter(mAdapter);
        mAccessToken = getIntent().getStringExtra(SAMISessionConstants.ACCESS_TOKEN);
        Log.d("Venkat", " WifiOnboardingAcitity Access token: " + mAccessToken);
        mOnboardingModule = new Module("Temp", "Temp", Constants.ARTIK_5, "Temp", "Temp", "Temp");
        Bundle bundle = getIntent().getBundleExtra(Constants.MODULE_INFO_BUNDLE);

        if (bundle != null) {
            mOnboardingModule = bundle.getParcelable(Constants.MODULE_INFO);
            Log.d(TAG, "REceived bundle :" + mOnboardingModule.getType() + " uuid: " + mOnboardingModule.getUuid());
            mPager.setVisibility(View.VISIBLE);
        }
        mWifiOnboarding = true;
        //mPager.setCurrentItem(Constants.LED_TEST_START);
        //mHandler.sendEmptyMessage(Constants.START_SCANNING);
        mWifiNetwork = new WiFiNetwork("", "", "");

        Thread t = new Thread() {
            public void run() {
                Intent gattServiceIntent = new Intent(WifiOnboarding_Activity.this, BleProvisioner.class);
                bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
            }
        };
        t.start();

    }

    private boolean isError(String status) {
        switch (status) {
            case OnboardingGattService.LONG_STATUS_START_REGISTRATION_ERROR:
            case OnboardingGattService.LONG_STATUS_COMPLETE_REGISTRATION_ERROR:
            case OnboardingGattService.LONG_STATUS_INVALID_SSID:
            case OnboardingGattService.LONG_STATUS_WRONG_PASSWORD:
            case OnboardingGattService.LONG_STATUS_NO_IPADDRESS:
                return true;
            case OnboardingGattService.LONG_STATUS_INVALID_WIFI_STATE:
                ++mInvalidWifiStateErrorCount;
                if (mInvalidWifiStateErrorCount >= INVALID_WIFI_ERROR_THRESHOLD) {
                    return true;
                } else {
                    return false;
                }
            case OnboardingGattService.LONG_STATUS_INTERNET_UNAVAILABLE:
                ++mNoInternetErrorCount;
                if (mNoInternetErrorCount >= NO_INTERNET_ERROR_THRESHOLD) {
                    return true;
                } else {
                    return false;
                }
            default:
                return false;

        }
    }

    private void showErrorDialog(String status) {
        if (mWifiScanFragment != null)
            mWifiScanFragment.hideProgressBar();

        if (mErrorDialog != null && mErrorDialog.isShowing()) {
            mErrorDialog.dismiss();
        }

        Log.e(TAG, "status = " + status);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(OnboardingGattService.STATUS_FAILED);
        switch (status) {
            case OnboardingGattService.LONG_STATUS_START_REGISTRATION_ERROR:
                dialogBuilder.setMessage(OnboardingGattService.LONG_STATUS_START_REGISTRATION_ERROR);
                break;
            case OnboardingGattService.LONG_STATUS_COMPLETE_REGISTRATION_ERROR:
                dialogBuilder.setMessage(OnboardingGattService.LONG_STATUS_COMPLETE_REGISTRATION_ERROR);
                break;
            case OnboardingGattService.LONG_STATUS_INVALID_SSID:
                dialogBuilder.setMessage(OnboardingGattService.LONG_STATUS_INVALID_SSID);
                break;
            case OnboardingGattService.LONG_STATUS_WRONG_PASSWORD:
                dialogBuilder.setMessage(OnboardingGattService.LONG_STATUS_WRONG_PASSWORD);
                break;
            case OnboardingGattService.LONG_STATUS_NO_IPADDRESS:
                dialogBuilder.setMessage(OnboardingGattService.LONG_STATUS_NO_IPADDRESS);
                break;
            case OnboardingGattService.LONG_STATUS_INTERNET_UNAVAILABLE:
                dialogBuilder.setMessage(OnboardingGattService.LONG_STATUS_INTERNET_UNAVAILABLE);
                break;
            case OnboardingGattService.LONG_STATUS_INVALID_WIFI_STATE:
                dialogBuilder.setMessage(OnboardingGattService.LONG_STATUS_INVALID_WIFI_STATE);
                break;
            case OnboardingGattService.INVALID_MAC_ADDRESS:
                dialogBuilder.setMessage(OnboardingGattService.INVALID_MAC_ADDRESS);
                break;
            default:

        }
        dialogBuilder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mErrorDialog.dismiss();
            }
        });
        mErrorDialog = dialogBuilder.create();
        mErrorDialog.show();
    }

    public void startTimer() {
        mCountDownTimer = new CountDownTimer(18000, 1000) {

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                jumpToPage(Constants.PLUGIN_ARTIK);
            }
        }.start();
    }

    public void stopTimer() {
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
            mCountDownTimer = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private static final ScheduledExecutorService worker =
            Executors.newSingleThreadScheduledExecutor();

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mBleProvisioner != null) {
            // Ensures Bluetooth is enabled on the device
            if (!mBleProvisioner.isEnabled()) {
                mBleProvisioner.init();
            }
            mBleProvisioner.resume();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mBleProvisioner != null) {
            mBleProvisioner.pause();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnection);
        if (mBleProvisioner != null) {
            mBleProvisioner = null;
        }
        try {
            if (mReceiver != null) {
                unregisterReceiver(mReceiver);
            }
        } catch (Exception e) {

        }
    }

    public void jumpToPage(View view) {
        int totalCount = mAdapter.getCount();
        Log.d(TAG, "totalCount " + totalCount);
        int nextIndex = ((mPager.getCurrentItem() + 1) > totalCount) ? (mPager.getCurrentItem()) : ((mPager.getCurrentItem() + 1));
        if (mPager != null)
            mPager.setCurrentItem(nextIndex);
        if (nextIndex == Constants.WIFI_CONNCTD) {
            mWifiOnboarding = false;
            if (WIFI_ONBOARDING_ONLY) {
                testLED();
            } else {
                //mBleProvisioner.disconnectGatt();
                //mBleProvisioner.connectGatt();
                //mBleProvisioner.setmServiceUUID(OnboardingGattService.SDR_REGISTRATION_UUID);
                // mBleProvisioner.discoverServices();
                mWifiOnboarding = false;
                //mBleProvisioner.readCharacteristic(OnboardingGattService.DEVICE_TYPE_ID_UUID);
                Runnable task = new Runnable() {
                    public void run() {
                        mBleProvisioner.readCharacteristic(OnboardingGattService.DEVICE_TYPE_ID_UUID);
                    }
                };
                worker.schedule(task, 2, TimeUnit.SECONDS);

                mProgressDialog = ProgressDialog.show(WifiOnboarding_Activity.this, mOnboardingModule.getName(),
                        "Registering device on Cloud ...");

            }
        }
    }

    public void jumpToPreviousPage(View view) {
        int totalCount = mAdapter.getCount();
        Log.d(TAG, "totalCount " + totalCount);
        int previousIndex = ((mPager.getCurrentItem() - 1) < totalCount) ? (mPager.getCurrentItem() - 1) : ((mPager.getCurrentItem()));
        mPager.setCurrentItem(previousIndex);
    }

    public void jumpToPage(int pageIndex) {
        mPager.setCurrentItem(pageIndex);
    }

    public void device_found_next(View view) {
        Log.d(TAG, "Received device_found_next");
        jumpToPage(view);
    }

    public void wifi_password_next(View view) {
        Log.d(TAG, "Received wifi_password_next");
        //jumpToPage(view);
        //mHandler.sendEmptyMessage(WifiOnboarding_Activity.CONNECT_DEVICE);
    }

    public void wifi_password_prev(View view) {
        Log.d(TAG, "Received wifi_password_prev");
        jumpToPreviousPage(view);

    }

    public void wifi_connected_next(View view) {
        Log.d(TAG, "Received wifi_connected_next");

/*        mWifiOnboarding = false;
        if(WIFI_ONBOARDING_ONLY) {
            testLED();
        } else {
            mBleProvisioner.setmServiceUUID(OnboardingGattService.SDR_REGISTRATION_UUID);
            mBleProvisioner.discoverServices();
            mProgressDialog = ProgressDialog.show(WifiOnboarding_Activity.this, mOnboardingModule.getModuleName(),
                    "Registering device on Cloud ...");
            mWifiOnboarding = false;
        }*/

    }

    public void LED_test_start_next(View view) {
        Log.d(TAG, "Received LED_test_start_next");
        jumpToPage(view);
        mPager.setVisibility(View.INVISIBLE);
        broadcastIntent(Constants.MODULE_ONBOARDED);
        finish();
    }

    public void LED_test_done_next(View view) {
        Log.d(TAG, "Received LED_test_done_next");
        jumpToPage(view);

        mPager.setVisibility(View.INVISIBLE);

/*        welcomeText.setVisibility(View.INVISIBLE);
        mSlidingTabsLayout.setVisibility(View.VISIBLE);
        mFab.setVisibility(View.VISIBLE);*/

    }


    public void broadcastIntent(String action) {
        Log.d(TAG, "sending  MODULE_ONBOARDED intent");
        Intent intent = new Intent();
        intent.setAction(action);

        intent.putExtra(Constants.NEW_MODULE, (Parcelable) mOnboardingModule);
        sendBroadcast(intent);
    }

    private void confirmUserWithSAMI() {
        boolean userConfirmed = false;// = false;
        try {

            if (mAccessToken.isEmpty() || mAccessToken.equals(null)) {
                Log.d(TAG, "mAccessToken is null");
                //loadWebView();
            }
            new confirmUserTask(WifiOnboarding_Activity.this, mAccessToken).execute().get();
        } catch (Exception e) {
            Log.i(TAG, e.getMessage());
        }
    }

    public void confirmSAMIUser(final String accessToken) {

        //String url = "https://api.samsungsami.io/v1.1/devices/registrations/pin/pin:"+mChallengePin+"/deviceName:"+mDeviceName;
        StringRequest putRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        if (response.equals("200")) {
                            mBleProvisioner.setRegistrationState(Constants.REGIS_STATE_USER_CONFIRMED);
                            mBleProvisioner.writeCharacteristic(OnboardingGattService.COMPLETE_REGISTRATION_UUID, "1");

                        } else {
                            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                                mProgressDialog.setMessage("Device validation failed");
                                mProgressDialog.dismiss();
                            }
                            Log.d(TAG, "User Not confirmed");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error

                        //Log.d(TAG,"Error :"+error.getMessage().toString());
                        if (mProgressDialog != null && mProgressDialog.isShowing()) {
                            mProgressDialog.setMessage("Device validation failed");
                            mProgressDialog.dismiss();
                        }
                        Log.d(TAG, "User Not confirmed");
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("pin", mChallengePin);
                params.put("deviceName", mDeviceName);
                Log.d(TAG, " Pin :" + mChallengePin + " Device Name: " + mDeviceName);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("USER-AGENT", "Mozilla/5.0");
                headers.put("ACCEPT-LANGUAGE", "en-US,en;0.5");
                headers.put("Content-Type", " application/json");
                headers.put("Accept", "application/json");
                headers.put("Authorization", "  bearer " + mAccessToken);
                return headers;
            }

        };
        Log.d(TAG, "Put Request: " + putRequest.toString());
        queue.add(putRequest);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            mBleProvisioner.disconnectGatt();
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    public class Receiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Constants.NEXT_FRAGMENT_INTENT)) {
                Log.d(TAG, "Received intent :" + intent.getAction());
                jumpToPage(null);
            }
        }
    }

    public class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public Fragment getItem(int position) {

            Log.d(TAG, "getItem " + position);

            switch (position) {
                case Constants.PLUGIN_ARTIK:
                    if (mPluginPowerFragment == null)
                        mPluginPowerFragment = new PluginPowerFragment();
                    mPluginPowerFragment.setModule(mOnboardingModule);
                    return mPluginPowerFragment;
                case Constants.ARTIK_FOUND:
                    if (mDeviceDetectedFragment == null)
                        mDeviceDetectedFragment = new DeviceDetectedFragment();

                    mDeviceDetectedFragment.setModule(mOnboardingModule);
                    return mDeviceDetectedFragment;

                case Constants.WIFI_SCAN:
                    if (mWifiScanFragment == null)
                        mWifiScanFragment = new WifiScanFragment();
                    mWifiScanFragment.setHandler(mHandler);
                    return mWifiScanFragment;

                case Constants.WIFI_CONNCTD:
                    if (mWifiConnectedFragment == null)
                        mWifiConnectedFragment = new WifiConnectedFragment();
                    return mWifiConnectedFragment;
              /*  case Constants.LED_TEST_START:
                    if (mTestStartFragment == null)
                        mTestStartFragment = new TestStartFragment();
                    //mFab.setVisibility(View.INVISIBLE);
                    return mTestStartFragment;*/

/*                case LED_TEST_DONE:
                    if (mTestCompleteFragment == null)
                        mTestCompleteFragment = new TestCompleteFragment();

                    return mTestCompleteFragment;*/

                default:
                    if (mDeviceDetectedFragment == null)
                        mDeviceDetectedFragment = new DeviceDetectedFragment();
                    mDeviceDetectedFragment.setModule(mOnboardingModule);
                    return null;

            }
        /* This should NOT happen */
            ///return null;
        }
    }

    private class confirmUserTask extends AsyncTask<String, Void, Boolean> {

        private final Context context;
        private String accessToken;

        public confirmUserTask(Context c, String accessToken) {
            Log.d(TAG, " confirmUserTask Access token: " + mAccessToken);
            this.context = c;
            this.accessToken = accessToken;
        }

        protected void onPreExecute() {
            if (mProgressDialog != null) {
                mProgressDialog.setMessage("Validating device ...");
            }
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                URL url = new URL("https://api.samsungsami.io/v1.1/devices/registrations/pin");
                String payload = "{\"pin\":" + "\"" + mChallengePin + "\",\"deviceName\":" + "\"" + mOnboardingModule.getName() + "\"}";

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                Log.d(TAG, "mAccessToken : " + mAccessToken + " Challengepin :" + mChallengePin);
                String urlParameters = mAccessToken;
                connection.setRequestMethod("PUT");
                connection.setRequestProperty("USER-AGENT", "Mozilla/5.0");
                connection.setRequestProperty("ACCEPT-LANGUAGE", "en-US,en;0.5");
                connection.setRequestProperty("Content-Type", " application/json");
                connection.setRequestProperty("Accept", " application/json");
                connection.setRequestProperty("Authorization", "  bearer " + mAccessToken);
                Log.d(TAG, "Put Request: " + connection.getURL().toString());
                connection.setDoOutput(true);

                DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
                dStream.writeBytes(payload);
                dStream.flush();
                dStream.close();
                int responseCode = connection.getResponseCode();

                if (responseCode == 200) {
                    return true;
                }
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            Log.d(TAG, "returning null");
            return false;
        }

        protected void onPostExecute(Boolean httpOk) {
            // use the result
            super.onPostExecute(httpOk);
            if (httpOk) {
                mBleProvisioner.setRegistrationState(Constants.REGIS_STATE_USER_CONFIRMED);
                mBleProvisioner.writeCharacteristic(OnboardingGattService.COMPLETE_REGISTRATION_UUID, "1");
            } else {
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.setMessage("Device validation failed");
                    mProgressDialog.dismiss();
                }
                Log.d(TAG, "User Not confirmed");
            }
        }

    }

    public void testLED() {
        mPager.setVisibility(View.INVISIBLE);
        broadcastIntent(Constants.MODULE_ONBOARDED);
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Constants.NEW_MODULE, (Parcelable) mOnboardingModule);
        startActivity(intent);
        mBleProvisioner.disconnectGatt();

        finish();

    }


}
