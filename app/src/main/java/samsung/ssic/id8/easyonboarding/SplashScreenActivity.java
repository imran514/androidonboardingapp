/*
*
* SplashScreenActivity.java
*
* Created by Namrata Garach on 2/22/2016
*
* */

package samsung.ssic.id8.easyonboarding;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashScreenActivity extends Activity {
    private final int SPLASH_DISPLAY_LENGTH =        1000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion < Build.VERSION_CODES.KITKAT) {
                    Toast.makeText(getApplicationContext(), R.string.version_not_suported, Toast.LENGTH_SHORT).show();
                    finish();
                }

                /* Create an Intent that will start the Menu-Activity. */
                Intent mainIntent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                SplashScreenActivity.this.startActivity(mainIntent);
                SplashScreenActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);

    }

    @Override
    protected void onPause(){
        super.onPause();
        finish();
    }

    @Override
    protected void onStop(){
        super.onStop();
        finish();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }
}
