package samsung.ssic.id8.easyonboarding.control;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.util.List;


import io.samsungsami.android.Sami;
import io.samsungsami.client.ApiException;
import io.samsungsami.model.Device;
import io.samsungsami.model.DevicesEnvelope;
import samsung.ssic.id8.easyonboarding.samplecode.SAMISessionConstants;

/**
 * Created by v.nandanavan on 6/4/16.
 */

public class IsDeviceRegistered extends AsyncTask<String, Void, Boolean> {

    private Context context;
    private Sami sami;

    public IsDeviceRegistered(Context context){
        this.context = context;
    }

    @Override
    protected Boolean doInBackground(String... params) {
        sami = Sami.getInstance();
        Log.d("IsDeviceRegistered","GetUserDevice background");
        String userId = params[0];
        String deviceId = "";
        DevicesEnvelope devicesEnvelope = null;
        try {
            devicesEnvelope = sami.getUsersApi().getUserDevices(null, 10, false, userId);
        } catch (ApiException e) {
            e.printStackTrace();
        }
        if(devicesEnvelope !=null) {
            List<Device> devices = devicesEnvelope.getData().getDevices();
            for(Device device:devices){
                if(device.getDtid().equals(SAMISessionConstants.ARTIK5_DEVICE_TYPE_ID)) {
                    //Log.d(TAG, " UserDetails device Id: " + device.getId());  // TODO save to user preferences
                    deviceId = device.getId();
                    return true;
                }
            }
        }
        return false;
    }
}
