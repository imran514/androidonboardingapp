package samsung.ssic.id8.easyonboarding.tabs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import samsung.ssic.id8.easyonboarding.R;

public class GroupsFragmentTab extends Fragment {
    public static GroupsFragmentTab newInstance() {
        GroupsFragmentTab fragment = new GroupsFragmentTab();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.artikgroups_layout, container, false);

//        TextView tv = (TextView) rootView.findViewById(R.id.text);
//        tv.setText(this.getTag() + " Content");
        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

}
