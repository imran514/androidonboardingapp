package samsung.ssic.id8.easyonboarding.tabs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ListView;

import java.util.ArrayList;

import samsung.ssic.id8.easyonboarding.R;
import samsung.ssic.id8.easyonboarding.device_onboarding.DeviceListAdapter;
import samsung.ssic.id8.easyonboarding.device_onboarding.DeviceListObject;

public class Artik1FragmentTab extends Fragment {
    ArrayList<DeviceListObject> mArtik1List;
    WebView mWebView;
    DeviceListAdapter listAdapter;
    private String mAccessToken;

    public static Artik1FragmentTab newInstance() {
        Artik1FragmentTab fragment = new Artik1FragmentTab();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.artik1_layout, container, false);
        mAccessToken = "HelloAccessToken";

        ListView artik0ListView = (ListView) rootView.findViewById(R.id.list);
        mArtik1List = getUserDeviceNamesDummy();
        mWebView = (WebView) rootView.findViewById(R.id.webview);
        mWebView.setVisibility(View.INVISIBLE);
        DeviceListAdapter listAdapter = new DeviceListAdapter(getActivity(),
                R.layout.list_item_card, mArtik1List, mWebView, mAccessToken);
        artik0ListView.setAdapter(listAdapter);
        return rootView;
    }

    public ArrayList<DeviceListObject> getUserDeviceNamesDummy() {
        final ArrayList<DeviceListObject> deviceListObjArrayList = new ArrayList<DeviceListObject>();
/*        DeviceListObject deviceListObject1 = new DeviceListObject("Test-1","Smoke Detector",
                false, false);
        DeviceListObject deviceListObject2= new DeviceListObject("Test-2","Motion Sensor",
                false, false);
        deviceListObjArrayList.add(deviceListObject1);
        deviceListObjArrayList.add(deviceListObject2);*/
        return deviceListObjArrayList;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


}
