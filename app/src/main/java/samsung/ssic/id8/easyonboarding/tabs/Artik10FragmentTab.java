package samsung.ssic.id8.easyonboarding.tabs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import samsung.ssic.id8.easyonboarding.EasyOnboardingApp;
import samsung.ssic.id8.easyonboarding.R;
import samsung.ssic.id8.easyonboarding.control.Module;
import samsung.ssic.id8.easyonboarding.device_onboarding.DeviceListAdapter;
import samsung.ssic.id8.easyonboarding.device_onboarding.DeviceListObject;
import samsung.ssic.id8.easyonboarding.util.Constants;

public class Artik10FragmentTab extends Fragment {
    ArrayList<DeviceListObject> mArtik10List;
    WebView mWebView;
    DeviceListAdapter mListAdapter;
    private String mAccessToken;

    public static Artik10FragmentTab newInstance() {
        Artik10FragmentTab fragment = new Artik10FragmentTab();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.artik10_layout, container, false);
        mAccessToken = "HelloAccessToken";

        ListView artik0ListView = (ListView) rootView.findViewById(R.id.list);
        mArtik10List = getUserDeviceNamesDummy();
        mWebView = (WebView) rootView.findViewById(R.id.webview);
        mWebView.setVisibility(View.INVISIBLE);
        mListAdapter = new DeviceListAdapter(getActivity(),
                R.layout.list_item_card, mArtik10List, mWebView, mAccessToken);
        artik0ListView.setAdapter(mListAdapter);
        DeviceListAdapter listAdapter = new DeviceListAdapter(getActivity(),
                R.layout.list_item_card, mArtik10List, mWebView, mAccessToken);
        artik0ListView.setAdapter(listAdapter);
        EasyOnboardingApp easyOnboardingApp = (EasyOnboardingApp)getActivity().getApplication();
        List<Module> modules = easyOnboardingApp.getModules();
        for (Object object : modules) {
            Module module = (Module)object;
            if (module.getType() == Constants.ARTIK_10) {
                DeviceListObject deviceListObject1 = new DeviceListObject(module.getPlace(), module.getName(),
                        false, false, module);
                mListAdapter.add(deviceListObject1);
            }
        }
        mListAdapter.notifyDataSetChanged();
        return rootView;
    }

    public ArrayList<DeviceListObject> getUserDeviceNamesDummy() {
        final ArrayList<DeviceListObject> deviceListObjArrayList = new ArrayList<DeviceListObject>();
/*        DeviceListObject deviceListObject1 = new DeviceListObject("Test-1","Quadcopter",
                false, false);
        DeviceListObject deviceListObject2= new DeviceListObject("Test-2","HomeRobot",
                false, false);
        deviceListObjArrayList.add(deviceListObject1);
        deviceListObjArrayList.add(deviceListObject2);*/
        return deviceListObjArrayList;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
