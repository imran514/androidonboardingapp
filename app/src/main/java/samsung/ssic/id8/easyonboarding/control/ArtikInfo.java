package samsung.ssic.id8.easyonboarding.control;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.UUID;

import samsung.ssic.id8.easyonboarding.R;
import samsung.ssic.id8.easyonboarding.edgenode.EdgeNodeOnboardingActivity;
import samsung.ssic.id8.easyonboarding.samplecode.SAMISessionConstants;
import samsung.ssic.id8.easyonboarding.util.Constants;
import samsung.ssic.id8.easyonboarding.wifi_onboarding.WifiOnboarding_Activity;

public class ArtikInfo extends AppCompatActivity {
    private static final String TAG = "ArtikInfo";
    private String mModuleType = "Artik 5";
    private Module mModule;
    private String mAccessToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.artik_identified);

        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayShowCustomEnabled(true);
        mActionBar.setCustomView(R.layout.qr_custom_bar);

        final TextView mArtikModuleId = (TextView) findViewById(R.id.identifier);
        final Button okButton = (Button) findViewById(R.id.enter_button);
        final Button skipButton = (Button) findViewById(R.id.skip_button);
        final EditText mArtikModuleName = (EditText) findViewById(R.id.moduleName);
        final EditText mArtikPlaceName = (EditText) findViewById(R.id.moduleLoc);
        final ImageView artikImage = (ImageView) findViewById(R.id.artik_image);
        okButton.setEnabled(false);

        mAccessToken = getIntent().getStringExtra(SAMISessionConstants.ACCESS_TOKEN);
        Log.d(TAG, " ArtikInfo Access token: " + mAccessToken);
        String moduleId = getIntent().getStringExtra(Constants.DISCOVERED_MODULE_ID);
        if (moduleId != null) {
            Log.d(TAG, "Module ID Read " + moduleId);
            mModule = getmModuleInfo(getIntent(), moduleId);
        } else {
            Log.d(TAG, "Unable to Identify module");
            Toast.makeText(this, "Unable to Identify module", Toast.LENGTH_SHORT).show();
            finish();
        }
        //checkIfAlreadyRegistered(mModule);

        setArtikImage(artikImage);

        mArtikModuleName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                String input = null;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    input = v.getText().toString();
                    if (input != null) {
                        okButton.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                        okButton.setEnabled(true);
                    }
                    return false; // consume.
                }
                return false; // pass on to other listeners.
            }
        });
        mArtikModuleName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() != 0) {
                    okButton.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    okButton.setEnabled(true);
                } else {
                    okButton.setBackgroundColor(Color.parseColor("#454545"));
                    okButton.setEnabled(false);
                }
            }
        });

        mArtikModuleName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // TODO Auto-generated method stub

                if (hasFocus) {
                    if (mArtikModuleName.getText().length() < 6) {
                        Toast.makeText(ArtikInfo.this, "Device name min.length 6 characters", Toast.LENGTH_SHORT);
                        mArtikModuleName.setFocusable(true);
                    } else {
                    }
                }
            }
        });


        mArtikPlaceName.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    okButton.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    okButton.setEnabled(true);
                } else {
                    okButton.setBackgroundColor(Color.parseColor("#454545"));
                    okButton.setEnabled(false);
                }
            }
        });

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mArtikModuleName.getText() != null && mArtikModuleName.getText().toString().length() <5){
                    mArtikModuleName.setError("Name should be more than 4 characters");
                    return;
                }
                if (mArtikPlaceName.getText() != null) {
                    mModule.setPlace(mArtikPlaceName.getText().toString());
                }
                if (mArtikModuleName.getText() != null) {
                    mModule.setName(mArtikModuleName.getText().toString());
                }
                sendModuleInfo();
            }
        });
        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendModuleInfo();
            }
        });
    }

    private void checkIfAlreadyRegistered(Module mModule) {
        SharedPreferences preferences = getSharedPreferences(SAMISessionConstants.ONBOARDING_PREFS, Context.MODE_PRIVATE);
        String userId = preferences.getString(SAMISessionConstants.ONBOARDING_USERID_PREF,"");
        if(userId.isEmpty() || userId ==null) {
            return;
        }
        new IsDeviceRegistered(getApplicationContext()){
            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                if(aBoolean) {
                    new AlertDialog.Builder(ArtikInfo.this)
                            .setTitle("ArtikCloud Registration")
                            .setMessage("Device already registered !")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                } else {
                    //device not registered
                }
            }
        }.execute(userId);

    }

    public void sendModuleInfo() {
/*        View view = this.getCurrentFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);*/

        if (mModule.getType() >= Constants.ARTIK_5) {

            Intent data = new Intent(this, WifiOnboarding_Activity.class);
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.MODULE_INFO, mModule);

            data.putExtra(Constants.MODULE_INFO_BUNDLE, bundle);
            data.putExtra(SAMISessionConstants.ACCESS_TOKEN, mAccessToken);
            startActivity(data);
        } else {
            Intent data = new Intent(this, EdgeNodeOnboardingActivity.class);
            //data.putExtra(Constants.MODULE_INFO, mModule);
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.MODULE_INFO, mModule);
            data.putExtra(Constants.MODULE_INFO_BUNDLE, bundle);
            data.putExtra(SAMISessionConstants.ACCESS_TOKEN, mAccessToken);
            startActivity(data);
        }
        finish();
    }

    public Module getmModuleInfo(Intent intent, String uuid) {
        //TODO:GET Device ID from UUID
        //Module module = new Module("SamsungArtik5", "Living Room", Constants.ARTIK_5);
        Bundle bundle = getIntent().getBundleExtra(Constants.MODULE_INFO_BUNDLE);
        Module module = bundle.getParcelable(Constants.MODULE_INFO);
        module.setUuid(UUID.fromString(uuid));
        return module;

    }

    public void setArtikImage(ImageView artikImage) {
        switch (mModule.getType()) {
            case Constants.ARTIK_0:
                artikImage.setImageResource(R.drawable.a0_identified);
                break;
            case Constants.ARTIK_5:
                artikImage.setImageResource(R.drawable.a5_identified);
                break;
            case Constants.ARTIK_7:
                artikImage.setImageResource(R.drawable.a7_identified);
                break;
            case Constants.ARTIK_10:
                artikImage.setImageResource(R.drawable.a10_identified);
                break;
            default:
                artikImage.setImageResource(R.drawable.a5_identified);
                break;

        }
    }

}
