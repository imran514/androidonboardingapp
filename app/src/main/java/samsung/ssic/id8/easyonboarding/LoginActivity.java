/*
*
* LoginActivity.java
*
* Created by Namrata Garach on 2/22/2016
*
* */

package samsung.ssic.id8.easyonboarding;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import io.samsungsami.android.Sami;
import io.samsungsami.android.SamiClient;
import samsung.ssic.id8.easyonboarding.control.TabActivity;
import samsung.ssic.id8.easyonboarding.samplecode.SAMISessionConstants;
import samsung.ssic.id8.easyonboarding.samplecode.SamiServers;


public class LoginActivity extends Activity implements SamiClient {
    public static String TAG = "LoginActivity";
    private static Sami sami;
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Bundle bundle = getIntent().getExtras();
        sharedpreferences = getSharedPreferences(SAMISessionConstants.ONBOARDING_PREFS, Context.MODE_PRIVATE);
        if (bundle != null && bundle.getBoolean("logout")) {
            sami.logout();
        }
        try {
            // Sign up or Log in SAMI account
            signUpLoginSamsungAccount();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void signUpLoginSamsungAccount() {

        sami = Sami.setInstance(this, SamiServers.getDefaultConfig());
        ((EasyOnboardingApp)getApplication()).setSamiInstance(sami);
        String accessToken = sharedpreferences.getString(SAMISessionConstants.ONBOARDING_ACCESS_TOKEN_PREF, null);
        String id = sharedpreferences.getString(SAMISessionConstants.ONBOARDING_USERID_PREF, null);
        if (sami.loadCredentials() && accessToken != null && id != null) {
            navigateToHome(accessToken);
        } else {
            sharedpreferences.edit().
                    remove(SAMISessionConstants.ONBOARDING_ACCESS_TOKEN_PREF).
                    apply();
            sami.login();
        }
    }

    @Override
    public void onLoginCompleted(String accessToken, String refreshToken) {
        try {
            sami.saveCredentials();
            Intent intent = new Intent(LoginActivity.this, TabActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            //Intent intent = new Intent(LoginActivity.this, DeviceListActivity.class);
            // passing the accesstoken obtained during login to use it in further api calls to SAMI
            Log.d("Venkat", " Access token: " + accessToken);
            final SharedPreferences.Editor editor = sharedpreferences.edit();

            editor.putString(SAMISessionConstants.ONBOARDING_USERID_PREF, sami.getUser().getId());
            editor.putString(SAMISessionConstants.ONBOARDING_ACCESS_TOKEN_PREF, accessToken);
            editor.commit();
            intent.putExtra(SAMISessionConstants.ACCESS_TOKEN, accessToken);
            // mArtik5List = getUserDeviceNames(SAMISessionConstants.ARTIK5_DEVICE_TYPE_ID);
            startActivity(intent);
        } catch (Exception e) {
            Log.e("LoginActivity", e.getMessage());
        }

    }

    private void navigateToHome(String accessToken) {
        Intent intent = new Intent(LoginActivity.this, TabActivity.class);

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(SAMISessionConstants.ACCESS_TOKEN, accessToken);
        // mArtik5List = getUserDeviceNames(SAMISessionConstants.ARTIK5_DEVICE_TYPE_ID);
        startActivity(intent);
    }


    @Override
    public void onLoginCanceled() {

    }

    @Override
    public void onInvalidCredentials() {

    }

}
