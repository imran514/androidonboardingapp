/*
*
* ConnectDeviceActivity.java
*
* Created by Namrata Garach on 2/25/16.
*
* */

package samsung.ssic.id8.easyonboarding.device_onboarding;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import samsung.ssic.id8.easyonboarding.R;
import samsung.ssic.id8.easyonboarding.samplecode.SAMISessionConstants;
import samsung.ssic.id8.easyonboarding.wifi_onboarding.WifiOnboarding_Activity;

/* This activity is called when user tries to connect a new device */
public class ConnectDeviceActivity extends Activity {

    String mAccessToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // use the access token saved from login for current SAMI session
        Intent intent = getIntent();
        mAccessToken = intent.getStringExtra(SAMISessionConstants.ACCESS_TOKEN);

        setContentView(R.layout.activity_login);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment()).commit();
        }
    }

    // Scan for bluetooth devices. This process is for onboarding SDR devices
    public void onBoardArtikSEDevice(View view) {
        Intent deviceScanActivityIntent = new Intent(ConnectDeviceActivity.this, WifiOnboarding_Activity.class);
        //Intent deviceScanActivityIntent = new Intent(ConnectDeviceActivity.this, DeviceScanActivity.class);
/*        deviceScanActivityIntent.setClassName(SAMISessionConstants.DEVICE_ONBOARDING_PACKAGE,
                SAMISessionConstants.DEVICE_SCAN_ACTIVITY_PATH);*/
        deviceScanActivityIntent.putExtra(SAMISessionConstants.ACCESS_TOKEN, mAccessToken);
        startActivity(deviceScanActivityIntent);
    }

    // Provide list of devices types available for cloud authentication and ablility to connect them
    public void onBoardCloudAuthDevices(View view) {
        Intent cloudAuthDeviceActivityIntent = new Intent();
        cloudAuthDeviceActivityIntent.setClassName(SAMISessionConstants.DEVICE_ONBOARDING_PACKAGE,
                SAMISessionConstants.ONBOARD_CLOUD_AUTH_DEVICE_ACTIVITY_PATH);
        startActivity(cloudAuthDeviceActivityIntent);
    }

    // Provide list of device type available for non-cloudauth, non-SDR and ability to connect them.
    public void onBoardOtherDevices(View view) {
        Intent otherDevicesActivityIntent = new Intent();
        otherDevicesActivityIntent.setClassName(SAMISessionConstants.DEVICE_ONBOARDING_PACKAGE,
                SAMISessionConstants.ONBOARD_OTHER_DEVICES);
        startActivity(otherDevicesActivityIntent);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_connectors,
                    container, false);
            return rootView;
        }
    }
}
