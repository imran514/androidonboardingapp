/*
*
* DeviceListAdapter.java
*
* Created by Namrata Garach on 2/23/16.
*
* */

package samsung.ssic.id8.easyonboarding.device_onboarding;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import io.samsungsami.model.DeviceType;
import samsung.ssic.id8.easyonboarding.R;
import samsung.ssic.id8.easyonboarding.control.Module;
import samsung.ssic.id8.easyonboarding.samplecode.SAMISessionConstants;
import samsung.ssic.id8.easyonboarding.util.Constants;


public class DeviceListAdapter extends ArrayAdapter<DeviceListObject> {

    private int layoutResourceId;
    private Context context;
    private ArrayList<DeviceListObject> deviceListObjectArrayList = new ArrayList<DeviceListObject>();

    private WebView mWebView;
    private String mAccessToken;
    private String mDeviceId;
    private DeviceType mDeviceType;

    public DeviceListAdapter(Context context, int layoutResourceId, ArrayList<DeviceListObject>
            items, WebView webView, String accessToken) {
        super(context, layoutResourceId, items);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.deviceListObjectArrayList = items;
        this.mWebView = webView;
        this.mAccessToken = accessToken;
    }

    public void add(ArrayList<DeviceListObject>
                            items) {
        deviceListObjectArrayList = items;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final Button authorizeSettingsButton;
        TextView deviceName;
        TextView macAddress;
        final DeviceListObject deviceListObject;

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        row = inflater.inflate(layoutResourceId, parent, false);
        authorizeSettingsButton = (Button) row.findViewById(R.id.authorizeButton);
        deviceName = (TextView) row.findViewById(R.id.deviceName);
        deviceListObject = deviceListObjectArrayList.get(position);
        deviceName.setText(deviceListObject.getDeviceName());
        macAddress = (TextView)row.findViewById(R.id.ip_address_value);
        Module module = deviceListObject.getModule();

        if (module != null) {
            if (module.getType() == Constants.ARTIK_5 || module.getType() == Constants.ARTIK_7 || module.getType() == Constants.ARTIK_10) {
                macAddress.setText(deviceListObject.getModule().getIPAddress());
            } else {
                TextView ipAddressText = (TextView) row.findViewById(R.id.ip_address_text);
                ipAddressText.setVisibility(View.INVISIBLE);
                macAddress.setVisibility(View.INVISIBLE);
            }
        }
        if ((deviceListObject.getRequiresProviderAuth()) && (deviceListObject.getHasCloudConnector()
        )) {
            authorizeSettingsButton.setText("AUTHORIZE");
        } else {
            authorizeSettingsButton.setBackgroundResource(R.drawable.device_info);
        }

        authorizeSettingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (authorizeSettingsButton.getText().equals("AUTHORIZE")) {
                    // authorize cloud connector device
                    // 1. Get authorization token
                    // 2. Start subscription
                    if (deviceListObject.getRequiresProviderAuth()) {
                        mDeviceId = deviceListObject.getDeviceId();
                        if (mAccessToken.isEmpty() || mAccessToken == null || (mAccessToken.length() <= 0)) {
                            getAuthorizationToken();
                        }
                        startSubscription(mAccessToken);
                    } else {
                        // This is not cloud connector device
                    }
                }
            }
        });
        return row;
    }

    private void getAuthorizationToken() {
        loadWebView();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void loadWebView() {
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String uri) {
                if (uri.startsWith(SAMISessionConstants.REDIRECT_URL)) {
                    String[] sArray = uri.split("&");
                    for (String paramVal : sArray) {
                        if (paramVal.indexOf("access_token=") != -1) {
                            String[] paramValArray = paramVal.split("access_token=");
                            mAccessToken = paramValArray[1];
                            break;
                        }
                    }
                    return true;
                }
                // Load the web page from URL (login and grant access)
                return super.shouldOverrideUrlLoading(view, uri);
            }
        });

        //String url = getAuthorizationRequestUri();
       // mWebView.loadUrl(url);
    }

    public void startSubscription(String accessToken) {
        // Open webview in new activity to run start subscription
        Intent authorizeCloudDeviceIntent = new Intent();
        authorizeCloudDeviceIntent.setClassName(SAMISessionConstants.DEVICE_ONBOARDING_PACKAGE,
                "com.samsung.cloudauth.AuthroizeCloudDevicesActivity");
        authorizeCloudDeviceIntent.putExtra(SAMISessionConstants.ACCESS_TOKEN, mAccessToken);
        authorizeCloudDeviceIntent.putExtra("DeviceID", mDeviceId);
        context.startActivity(authorizeCloudDeviceIntent);
    }
/*
    public String getAuthorizationRequestUri() {
        return SAMISessionConstants.SAMI_AUTH_BASE_URL + "/authorize?client=mobile&response_type=" +
                "token&" + "client_id=" + SAMISessionConstants.CLIENT_ID + "&redirect_uri=" +
                SAMISessionConstants.REDIRECT_URL;
    }*/
}