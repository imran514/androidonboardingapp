package samsung.ssic.id8.easyonboarding.control;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import samsung.ssic.id8.easyonboarding.R;
import samsung.ssic.id8.easyonboarding.samplecode.SAMISessionConstants;
import samsung.ssic.id8.easyonboarding.util.Constants;
import samsung.ssic.id8.easyonboarding.wifi_onboarding.OnboardingGattService;

public class Manual_CaptureActivity extends AppCompatActivity {

    private String mAccessToken;
    private AlertDialog.Builder mBuilder;
    private LayoutInflater mLayoutInflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.activity_manual__capture);
        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayShowCustomEnabled(true);
        mActionBar.setCustomView(R.layout.qr_custom_bar);
        mAccessToken = getIntent().getStringExtra(SAMISessionConstants.ACCESS_TOKEN);
        mBuilder = new AlertDialog.Builder(this);


        final EditText artikModuleId = (EditText) findViewById(R.id.moduleId);
        artikModuleId.setImeOptions(EditorInfo.IME_ACTION_DONE);
        artikModuleId.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                String input = null;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    input = v.getText().toString();
                    validate(input);
                    return false; // consume.
                }
                return false; // pass on to other listeners.
            }
        });

    }

    public void validate(String input) {
        if (input != null && input.equalsIgnoreCase(OnboardingGattService.SERVICE_UUID.toString())) {
            goToArtikInfo(input);
        } else {
            new AlertDialog.Builder(this)
                    .setTitle("Invalid DeviceID")
                    .setMessage("Please enter the ID as on Artik kit !")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    }

    public void goToArtikInfo(String result) {
        Intent data = new Intent(Manual_CaptureActivity.this, ArtikInfo.class);
        data.putExtra(Constants.DISCOVERED_MODULE_ID, result);
        data.putExtra(SAMISessionConstants.ACCESS_TOKEN, mAccessToken);
        startActivity(data);
        finish();
    }


}
