package samsung.ssic.id8.easyonboarding.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.ArrayAdapter;

import samsung.ssic.id8.easyonboarding.R;


/**
 * Created by sean.o on 8/25/14.
 */
public class AlertDialogList {
    public static final String TAG = AlertDialogList.class.getSimpleName();
    private static ArrayAdapter<String> arrayAdapter;
    private static AlertDialog.Builder builderSingle;
    private static Context mContext;
    private static Integer action;
    private static int actionIndex = 0;

    public AlertDialogList(final Context context) {
        mContext = context;
        action = -1;
        builderSingle = new AlertDialog.Builder(mContext);
        Drawable icon = ContextCompat.getDrawable(context, R.drawable.artik_small).mutate();
        //builderSingle.setIcon(R.drawable.artik_small);
        builderSingle.setIcon(icon);
    }

    public void show(final String pTitle, final String[] valuesList, final Integer listSize, final Integer pADListIndex) {
        String[] values = new String[listSize];
        System.arraycopy(valuesList, 0, values, 0, listSize);

        action = pADListIndex;
        arrayAdapter = new ArrayAdapter<String>(
                mContext,
                R.layout.lc_select_dialog_single_choice, values);
        builderSingle.setAdapter(arrayAdapter, mADLclick());
        builderSingle.setNegativeButton("CANCEL", mADLCancel());


        builderSingle.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Intent intent1 = new Intent();
                intent1.setAction(Constants.ADD_DEVICES_DIALOG_CANCEL);
                broadcastIntent(intent1);
            }
        });
        builderSingle.setTitle(pTitle);
        builderSingle.show();
    }

    private DialogInterface.OnClickListener mADLclick() {
        DialogInterface.OnClickListener pADLclick = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //final ArrayAdapter<String> wArrayAdapter = arrayAdapter;
                Log.i(TAG, "mADLclick onClick which: " + which);

                if (which == Constants.QR_CODE) {
                    Log.i(TAG, "process QR_CODE selection: " + which);
                    //StartControlPointTask.wemoListChoice(which);
                    //NewDeviceCard.foundWeMo();
                }

                if (which == Constants.NFC_TAG) {
                    Log.i(TAG, "process NFC_TAG selection: " + which);
                }

                if (which == Constants.MANUAL_ENTRY) {
                    Log.i(TAG, "process MANUAL_ENTRY selection: " + which);
                }


                Intent intent1 = new Intent();
                intent1.setAction(Constants.ADD_DEVICES_DIALOG_SELECTED);
                intent1.putExtra("SELECTION", which);
                broadcastIntent(intent1);
                action = -1;
            }
        };
        return pADLclick;
    }

    private DialogInterface.OnClickListener mADLCancel() {
        DialogInterface.OnClickListener pADLCancel = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //final ArrayAdapter<String> wArrayAdapter = arrayAdapter;
                Log.i(TAG, "mADLCancel onClick which: " + which);
//                String strName = wArrayAdapter.getItem(which);


                dialog.dismiss();

                if (which == Constants.QR_CODE) {
                    Log.i(TAG, "Cancel QR_CODE selection");
                }

                if (which == Constants.NFC_TAG) {
                    Log.i(TAG, "Cancel NFC_TAG selection");
                }

                if (which == Constants.MANUAL_ENTRY) {
                    Log.i(TAG, "Cancel MANUAL_ENTRY selection");

                }

                action = -1;
            }
        };
        return pADLCancel;
    }

    public void broadcastIntent(Intent intent) {

        mContext.sendBroadcast(intent);
    }

}
