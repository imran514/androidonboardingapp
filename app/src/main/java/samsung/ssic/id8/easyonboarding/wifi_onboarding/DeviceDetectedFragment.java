package samsung.ssic.id8.easyonboarding.wifi_onboarding;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import samsung.ssic.id8.easyonboarding.R;
import samsung.ssic.id8.easyonboarding.control.Module;
import samsung.ssic.id8.easyonboarding.util.Constants;

public class DeviceDetectedFragment extends Fragment {

    private Module mModule;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.device_found_fragment, container, false);
        ImageView artikImage = (ImageView) v.findViewById(R.id.artik_found_img);
        setArtikImage(artikImage);

        return v;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void setModule(Module module){
        mModule = module;
    }

    public void setArtikImage(ImageView artikImage) {
        switch (mModule.getType()) {
            case Constants.ARTIK_0:
                artikImage.setImageResource(R.drawable.a0_identified);
                break;
            case Constants.ARTIK_5:
                artikImage.setImageResource(R.drawable.a5_identified);
                break;
            case Constants.ARTIK_7:
                artikImage.setImageResource(R.drawable.a7_identified);
                break;
            case Constants.ARTIK_10:
                artikImage.setImageResource(R.drawable.a10_identified);
                break;
            default:
                artikImage.setImageResource(R.drawable.a5_identified);
                break;

        }
    }
}