package io.samsungsami.android.websockets;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterMessage extends JSONObject {
    //TODO: move this to Jackson
    public static final String FIELD_SDID = "sdid";
    public static final String FIELD_TYPE = "type";
    public static final String FIELD_AUTHORIZATION = "Authorization";
    public static final String TYPE_REGISTER = "register";
    private static final String BEARER_PREFIX = "bearer ";

    public RegisterMessage(String sdid, String token) throws JSONException{
        this.put(FIELD_SDID, sdid);
        this.put(FIELD_TYPE, TYPE_REGISTER);
        this.put(FIELD_AUTHORIZATION, BEARER_PREFIX+token);
    }

}
