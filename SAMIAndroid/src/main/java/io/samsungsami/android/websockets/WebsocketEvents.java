package io.samsungsami.android.websockets;

import org.java_websocket.handshake.ServerHandshake;

/**
 * Created by ari
 */
public interface WebsocketEvents {

    /**
     * Triggers on connection
     * @param handshakedata
     */
    public void onOpen(ServerHandshake handshakedata);

    /**
     * Triggers when a message arrives from the server
     * @param message
     */
    public void onMessage(String message);

    /**
     * Triggers on disconnections
     * @param code
     * @param reason
     * @param remote
     */
    public void onClose(int code, String reason, boolean remote);

    /**
     * Triggers if something goes wrong
     * @param ex
     */
    public void onError(Exception ex);

    /**
     * Triggers when a ping message arrives
     */
    public void onPing();

    /**
     * Triggers if the time elapsed between pings is more than the allowed timeout
     */
    public void onPingTimeout();
}
