package io.samsungsami.android.websockets;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkWatcher {
	
	volatile static boolean lastInternetStatus = true;

	/**
	 * Returns internet connection/disconnection status
	 * @return
	 */
	public static boolean isNetworkAvailable(Context context) {
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    
	    lastInternetStatus = activeNetworkInfo != null && activeNetworkInfo.isConnected();
	    return lastInternetStatus;
	}

	/**
	 * Gets the previous status of the internet connection
	 * @return
	 */
	public static boolean getLastInternetStatus() {
		return lastInternetStatus;
	}
}
