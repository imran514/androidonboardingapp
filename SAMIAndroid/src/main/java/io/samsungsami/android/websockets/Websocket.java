package io.samsungsami.android.websockets;

import android.util.Log;

import org.java_websocket.WebSocketImpl;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;

/**
 * Created by ari
 */
public class Websocket {
    private static final String TAG = Websocket.class.getName();
    private WebsocketHandler wssClient;

    /**
     * Connect to a websocket endpoint
     * @param url
     * @param websocketEvents
     */
    public void connect(String url, WebsocketEvents websocketEvents){
        System.setProperty("http.keepAlive", "false");
        HttpsURLConnection
                .setDefaultHostnameVerifier(new HostnameVerifier() {

                    public boolean verify(String hostname,
                                          SSLSession session) {
                        return true;
                    }
                });
        SSLSocketFactory factory = (SSLSocketFactory) SSLSocketFactory.getDefault();

        WebSocketImpl.DEBUG = false;
        release();
        try {
            wssClient = new WebsocketHandler( new URI( url ), websocketEvents );
            wssClient.setSocket( factory.createSocket() );
            wssClient.connectBlocking();
        } catch (URISyntaxException e) {
            Log.d(TAG, e.toString());
        } catch (IOException e) {
            Log.d(TAG, e.toString());
        } catch (InterruptedException e) {
            Log.d(TAG, e.toString());
        } catch (NullPointerException e) {
            Log.d(TAG, e.toString());
        }
    }

    /**
     * Closes the socket and releases the resource, no matter what
     */
    private void release(){
        try {
            wssClient.closeBlocking();
        } catch (InterruptedException e) {
            // Log.d(TAG, e.toString());
        } catch (NullPointerException e) {
            // Log.d(TAG, e.toString());
        } catch (Exception e){
            // Log.d(TAG, e.toString());
        }
        try{
            wssClient.setOutsideWorld(null);
        } catch (Exception e){
            // Log.d(TAG, e.toString());
        }
        wssClient = null;
    }

    /**
     * Socket is not reusable, so we release it to create a new one.
     */
    public void disconnect(){
        release();
    }

    /**
     * Sends a message through the socket
     * @param message
     */
    public void send(String message){
        if(wssClient != null)
            wssClient.send(message);
    }

    public boolean isConnected(){
        return (wssClient != null && wssClient.isOpen());
    }
    public boolean isConnecting(){
        return (wssClient != null && wssClient.isConnecting());
    }
    public boolean isConnectedOrConnecting(){
        return isConnected() || isConnecting();
    }
}
