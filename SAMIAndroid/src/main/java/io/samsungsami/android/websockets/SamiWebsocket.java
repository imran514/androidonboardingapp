package io.samsungsami.android.websockets;

import android.util.Log;

import org.java_websocket.handshake.ServerHandshake;

/**
 * Created by ari on 03/03/15.
 */
public abstract class SamiWebsocket {
    private final static String TAG = SamiWebsocket.class.getName();
    protected Websocket websocket;
    protected WebsocketEvents websocketEvents;
    protected String connectionString;

    /**
     * Connects to the websocket, requires to define a WebsocketEvents object first
     * @param connectionString the url to connect
     */
    public void connect(String connectionString) {
        if(this.websocketEvents == null){
            Log.w(TAG, "Please set the WebsocketEvents handler at your code before calling this method. Using dummy handler.");
            websocketEvents = new WebsocketEvents() {
                @Override
                public void onOpen(ServerHandshake handshakedata) {
                    Log.w(TAG, "onOpen()");
                }

                @Override
                public void onMessage(String message) {
                    Log.w(TAG, "onMessage(): "+message);
                }

                @Override
                public void onClose(int code, String reason, boolean remote) {
                    Log.w(TAG, "onClose(): "+code);
                }

                @Override
                public void onError(Exception ex) {
                    Log.w(TAG, "onClose(): "+ex.getMessage());
                }

                @Override
                public void onPing() {
                    Log.w(TAG, "onPing()");
                }

                @Override
                public void onPingTimeout() {
                    Log.w(TAG, "onPingTimeout()");
                }
            };
        }
        if(websocket == null) {
            websocket = new Websocket();
        }
        if(!websocket.isConnected() && !websocket.isConnecting()) {
            this.connectionString = connectionString;
            websocket.connect(connectionString, this.websocketEvents);
        }
    }

    /**
     * Disconnects the websocket
     */
    public void disconnect() {
        if (websocket != null && websocket.isConnected()){
            websocket.disconnect();
        }
    }

    /**
     * Set the websocket events handler, call this method inside your websocket class constructor
     * @param websocketEvents
     */
    protected void setWebsocketEvents(WebsocketEvents websocketEvents){
        this.websocketEvents = websocketEvents;
    }

    /**
     * Monitor flag
     * @return
     */
    public boolean isConnected(){
        return websocket != null && websocket.isConnected();
    }

    /**
     * Monitor flag
     * @return
     */
    public boolean isConnectedOrConnecting(){
        return websocket != null && websocket.isConnectedOrConnecting();
    }

    /**
     * Sends a message through the socket
     * @param message
     */
    public void send(String message){
        websocket.send(message);
    }

}
