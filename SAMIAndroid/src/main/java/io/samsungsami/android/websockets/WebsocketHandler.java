package io.samsungsami.android.websockets;

import android.util.Log;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;

public class WebsocketHandler extends WebSocketClient {
    private WebsocketEvents outsideWorld;
    private long lastTs = 0;
    private static final long PING_TIMEOUT = 35000;

    public WebsocketHandler(URI serverURI, WebsocketEvents outsideWorld) {
        super(serverURI);
        this.outsideWorld = outsideWorld;
    }

    @Override
    public void onOpen( ServerHandshake handshakedata ) {
        if(outsideWorld != null)
            outsideWorld.onOpen(handshakedata);
    }

    /**
     * The ping is dropped from onMessage (there is a special event meant for this)
     * @param message
     */
    @Override
    public void onMessage( String message ) {
        if(isPing(message)){
            onPing();
        } else {
            if(outsideWorld != null)
                outsideWorld.onMessage(message);
        }
    }

    @Override
    public void onClose( int code, String reason, boolean remote ) {
        if(outsideWorld != null)
            outsideWorld.onClose(code, reason, remote);
    }

    @Override
    public void onError( Exception ex ) {
        if(outsideWorld != null)
            outsideWorld.onError(ex);
    }

    /**
     * Controls timeout between pings
     */
    public void onPing() {
        long currentTs = System.currentTimeMillis();
        long elapsed = (currentTs - lastTs);
        if (outsideWorld != null && (lastTs > 0 && elapsed > PING_TIMEOUT) ) {
            outsideWorld.onPingTimeout();
        }
        lastTs = currentTs;
        if(outsideWorld != null)
            outsideWorld.onPing();
    }

    // TODO: find a faster way to do this
    private boolean isPing(String message){
        return (message.contains("\"type\":\"ping\""));
    }

    public void setOutsideWorld(WebsocketEvents outsideWorld) {
        this.outsideWorld = outsideWorld;
    }
}
