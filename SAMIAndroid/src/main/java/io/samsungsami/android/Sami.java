package io.samsungsami.android;

import io.samsungsami.android.api.Call;
import io.samsungsami.android.api.Cast;
import io.samsungsami.android.api.Code;

import io.samsungsami.android.models.AccessToken;
import io.samsungsami.android.websockets.RegisterMessage;
import io.samsungsami.api.DeviceTypesApi;
import io.samsungsami.api.DevicesApi;
import io.samsungsami.api.MessagesApi;
import io.samsungsami.api.UsersApi;
import io.samsungsami.client.ApiException;
import io.samsungsami.model.User;
import io.samsungsami.model.UserEnvelope;

import io.samsungsami.android.utils.FileUtils;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.WeakHashMap;

public class Sami implements SamiClient {
    public static final String TAG = Sami.class.getName();
    private static final Sami instance = new Sami();
    private static final String BEARER_FILE = "session.data";
    private static boolean isVisible;
    private static boolean isRunningInBackground = false;
    private static Credentials credentials;
    private static SamiStack samiStack;
    private static Timer timer;
    private static final long REFRESH_TOKEN_PERIOD = 28800000; // each 8 hours
    private static WeakHashMap<String, Context> clientPool;
    private static Handler mHandler;

    /**
     * This is now a single instance class
     */
    private Sami(){
        clientPool = new WeakHashMap<String, Context>();
        setRefreshTokenTimer();
        mHandler = new Handler();
    }

    /**
     * Sets the configuration for the SAMI auth instance
     * @param context the activity or service implementing SamiClient interface
     * @param samiStack the Sami server configuration object
     * @return an object that lets you interact with SAMI
     */
    public static Sami setInstance(Context context, SamiStack samiStack){
        String className = context.getClass().getName();
        Log.d(TAG, "setInstance(): " + className);
        unregister(context);
        if (!getInstance().clientPool.containsKey(className)){
            getInstance().clientPool.put(className, context);
            Log.d(TAG, "setInstance():  Registered: " + className + " client pool size: " +getInstance().clientPool.entrySet().size());
        }
        getInstance().setSamiStack(context, samiStack);
        return getInstance();
    }

    /**
     * Changes the configuration an reload the credentials for the current instance
     * @param context the activity or service implementing SamiClient interface
     * @param samiStack the Sami stack configuration object
     */
    private void setSamiStack(Context context, SamiStack samiStack){
        if(Service.class.isAssignableFrom(context.getClass())){
            isRunningInBackground = true;
        }
        Sami.samiStack = samiStack;
        if(!loadCredentials()){
            Log.d(TAG, "setSamiStack(): Credentials and token are not set.");
        }
    }

    /**
     * Removes a context from the client pool
     * @param context
     */
    private static boolean unregister(Context context){
        String className = context.getClass().getName();
        if (getInstance().clientPool.containsKey(className)){
            getInstance().clientPool.remove(className);
            Log.d(TAG, "unregister():  Removed: " + className + " client pool size: " +getInstance().clientPool.entrySet().size());
            return true;
        } else{
            return false;
        }

    }

    /**
     * Saves the current credentials to a file
     * Useful to make them persist between sessions
     * @return
     */
    public boolean saveCredentials(){
        return FileUtils.savePrivateFile(getContext(), BEARER_FILE,
                Credentials.toJson(credentials));
    }

    /**
     * Removes credentials file from device
     * @return
     */
    public boolean deleteCredentials(){
        credentials = null;
        return FileUtils.deletePrivateFile(getContext(), Sami.BEARER_FILE);
    }

    /**
     * Loads the current stored credentials
     * @return
     */
    public synchronized boolean loadCredentials(){
        credentials = null;
        String fileContent = FileUtils.readPrivateFile(getContext(), BEARER_FILE);
        if(fileContent != null){
            credentials = Credentials.fromJson(fileContent);
        }
        return (credentials != null);
    }

    /**
     * Sets the desired credentials, and saves them to persist
     * @param credentials
     */
    public void setCredentials(Credentials credentials){
        Sami.credentials = credentials;
        saveCredentials();
        loadCredentials();
    }

    /**
     * Gets the current logged in user credentials
     * @return
     */
    public Credentials getCredentials(){
        return credentials;
    }

    /**
     * Gets the current logged in user
     * @return
     */
    public User getUser(){
        User user = null;
        if(credentials != null){
            user = userSelf(credentials.getToken());
        }
        return user;
    }

    /**
     * Gets the current access token
     * @return
     */
    public synchronized String getToken(){
        if(credentials != null){
            return credentials.getToken();
        } else {
            Log.d(TAG, "getToken(): Credentials are null.");
            return null;
        }
    }

    /**
     * Displays the login activity, controls flood of login windows
     * Calls onLoginCompleted after login
     * Calls onLoginCancelled if user cancels the dialog
     */
    public void login(){
        if(isVisible){
            return;
        }
        isVisible = true;
        AccessToken refreshedToken = refreshToken();
        if (refreshedToken != null) {
            Log.d(TAG, "login(): Refreshed token");
            onLoginCompleted(refreshedToken.getAccessToken(), refreshedToken.getRefreshToken());
        } else {
            String url = samiStack.getAccountsUrl() + samiStack.getLoginUrl();
            Intent intent = new Intent(getContext(), AccountsActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("url", url);
            Log.d(TAG, "login(): Starting Accounts activity");
            getContext().startActivity(intent);
        }
    }

    /**
     * Triggered after a successful login
     * Obtains the User model using the new access token
     */
    @Override
    public void onLoginCompleted(final String accessToken, final String refreshToken) {
        Log.d(TAG, "onLoginCompleted(): client pool size: " + getClientPool().entrySet().size());
        isVisible = false;
        User user = userSelf(accessToken);
        if(user != null) {
            setCredentials(new Credentials(accessToken,
                    user.getId(),
                    user.getName(),
                    user.getEmail(),
                    AccountsWebView.getLatestPassword(),
                    refreshToken));
            for (final Map.Entry<String, Context> context : getClientPool().entrySet()) {
                Log.d(TAG, "onLoginCompleted(): Notifying " + context.getKey());
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ((SamiClient) context.getValue()).onLoginCompleted(accessToken, refreshToken);
                    }
                }, 10);
            }
        } else {
            onInvalidCredentials();
        }
    }

    /**
     * Starts a timer that will try to refresh the token at a fixed interval
     */
    private void setRefreshTokenTimer(){
        if(timer != null) {
            timer.cancel();
        }
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if(getInstance() != null) {
                    AccessToken refreshedToken = getInstance().refreshToken();
                    if (refreshedToken != null) {
                        Log.d(TAG, "Refreshed token timer: " + refreshedToken.toString());
                        getInstance().onLoginCompleted(refreshedToken.getAccessToken(), refreshedToken.getRefreshToken());
                    }
                }
            }
        }, 0, REFRESH_TOKEN_PERIOD);
    }

    /**
     * Called from the webview if the screen is cancelled
     */
    public void onLoginCanceled() {
        isVisible = false;
        ((SamiClient) getContext()).onLoginCanceled();
    }

    private String getPostReponseAsString(HttpPost httpPost){
        String responseString = null;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response = httpclient.execute(httpPost);
            int code = response.getStatusLine().getStatusCode();
            if (code == 204)
                responseString = "";
            else if (code >= 200 && code < 300) {
                if (response.getEntity() != null) {
                    HttpEntity resEntity = response.getEntity();
                    responseString = EntityUtils.toString(resEntity);
                }
            } else {
                if (response.getEntity() != null) {
                    HttpEntity resEntity = response.getEntity();
                    responseString = EntityUtils.toString(resEntity);
                } else
                    responseString = "no data";
                throw new ApiException(code, responseString);
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ApiException e) {
            e.printStackTrace();
        }
        return responseString;
    }
    /**
     *  Returns true if session and token is still valid, sync call. See DAC-64 for details.
     */
    public boolean hasValidToken() {
        Object result = null;
        if(getToken() != null) {
            result = new Call(new Code() {

                @Override
                public Object run() {
                    String url = getSamiStack().getAccountsUrl() + "/checkToken";
                    Log.d(TAG, "Check token ( " + getToken() + " ) URL: " + url);
                    HttpPost httpPost = new HttpPost(url);
                    httpPost.setHeader("Content-type", "application/json");
                    try {
                        JSONObject body = new JSONObject();
                        body.put("token", getToken());
                        httpPost.setEntity(new StringEntity(body.toString(), "UTF8"));
                    } catch (Exception e) {
                        Log.d(TAG, "Check token bad body :"+ e.getMessage());
                        return null;
                    }
                    return getPostReponseAsString(httpPost);
                }
            }).executeInSync();
        }
        if (result != null) {
            String checkTokenResponse  = (String) result;
            Log.d(TAG, "Check token response: " + checkTokenResponse);
            return checkTokenResponse.contains("Valid token");
        } else {
            Log.d(TAG, "Check token error: token or response is null");
            return false;
        }
    }

    /**
     *  Exchanges the refreshToken by a new accessToken+refreshToken.
     *  Triggers onLoginCompleted(accessToken,refreshToken) or onInvalidCredentials()
     *  WARNING: This is a workaround until the official SAMI Android SDK is updated with the auth endpoints
     */
    public AccessToken refreshToken() {
        if (credentials != null && credentials.getRefreshToken() != null) {
            Object result = new Call(new Code() {

                @Override
                public Object run() {
                    if (getCredentials().getRefreshToken() != null) {
                        String refreshUrl = getSamiStack().getAccountsUrl() + "/token?grant_type=refresh_token&refresh_token=" + getCredentials().getRefreshToken();
                        Log.d(TAG, "Refresh token ( "+getToken()+" ) URL: " + refreshUrl);
                        HttpPost httpPost = new HttpPost(refreshUrl);
                        httpPost.getParams().setParameter("grant_type", "refresh_token");
                        httpPost.getParams().setParameter("refresh_token", getCredentials().getRefreshToken());
                        httpPost.setHeader("Authorization", "bearer " + getToken());
                        return getPostReponseAsString(httpPost);
                    } else {
                        return null;
                    }
                }
            }).executeInSync();

            if (result != null) {
                Log.d(TAG, "Refresh token response: " + (String) result);
                AccessToken refreshedToken = AccessToken.fromJson((String) result);
                return refreshedToken;
            } else {
                Log.d(TAG, "Refresh token response is null");
                return null;
            }
        } else {
            Log.d(TAG, "Refresh token cache is null, skip refresh token call");
            return null;
        }
    }

    /**
     *  Returns User model if token is valid, sync call.
     */
    public User userSelf(final String accessToken) {

        Object result = new Call(new Code() {

            @Override
            public Object run() {
                UsersApi usersApi = SamiHub.getUsersApi(samiStack.getGaiaUrl(), accessToken);
                try {
                    UserEnvelope userEnvelope = usersApi.self();
                    User user = userEnvelope.getData();
                    return user;
                } catch (ApiException e) {
                    e.printStackTrace();
                } catch (Exception e){
                    e.printStackTrace();
                }
                return null;
            }
        }).executeInSync();

        return Cast.as(User.class, result);

    }

    /**
     * Triggered if access token is not valid anymore
     */
    @Override
    public void onInvalidCredentials() {
        // Avoid potential client infinite loop by setting the refresh token to null.
        // We keep other credentials for autologin
        if(credentials != null) {
            credentials.setRefreshToken(null);
            saveCredentials();
        }
        for (final Map.Entry<String, Context> context : getClientPool().entrySet()) {
            Log.d(TAG, "onInvalidCredentials(): Notifying " + context.getKey());
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ((SamiClient) context).onInvalidCredentials();
                }
            }, 10);
        }
    }

    /**
     * Returns whether the login for is currently displayed or not
     * @return
     */
    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean isVisible) {
        Sami.isVisible = isVisible;
    }

    /**
     * Returns current configuration
     * @return
     */
    public SamiStack getSamiStack(){
        return samiStack;
    }

    /**
     * Returns a valid context (any) already binded to this library instance
     * @return
     */
    public Context getContext() {
        for (final Map.Entry<String, Context> entry : clientPool.entrySet()) {
            if(entry.getValue() != null)
                return entry.getValue();
        }
        return null;
    }

    public static WeakHashMap<String, Context> getClientPool() {
        return clientPool;
    }


    /**
     * Returns if the dialogs will popup from a background process
     * @return
     */
    public boolean isRunningInBackground() {
        return isRunningInBackground;
    }

    /**
     * Sets if the dialogs should popup with special permissions
     * to be displayed from a background process
     * @param isRunningInBackground
     */
    public void setRunningInBackground(boolean isRunningInBackground) {
        Sami.isRunningInBackground = isRunningInBackground;
    }

    /**
     * Returns the default instance of the authentication library
     * @return
     */
    public static Sami getInstance(){
        return Sami.instance;
    }

    /**
     * For now this removes client side credentials
     * @return
     */
    public boolean logout(){
        return deleteCredentials();
    }

    /**
     * Gets Chronos API invoker for current configuration
     * @return
     */
    public MessagesApi getMessagesQueryApi(){
        return SamiHub.getMessagesApi(samiStack.getChronosUrl(), getToken());
    }

    /**
     * Gets Connectors API invoker for current configuration
     * @return
     */
    public MessagesApi getMessagesPostApi(){
        return SamiHub.getMessagesApi(samiStack.getConnectorsUrl(), getToken());
    }
    public MessagesApi getMessagesPostApi(String customToken){
        return SamiHub.getMessagesApi(samiStack.getConnectorsUrl(), customToken);
    }

    /**
     * Gets Gaia API invoker for current configuration
     * @return
     */
    public UsersApi getUsersApi(){
        return SamiHub.getUsersApi(samiStack.getGaiaUrl(), getToken());
    }

    /**
     * Gets Gaia API invoker for current configuration
     * @return
     */
    public DevicesApi getDevicesApi(){
        return SamiHub.getDevicesApi(samiStack.getGaiaUrl(), getToken());
    }

    /**
     * Gets Gaia API invoker for current configuration
     * @return
     */
    public DeviceTypesApi getDeviceTypesApi(){
        return SamiHub.getDevicetypesApi(samiStack.getGaiaUrl(), getToken());
    }

    /**
     * Gets a registration message for the websocket connection
     * @param sdid
     * @return
     */
    public String getWebsocketRegisterMessage(String sdid){
        try {
            return new RegisterMessage(sdid, getToken()).toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Gets a registration message for the websocket connection and an access token
     * @param sdid
     * @param someToken
     * @return
     */
    public String getWebsocketRegisterMessage(String sdid, String someToken){
        try {
            return new RegisterMessage(sdid, someToken).toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Once authenticated, returns the connection String for /live websocket
     * @return
     */
    public String getLiveConnectionString(){
        String baseUrl = getSamiStack().getWebsocketUrl();
        String userId = "";
        String token = "";
        if(getCredentials() != null){
            userId = getCredentials().getId();
            token = getCredentials().getToken();
        }
        baseUrl += SamiStack.PIPE_LIVE + "?userId="+userId+"&Authorization=bearer+"+token;
        return baseUrl;
    }

    /**
     * Once authenticated, returns the connection String for /websocket websocket
     * @return
     */
    public String getWebsocketConnectionString(boolean ack){
        String baseUrl = getSamiStack().getWebsocketUrl();
        baseUrl += SamiStack.PIPE_WEBSOCKET + (ack ? "?ack=true" : "");
        return baseUrl;
    }

}
