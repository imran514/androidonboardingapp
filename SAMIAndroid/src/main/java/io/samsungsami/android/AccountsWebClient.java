package io.samsungsami.android;

import android.annotation.SuppressLint;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class AccountsWebClient extends WebViewClient{
	
	public static final String TAG = AccountsWebClient.class.getName();
	private static final String FORM_SELECTOR = "document.querySelectorAll('form[id*=\"input\"], form[id*=\"signin\"]')[0].id";
	private static final String FORM_USERNAME_SELECTOR = "document.querySelectorAll('input[id*=\"User\"], input[id*=\"user\"]')[0].id";
	private static final String FORM_PASSWORD_SELECTOR = "document.querySelectorAll('input[id*=\"Pass\"], input[id*=\"pass\"]')[0].id";
	private static final String SUBMIT_SELECTOR = "document.querySelectorAll('input[type=\"submit\"]')[0]";
	
	public AccountsWebClient(){}

	public static Map<String, String> buildQueryMap(String query){
		String[] params = query.split("&");
		Map<String, String> map = new HashMap<String, String>();
		for (String param : params)
		{
			String[] pair = param.split("=");
			String name = pair[0];
			if(name.contains("#")){
				name = name.substring(name.indexOf("#")+1);
			}
			String value = pair[1];
			map.put(name, value);
		}
		return map;
	}

	/**
	 * Everything is handled by the webview, true to handle download manually
	 */
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
    	if(isAuthorizeUrl(url)) {
    		Log.d(TAG, "shouldOverrideUrlLoading page URL: "+url);
            String encodedUrl;
            try {
                encodedUrl = URLEncoder.encode(url, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                encodedUrl = url; // fallback
            }

            Map<String, String> queryMap = buildQueryMap(url);
            String accessToken = queryMap.get("access_token");
            String refreshToken = null;
            // Fix until this is deployed to PROD
            if(queryMap.containsKey("refresh_token")) {
                refreshToken = queryMap.get("refresh_token");
            }

    		//TODO: clear the window, but this triggers another page load event
    		//view.loadData("","text/plain","utf-8"); 
    		view.clearView();
    		String currentToken = Sami.getInstance().getToken();
    		if(Sami.getInstance().isVisible() && (currentToken == null 
    			|| !currentToken.equalsIgnoreCase(accessToken))){
    			Sami.getInstance().setVisible(false);
    			((AccountsWebView) view).onLoginCompleted(accessToken, refreshToken);
    		}
        }
    	return false;
    }
    
    /**
     * Guess if the url is the login form
     * @param url
     * @return
     */
    public boolean isLoginForm(String url){
    	return ((url.contains("response_type=") || url.contains("check.do")) && !url.contains("signin"));
    }
    
    /**
     * Guess if the url belongs to SA service
     * @param url
     * @return
     */
    public boolean isSALoginForm(String url){
    	return (url.contains("check.do") && !url.contains("signin"));
    }
    
    /**
     * Guess if the url is the one that contains the access token
     * @param url
     * @return
     */
    public boolean isAuthorizeUrl(String url){
		return url.contains("access_token=");
	}
    
    public void onPageFinished(WebView view, String url) {
    	onPageFinishedAccounts((AccountsWebView)view, url);
    }
    
    /**
     * Injects javascript to get the user credentials on the login form
     * @param view
     * @param url
     */
    @SuppressLint("NewApi")
	public void onPageFinishedAccounts(AccountsWebView view, String url) {
    	if (isLoginForm(url)){
    		String captureScript = 
    				"try{" +
	    				"document.getElementById("+FORM_SELECTOR+").onsubmit=function(){" +
	    					"var usernameFieldId = " + FORM_USERNAME_SELECTOR + ";" +
	    					"var passwordFieldId = " + FORM_PASSWORD_SELECTOR + ";" +
	    					"window.myInterface.setCredentials(" +
	            			"document.getElementById(usernameFieldId).value," +
	            			"document.getElementById(passwordFieldId).value" +
	            			");" +
	    				"};" +
    				"}catch(e){}";
        	if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) { 
        		view.loadUrl("javascript:"+captureScript);
        	}
        	else{
        		view.evaluateJavascript(captureScript, null);
        	}
    		
    		//Try to auto fill the login form
    		String username = "";
    		String password = "";
    		if(Sami.getInstance().getCredentials() != null){
    			username = Sami.getInstance().getCredentials().getEmail();
    			password = Sami.getInstance().getCredentials().getPassword();
    		}
    		String setScript = "try{" +
    				"var usernameFieldId = " + FORM_USERNAME_SELECTOR + ";" +
					"var passwordFieldId = " + FORM_PASSWORD_SELECTOR + ";" +
    				"document.getElementById(usernameFieldId).value = '"+username+"';" +
    				"document.getElementById(passwordFieldId).value = '"+password+"';" +
    				"}catch(e){}";
    		// Autosubmit if both credentials are available
            if(username.length() > 0 && password.length() > 0){
                setScript = "try{" +
                        "var usernameFieldId = " + FORM_USERNAME_SELECTOR + ";" +
                        "var passwordFieldId = " + FORM_PASSWORD_SELECTOR + ";" +
                        "document.getElementById(usernameFieldId).value = '"+username+"';" +
                        "document.getElementById(passwordFieldId).value = '"+password+"';" +
                        "setTimeout(function(){"+SUBMIT_SELECTOR+".click();},500);" +
                        "}catch(e){}";
            }
    		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) { 
    			view.loadUrl("javascript:"+setScript); 
    		}
    		else{
    			view.evaluateJavascript(setScript, null);
    		}
    		
    	}
    	else{
    		Log.d(TAG, "onPageFinished URL: "+url);
    	}
    	
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        view.requestFocus(View.FOCUS_DOWN);
    }

    
}
