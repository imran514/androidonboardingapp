package io.samsungsami.android.models;

public class Device {
	public static final String PROPERTY_DEVICE_TYPE_ID = "dtid";
	public static final String PROPERTY_SOURCE_DEVICE_ID = "sdid";
	public static final String PROPERTY_DATE = "ts";
	public static final int DEVICE_NAME_MIN_LENGTH = 5;
	public String deviceTypeId;
	public String id;
	public String name;
	
	public Device(String deviceTypeId, String id, String name){
		this.deviceTypeId = deviceTypeId;
		this.id = id;
		this.name = name;
	}

	@Override
	public String toString() {
		return name+" ("+id+")";
	}
	
	
	
}
