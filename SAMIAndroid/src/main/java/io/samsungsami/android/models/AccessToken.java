package io.samsungsami.android.models;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * Created by ari on 11/02/15.
 */
public class AccessToken {
    public String access_token;
    public String token_type;
    public Integer expires_in;
    public String refresh_token;

    /** Load the access token from a accounts/token service call. */
    public AccessToken() {
    }

    /** Load the access token directly from the params. */
    public AccessToken(String token, String tokenType, Integer expiresIn, String refreshToken) {
        this.access_token = token;
        this.token_type = tokenType;
        this.expires_in = expiresIn;
        this.refresh_token = refreshToken;
    }

    public String getAccessToken() {
        return access_token;
    }

    public void setAccessToken(String access_token) {
        this.access_token = access_token;
    }

    public String getTokenType() {
        return token_type;
    }

    public void setTokenType(String token_type) {
        this.token_type = token_type;
    }

    public Integer getExpiresIn() {
        return expires_in;
    }

    public void setExpiresIn(Integer expires_in) {
        this.expires_in = expires_in;
    }

    public String getRefreshToken() {
        return refresh_token;
    }

    public void setRefreshToken(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    /**
     * Returns an AccessToken object from a JSON string
     * @return
     */
    public static AccessToken fromJson(String json){
        AccessToken accessToken = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            accessToken = mapper.readValue(json, AccessToken.class);
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return accessToken;
    }

    /**
     * Returns a JSON string from an AccessToken object
     * @return
     */
    public static String toJson(AccessToken accessToken){
        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = mapper.writeValueAsString(accessToken);
        } catch (JsonGenerationException ex) {
            ex.printStackTrace();
        } catch (JsonMappingException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return json;
    }

    @Override
    public String toString()  {
        StringBuilder sb = new StringBuilder();
        sb.append("class User {\n");

        sb.append("  access_token: ").append(access_token).append("\n");
        sb.append("  token_type: ").append(token_type).append("\n");
        sb.append("  expires_in: ").append(expires_in).append("\n");
        sb.append("  refresh_token: ").append(refresh_token).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
