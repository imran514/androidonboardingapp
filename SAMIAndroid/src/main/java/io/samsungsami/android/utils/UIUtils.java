package io.samsungsami.android.utils;

import java.util.ArrayList;

import com.samsung.samiauth.R;

import io.samsungsami.android.models.Device;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout.LayoutParams;

public class UIUtils {
	
	/**
	 * Returns an imageview with the desired image
	 * @param context
	 * @param resource
	 * @return
	 */
	public static ImageView getImageView(Context context, int resource){
		ImageView imageView = new ImageView(context);
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		params.gravity = Gravity.CENTER;
		imageView.setScaleType(ScaleType.CENTER_INSIDE);
		imageView.setLayoutParams(params);
		imageView.setImageResource(resource);
		return imageView;
	}
	
	/**
	 * Returns a textview with the desired string
	 * @param context
	 * @param text
	 * @return
	 */
	public static TextView getTextView(Context context, String text){
		TextView textView = new TextView(context);
		LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		params.gravity = Gravity.CENTER;
		textView.setTextColor(Color.WHITE);
		textView.setLayoutParams(params);
		textView.setText(text);
		return textView;
	}
	
	/**
	 * Shows an alert dialog, optionally can pass a runnable to do onOk
	 * @param context
	 * @param title
	 * @param text
	 * @param onOk Runnable to run or null
	 */
	public static void alertDialog(Context context, String title, String text, final Runnable onOk) {
		confirmDialog(context, title, text, onOk, null, false);
	}
	

	public static void confirmDialog(Context context, String title, String text, final Runnable onOk) {
		confirmDialog(context, title, text, onOk, null, true);
	}

    /**
     * Shows a dialog to confirm a Runnable.run()
     * @param context
     * @param title
     * @param text
     * @param onOk runnable to execute
     * @param onCancel runnable to execute
     * @param allowNegative if set displays cancel button
     */
	public static void confirmDialog(Context context, String title, String text, final Runnable onOk, final Runnable onCancel, boolean allowNegative) {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(title);
		builder.setMessage(text);
		builder.setCancelable(true);
		builder.setIcon(android.R.drawable.ic_dialog_alert);
		
		// Set up the buttons
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() { 
		   
		    public void onClick(DialogInterface dialog, int which) {
		    	dialog.dismiss();
		    	if(onOk != null){
		    		onOk.run();
		    	}
		    }
		});
		if(allowNegative){
			builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			   
			    public void onClick(DialogInterface dialog, int which) {
			        dialog.cancel();
			        if(onCancel != null){
			        	onCancel.run();
			        }
			    }
			});
		}

		builder.show();
	}
	
	/**
	 * Shows a dialog to ask for text
	 * @param context
	 * @param title
	 * @param text
	 * @param onOk
	 */
	public static void inputDialog(Context context, String title, String text, final UIUtilsOnOk onOk, String defaultValue) {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(title);
		builder.setCancelable(true);
		builder.setIcon(android.R.drawable.ic_dialog_info);

		// Set up the input
		LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		final TextView tv = new TextView(context);
		tv.setLayoutParams(layoutParams);
		tv.setText(text);
		final EditText input = new EditText(context);
		input.setLayoutParams(layoutParams);
		input.setInputType(InputType.TYPE_CLASS_TEXT);
		if(defaultValue != null){
			input.setText(defaultValue);
		}
		LinearLayout layout = new LinearLayout(context);
		layout.setOrientation(LinearLayout.VERTICAL);
		layout.setLayoutParams(layoutParams);
		layout.setPadding(10, 10, 10, 10);
		layout.addView(tv);
		layout.addView(input);
		builder.setView(layout);

		// Set up the buttons
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() { 
		    
		    public void onClick(DialogInterface dialog, int which) {
		    	dialog.dismiss();
		    	if(onOk != null){
		    		onOk.setString(input.getText().toString());
		    		onOk.run();
		    	}
		    }
		});
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		    
			public void onClick(DialogInterface dialog, int which) {
		        dialog.cancel();
		    }
		});
		AlertDialog dialog = builder.create();
		dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
		dialog.show();
	}
	
	/**
	 * Shows a dialog to ask for device selection
	 * @param context
	 * @param title
	 * @param text
	 * @param onOk
	 */
	public static void selectDialog(Context context, String title, 
			String text, final UIUtilsOnOk onOk, final Runnable onCreate,
			ArrayList<Device> appDevices) {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(title);
		builder.setCancelable(true);
		builder.setIcon(android.R.drawable.ic_dialog_info);

		// Set up the input
		LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		final TextView tv = new TextView(context);
		tv.setLayoutParams(layoutParams);
		tv.setText(text);
		
		final Spinner select = new Spinner(context);
		select.setLayoutParams(layoutParams);
		if(appDevices == null){
			appDevices = new ArrayList<Device>();
		}
		Device[] items = new Device[appDevices.size()];
		appDevices.toArray(items);
		ArrayAdapter<Device> adapter = new ArrayAdapter<Device>(context, android.R.layout.simple_spinner_item, items);
		select.setAdapter(adapter);
	
		final Button create = new Button(context);
		create.setLayoutParams(layoutParams);
		create.setText(context.getString(R.string.createNewDevice));
		
		LinearLayout layout = new LinearLayout(context);
		layout.setOrientation(LinearLayout.VERTICAL);
		layout.setLayoutParams(layoutParams);
		layout.setPadding(10, 10, 10, 10);
		layout.addView(tv);
		layout.addView(select);
		layout.addView(create);
		builder.setView(layout);

		// Set up the buttons
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() { 
		    
		    public void onClick(DialogInterface dialog, int which) {
		    	dialog.dismiss();
		    	if(onOk != null){
		    		Device device = (Device) select.getSelectedItem();
		    		onOk.setString(device.id);
		    		onOk.run();
		    	}
		    }
		});
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		    
			public void onClick(DialogInterface dialog, int which) {
		        dialog.cancel();
		    }
		});
		final AlertDialog dialog = builder.create();
		
		create.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				dialog.dismiss();
				onCreate.run();
			}
		});
		
		dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
		dialog.show();
	}
	
}
