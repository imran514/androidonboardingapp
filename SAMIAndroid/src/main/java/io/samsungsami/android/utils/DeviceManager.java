package io.samsungsami.android.utils;

import io.samsungsami.model.DeviceArray;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.samsungsami.android.models.Device;
import android.util.Log;

public class DeviceManager {
	public ArrayList<Device> userDevices = new ArrayList<Device>();
	private static final String TAG = DeviceManager.class.getName();
	
	public DeviceManager(){
		clearCache();
	}
	
	/**
	 * Clear the cache of devices in memory
	 */
	public void clearCache(){
		userDevices.clear();
	}
	
	/**
	 * Loads a new set of devices
	 * @param result result from the SAMI users devices API call
	 */
	public void updateDevices(JSONObject result){
		JSONArray jsonData = null;
    	try {
    		jsonData = result.getJSONObject("data").getJSONArray("devices");
		}
    	catch (Exception e) {
			Log.d(TAG, "Error parsing result to get devices.");
		}
		clearCache();
		
		if (jsonData != null){	
			for(int i=0; i<jsonData.length(); i++){ 
				JSONObject device;
				try {
					device = (JSONObject) jsonData.getJSONObject(i);
					userDevices.add(new Device(
							device.getString("dtid"), 
							device.getString("id"), 
							device.getString("name")));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Loads the list of a user's devices
	 * @param deviceArray
	 */
	public void updateDevices(DeviceArray deviceArray){
		clearCache();
		if(deviceArray != null){
			for (io.samsungsami.model.Device device : deviceArray.getDevices()){
				userDevices.add(new Device(device.getDtid(), device.getId(), device.getName()));
			}
		}
	}
	
	/**
	 * Returns a JSON string from a credentials object
	 * @return
	 */
	public static String toJson(io.samsungsami.model.Device device){
		ObjectMapper mapper = new ObjectMapper();
		String json = null;
		try {
			json = mapper.writeValueAsString(device);
		} catch (JsonGenerationException ex) {
			ex.printStackTrace();
		} catch (JsonMappingException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return json;
	}
	
	/**
	 * Returns if the current stack has devices
	 * @return
	 */
	public boolean hasDevices(){
		return userDevices.size() > 0;
	}
	
	/**
	 * Returns a list of devices from the current devices stack
	 * @param dtid device type to filter
	 * @return
	 */
	public ArrayList<Device> getDevicesByType(String dtid){
		ArrayList<Device> result = new ArrayList<Device>();
		for(Device device : userDevices){
			if(device.deviceTypeId.equalsIgnoreCase(dtid)){
				result.add(device);
			}
		}
		return result;
	}
	
	/**
	 * Get an array for listpreference entries
	 * @return
	 */
	public CharSequence[] getCharSequenceEntries(){
		CharSequence[] entries = new CharSequence[userDevices.size()+1];
		entries[0] = "";
    	for(int i=0;i<userDevices.size();i++){
    		entries[i+1] = userDevices.get(i).name;
    	}
    	return entries;
	}
	 
	/**
	 * Get an array for listpreference values
	 * @return
	 */
	public CharSequence[] getCharSequenceEntriesValues(){
		CharSequence[] entries = new CharSequence[userDevices.size()+1];
		entries[0] = "";
		for(int i=0;i<userDevices.size();i++){
    		entries[i+1] = userDevices.get(i).id;
    	}
    	return entries;
	}
	
}
