package io.samsungsami.android.utils;

public class UIUtilsOnOk implements Runnable {
    public String string;
    
    public UIUtilsOnOk() {
    }
    /**
     * Used by UIUtils confirmDialog to pass the text from the dialog EditText
     * @param string
     */
    public void setString(String string){
    	this.string = string;
    }
    
    /**
     * Override the method run to do whatever is needed after confirm
     */
    public void run() {}

}
